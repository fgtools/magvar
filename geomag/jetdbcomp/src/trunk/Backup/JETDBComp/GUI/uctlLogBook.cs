/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas M�ller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using JETDBComp.Model;
#endregion

namespace JETDBComp.GUI
{
	public partial class uctlLogBook : UserControl
	{
		/*
		 * if the filter is activated only old events are shown,
		 * which contain the first 12 characters of the text in the selected treenode,
		 * and which are of the same event type.
		 * */
		
		#region constructor
		public uctlLogBook()
		{
			InitializeComponent();
			
			clsLogging.Message += new clsLogging.dlgMessage (this.WriteLogMessage);
			clsLogging.LOG(enumMsgType.Success,"initialization","Writing down the Logs has been initialized");
		}
		#endregion
		
		#region events
		public delegate void dlgNewEvent();
		public event dlgNewEvent Error;
		public event dlgNewEvent Warning;
		public event dlgNewEvent Info;
		public event dlgNewEvent Fail;
		public event dlgNewEvent Success;
		#endregion
		
		#region private attributes
		private List<TreeNode> mHiddenNodes = new List<TreeNode>();
		#endregion

		#region private methods

		private TreeNode SetImageKey(string strTitle,enumMsgType objType){
			TreeNode tnMessage = new TreeNode(strTitle);
			switch(objType){
				case enumMsgType.Error:
					tnMessage.ImageKey = "Error";
					break;
				case enumMsgType.Warning :
					tnMessage.ImageKey = "Warning";
					break;
				case enumMsgType.Info :
					tnMessage.ImageKey = "Info";
					break;
				case enumMsgType.Fail :
					tnMessage.ImageKey = "Fail";
					break;
				case enumMsgType.Success :
					tnMessage.ImageKey = "Success";
					break;
			}
			return tnMessage;
		}

		private void RaiseTypeEvent(enumMsgType objType){
			dlgNewEvent evt = null;
			if (objType == enumMsgType.Error) evt = this.Error;
			if (objType == enumMsgType.Warning ) evt = this.Warning ;
			if (objType == enumMsgType.Info ) evt = this.Info ;
			if (objType == enumMsgType.Fail ) evt = this.Fail ;
			if (objType == enumMsgType.Success ) evt = this.Success ;
			if (evt != null)
				evt();
		}
		
		#endregion

		#region event handler
		// handles the event that a new log entry was created.
		private void WriteLogMessage(clsMessage msg){
			if (InvokeRequired )
				this.Invoke(new dlgOnMessage(OnMessage),new object[]{msg});
			else
				OnMessage(msg);
			
		}
		private delegate void dlgOnMessage(clsMessage msg);
		//private dlgOnMessage MessageDelegate;
		
		private void OnMessage(clsMessage msg){
			TreeNode tnMessage = SetImageKey(msg.Title,msg.Type);

			tnMessage.SelectedImageKey = tnMessage.ImageKey;
			
			TreeNode tnTime= new TreeNode(msg.TimeStamp.ToLocalTime().ToString("dd.MM.yy HH:mm:ss.fff "));
			tnMessage.Nodes.Add(tnTime);
			tnTime.ImageKey = "Clock";
			tnTime.SelectedImageKey = "Clock";
			tnMessage.Nodes.Add(msg.Section);
			string[] strLines = msg.Description.Split('\n');
			foreach (string strLine in strLines)
				tnMessage.Nodes.Add( strLine);
			if ( cbFilter.Checked )
			{
				this.mHiddenNodes.Insert(0,tnMessage);
			}else{
				this.tvLog.Nodes.Insert(0,tnMessage);
			}
			
			while (tvLog.Nodes.Count > 1000)
				this.tvLog.Nodes.RemoveAt(1000);
			
			if(msg.Type == enumMsgType.Error){
				tvLog.CollapseAll();
				tvLog.Nodes[0].BackColor=Color.Red;
				tvLog.Nodes[0].Nodes[0].EnsureVisible();
			}
			RaiseTypeEvent(msg.Type);
		}
		#endregion
		
		#region delete event handler
		// removes all log messages
		private void btnDelAllClick(object sender, EventArgs e)
		{
			this.tvLog.Nodes.Clear();
		}
		
		// deletes the whole entry the selected node belongs to.
		private void btnDeleteClick(object sender, EventArgs e)
		{
			try{
				TreeNode tn = this.tvLog.SelectedNode;
		
				if (tn == null) 
					return;
				while ( ! (tn.Parent == null) ){
					tn = tn.Parent;
				}
				this.tvLog.Nodes.Remove(tn);
				if ( this.tvLog.SelectedNode == null ){
					if (this.cbFilter.Checked)
						FilterChanged(sender,e);
					this.cbFilter.Enabled = false;
				}
			}catch(Exception ex){
				MessageBox.Show( ex.Message ,ex.StackTrace );
			}
		}
		#endregion
		
		#region filterevents
		// handles the check-button "Filter"
		void FilterChanged(object sender, EventArgs e)
		{
			try{
				TreeNode tnNode;
				bool found = false;
				
				// if there are no hidden entries and the filter is activated:
				if (( mHiddenNodes.Count  == 0 ) && this.cbFilter.Checked){

					
					// copy all items into the hidden log-list.
					foreach(TreeNode item in this.tvLog.Nodes)
						this.mHiddenNodes.Add(item);
					
					// ------------------- filtering ---------------------
					
					// get the text of the current node
					tnNode = tvLog.SelectedNode;
					if (tnNode == null )
						return;
					string strText =tnNode.Text;
					
					// watch only the first 12 characters
					if (strText.Length > 12)
						strText = strText.Substring(0,12);
					
					// find the beginning of the event the selected node belongs to.
					while (tnNode.Parent != null)
						tnNode = tnNode.Parent;
					
					// get the name of the selected events symbol
					string strSymbol = tnNode.ImageKey;
					
					// hide all nodes (they have been copied to mlNodes)
					tvLog.Nodes.Clear();
					
					// search for the same text in events of the same type
					foreach(TreeNode item in mHiddenNodes){
						found = false;
						if (item.ImageKey == strSymbol){
							
							if (item.Text.StartsWith (strText))
								found = true;
							for(int intIndex=0;(intIndex < item.Nodes.Count) && ( ! found);intIndex++){
								if (item.Nodes[intIndex].Text.StartsWith (strText)){
									found = true;
									break;
								}
							}
						}
						// show the event if the text was found;
						if (found)
							tvLog.Nodes.Add( (TreeNode)item);//.Clone());
					}
				}else if ((mHiddenNodes.Count > 0 )&& !this.cbFilter.Checked){
					//filter aufheben.
					this.tvLog.Nodes.Clear();
					foreach(TreeNode item in this.mHiddenNodes)
						this.tvLog.Nodes.Add(item);
					this.mHiddenNodes.Clear();
					cbFilter.Enabled=false;
				}else {
					this.cbFilter.Checked=false;
		
//					throw new System.InvalidOperationException("Filterung ist in einem undefinierten Zustand.");
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
				if ( this.cbFilter.Checked ){
					foreach (TreeNode item in tvLog.Nodes)
						if (!mHiddenNodes.Contains(item))
							mHiddenNodes.Add(item);
					
				}else{
					foreach (TreeNode item in mHiddenNodes)
						if (!tvLog.Nodes.Contains(item))
							tvLog.Nodes.Add(item);
					
				}
			}
		}
		
		// if no TreeNode is selected and the filter was not already set, filtering is disabled.
		void TvLogAfterSelect(object sender, TreeViewEventArgs e){
			cbFilter.Enabled = ( tvLog.SelectedNode != null) || cbFilter.Checked ;
		}
		#endregion
		
		#region copy an event
		// Ctrl-C copies the entry which belongs to the selected node to the windows clipboard.
		void TvLogKeyUp(object sender, KeyEventArgs e)
		{
			try{
				if ((e.KeyCode == Keys.C) && (e.Modifiers == Keys.Control)){
					string strSelectedItemTexts ="";
					TreeNode tnNode =tvLog.SelectedNode;
					if (tnNode.Parent != null){
						tnNode = tnNode.Parent;
					}
					strSelectedItemTexts = tnNode.Text+"\n";
					foreach(TreeNode child in tnNode.Nodes){
						strSelectedItemTexts +=child.Text +"\n";
					}
					
					System.Windows.Forms.Clipboard.SetText (strSelectedItemTexts);
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		#endregion

	}
}