﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#region using
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using JETDBComp.Model;
#endregion

namespace JETDBComp.GUI
{
	public partial class clsCKDialog : Form
	{
		public clsCKDialog(TableNode tn)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			DataTable[] dt = (DataTable[]) tn.Tag;
			if (dt[2] != null){
				foreach(DataColumn dc in dt[2].Columns){
					if (dc.ColumnName != "\u0394") // delta ausschließen.
						this.cblColumns.Items.Add(dc.ColumnName);
				}
				foreach(DataColumn dc in dt[2].PrimaryKey){
					int idx = this.cblColumns.Items.IndexOf(dc.ColumnName);
					this.cblColumns.SetItemChecked(idx,true);
				}
			}
		}
		
		public string[] getPK(){
			string[] result = new string[this.cblColumns.CheckedItems.Count];
			int index = 0;
			foreach (object obj in this.cblColumns.CheckedItems ){
				result[index]=obj.ToString();
				index++;
			}
			return result;
		}
		
		
		
		void Label1MouseEnter(object sender, EventArgs e)
		{
			label1.Height = 81;
		}
		
		void Label1MouseLeave(object sender, EventArgs e)
		{
			label1.Height = 42;			
		}
	}
}