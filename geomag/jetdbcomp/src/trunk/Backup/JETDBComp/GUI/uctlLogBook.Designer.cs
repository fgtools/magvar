﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace JETDBComp.GUI
{
	partial class uctlLogBook
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uctlLogBook));
			this.cbFilter = new System.Windows.Forms.CheckBox();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnDelAll = new System.Windows.Forms.Button();
			this.tvLog = new System.Windows.Forms.TreeView();
			this.imageList2 = new System.Windows.Forms.ImageList(this.components);
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cbFilter
			// 
			this.cbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbFilter.Appearance = System.Windows.Forms.Appearance.Button;
			this.cbFilter.Enabled = false;
			this.cbFilter.Location = new System.Drawing.Point(240, 69);
			this.cbFilter.Name = "cbFilter";
			this.cbFilter.Size = new System.Drawing.Size(63, 30);
			this.cbFilter.TabIndex = 7;
			this.cbFilter.Text = "Filter";
			this.cbFilter.UseVisualStyleBackColor = true;
			this.cbFilter.CheckedChanged += new System.EventHandler(this.FilterChanged);
			// 
			// btnDelete
			// 
			this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDelete.Location = new System.Drawing.Point(240, 38);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(63, 30);
			this.btnDelete.TabIndex = 6;
			this.btnDelete.Text = "Löschen";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new System.EventHandler(this.btnDeleteClick);
			// 
			// btnDelAll
			// 
			this.btnDelAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDelAll.Location = new System.Drawing.Point(240, 3);
			this.btnDelAll.Name = "btnDelAll";
			this.btnDelAll.Size = new System.Drawing.Size(63, 34);
			this.btnDelAll.TabIndex = 5;
			this.btnDelAll.Text = "alle Löschen";
			this.btnDelAll.UseVisualStyleBackColor = true;
			this.btnDelAll.Click += new System.EventHandler(this.btnDelAllClick);
			// 
			// tvLog
			// 
			this.tvLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.tvLog.FullRowSelect = true;
			this.tvLog.HideSelection = false;
			this.tvLog.ImageIndex = 0;
			this.tvLog.ImageList = this.imageList2;
			this.tvLog.Location = new System.Drawing.Point(1, 1);
			this.tvLog.Name = "tvLog";
			this.tvLog.PathSeparator = ".";
			this.tvLog.SelectedImageIndex = 7;
			this.tvLog.Size = new System.Drawing.Size(236, 210);
			this.tvLog.TabIndex = 4;
			this.tvLog.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TvLogAfterSelect);
			this.tvLog.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TvLogKeyUp);
			// 
			// imageList2
			// 
			this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
			this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList2.Images.SetKeyName(0, "none.ico");
			this.imageList2.Images.SetKeyName(1, "Warning");
			this.imageList2.Images.SetKeyName(2, "Error");
			this.imageList2.Images.SetKeyName(3, "Success");
			this.imageList2.Images.SetKeyName(4, "Info");
			this.imageList2.Images.SetKeyName(5, "Fail");
			this.imageList2.Images.SetKeyName(6, "Clock");
			this.imageList2.Images.SetKeyName(7, "bullet_triangle_blue.png");
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.label1.ImageIndex = 1;
			this.label1.ImageList = this.imageList2;
			this.label1.Location = new System.Drawing.Point(232, 99);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(74, 16);
			this.label1.TabIndex = 8;
			this.label1.Text = "Warnung    ";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.label2.ImageIndex = 2;
			this.label2.ImageList = this.imageList2;
			this.label2.Location = new System.Drawing.Point(232, 118);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(74, 16);
			this.label2.TabIndex = 9;
			this.label2.Text = "Fehler";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.label3.ImageIndex = 3;
			this.label3.ImageList = this.imageList2;
			this.label3.Location = new System.Drawing.Point(232, 137);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(74, 16);
			this.label3.TabIndex = 10;
			this.label3.Text = "Erfolg";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.label4.ImageIndex = 4;
			this.label4.ImageList = this.imageList2;
			this.label4.Location = new System.Drawing.Point(232, 156);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(74, 16);
			this.label4.TabIndex = 11;
			this.label4.Text = "   Info";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.label5.ImageIndex = 5;
			this.label5.ImageList = this.imageList2;
			this.label5.Location = new System.Drawing.Point(232, 175);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(74, 16);
			this.label5.TabIndex = 12;
			this.label5.Text = "Mislungen     ";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// uctlLogBook
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tvLog);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbFilter);
			this.Controls.Add(this.btnDelete);
			this.Controls.Add(this.btnDelAll);
			this.Name = "uctlLogBook";
			this.Size = new System.Drawing.Size(304, 216);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ImageList imageList2;
		private System.Windows.Forms.TreeView tvLog;
		private System.Windows.Forms.Button btnDelAll;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.CheckBox cbFilter;
	}
}
