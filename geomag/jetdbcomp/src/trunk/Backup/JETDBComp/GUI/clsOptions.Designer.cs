﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace JETDBComp.GUI
{
	public partial class clsOptions
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbAreas = new System.Windows.Forms.GroupBox();
			this.cbProc = new System.Windows.Forms.CheckBox();
			this.cbQueries = new System.Windows.Forms.CheckBox();
			this.cbTable = new System.Windows.Forms.CheckBox();
			this.cbAccess = new System.Windows.Forms.CheckBox();
			this.cbSystem = new System.Windows.Forms.CheckBox();
			this.gbAmount = new System.Windows.Forms.GroupBox();
			this.cbDifferences = new System.Windows.Forms.CheckBox();
			this.cbDescription = new System.Windows.Forms.CheckBox();
			this.cbOrder = new System.Windows.Forms.CheckBox();
			this.cbData = new System.Windows.Forms.CheckBox();
			this.cbStructure = new System.Windows.Forms.CheckBox();
			this.cbProvider = new System.Windows.Forms.ComboBox();
			this.lblProv = new System.Windows.Forms.Label();
			this.btnAbbort = new System.Windows.Forms.Button();
			this.btnOk = new System.Windows.Forms.Button();
			this.gbAreas.SuspendLayout();
			this.gbAmount.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbAreas
			// 
			this.gbAreas.Controls.Add(this.cbProc);
			this.gbAreas.Controls.Add(this.cbQueries);
			this.gbAreas.Controls.Add(this.cbTable);
			this.gbAreas.Controls.Add(this.cbAccess);
			this.gbAreas.Controls.Add(this.cbSystem);
			this.gbAreas.Location = new System.Drawing.Point(4, 2);
			this.gbAreas.Name = "gbAreas";
			this.gbAreas.Size = new System.Drawing.Size(124, 153);
			this.gbAreas.TabIndex = 0;
			this.gbAreas.TabStop = false;
			this.gbAreas.Text = "Bereiche";
			// 
			// cbProc
			// 
			this.cbProc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbProc.Location = new System.Drawing.Point(6, 115);
			this.cbProc.Name = "cbProc";
			this.cbProc.Size = new System.Drawing.Size(112, 24);
			this.cbProc.TabIndex = 4;
			this.cbProc.Text = "Prozeduren";
			this.cbProc.UseVisualStyleBackColor = true;
			// 
			// cbQueries
			// 
			this.cbQueries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbQueries.Location = new System.Drawing.Point(6, 91);
			this.cbQueries.Name = "cbQueries";
			this.cbQueries.Size = new System.Drawing.Size(112, 24);
			this.cbQueries.TabIndex = 3;
			this.cbQueries.Text = "Abfragen";
			this.cbQueries.UseVisualStyleBackColor = true;
			// 
			// cbTable
			// 
			this.cbTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbTable.Location = new System.Drawing.Point(6, 67);
			this.cbTable.Name = "cbTable";
			this.cbTable.Size = new System.Drawing.Size(112, 24);
			this.cbTable.TabIndex = 2;
			this.cbTable.Text = "Benutzer-Tabellen";
			this.cbTable.UseVisualStyleBackColor = true;
			// 
			// cbAccess
			// 
			this.cbAccess.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbAccess.Location = new System.Drawing.Point(6, 43);
			this.cbAccess.Name = "cbAccess";
			this.cbAccess.Size = new System.Drawing.Size(112, 24);
			this.cbAccess.TabIndex = 1;
			this.cbAccess.Text = "Accesstabellen";
			this.cbAccess.UseVisualStyleBackColor = true;
			// 
			// cbSystem
			// 
			this.cbSystem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbSystem.Location = new System.Drawing.Point(6, 19);
			this.cbSystem.Name = "cbSystem";
			this.cbSystem.Size = new System.Drawing.Size(112, 24);
			this.cbSystem.TabIndex = 0;
			this.cbSystem.Text = "Systemtabellen";
			this.cbSystem.UseVisualStyleBackColor = true;
			// 
			// gbAmount
			// 
			this.gbAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.gbAmount.Controls.Add(this.cbDifferences);
			this.gbAmount.Controls.Add(this.cbDescription);
			this.gbAmount.Controls.Add(this.cbOrder);
			this.gbAmount.Controls.Add(this.cbData);
			this.gbAmount.Controls.Add(this.cbStructure);
			this.gbAmount.Location = new System.Drawing.Point(134, 2);
			this.gbAmount.Name = "gbAmount";
			this.gbAmount.Size = new System.Drawing.Size(157, 153);
			this.gbAmount.TabIndex = 5;
			this.gbAmount.TabStop = false;
			this.gbAmount.Text = "Umfang";
			// 
			// cbDifferences
			// 
			this.cbDifferences.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbDifferences.Location = new System.Drawing.Point(6, 115);
			this.cbDifferences.Name = "cbDifferences";
			this.cbDifferences.Size = new System.Drawing.Size(147, 24);
			this.cbDifferences.TabIndex = 4;
			this.cbDifferences.Text = "Nur Unterschiede";
			this.cbDifferences.UseVisualStyleBackColor = true;
			// 
			// cbDescription
			// 
			this.cbDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbDescription.Location = new System.Drawing.Point(6, 91);
			this.cbDescription.Name = "cbDescription";
			this.cbDescription.Size = new System.Drawing.Size(147, 24);
			this.cbDescription.TabIndex = 3;
			this.cbDescription.Text = "Kommentare";
			this.cbDescription.UseVisualStyleBackColor = true;
			// 
			// cbOrder
			// 
			this.cbOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbOrder.Location = new System.Drawing.Point(6, 67);
			this.cbOrder.Name = "cbOrder";
			this.cbOrder.Size = new System.Drawing.Size(147, 24);
			this.cbOrder.TabIndex = 2;
			this.cbOrder.Text = "Reihenfolge";
			this.cbOrder.UseVisualStyleBackColor = true;
			// 
			// cbData
			// 
			this.cbData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbData.Enabled = false;
			this.cbData.Location = new System.Drawing.Point(6, 43);
			this.cbData.Name = "cbData";
			this.cbData.Size = new System.Drawing.Size(147, 24);
			this.cbData.TabIndex = 1;
			this.cbData.Text = "Daten";
			this.cbData.UseVisualStyleBackColor = true;
			// 
			// cbStructure
			// 
			this.cbStructure.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbStructure.Checked = true;
			this.cbStructure.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbStructure.Enabled = false;
			this.cbStructure.Location = new System.Drawing.Point(6, 19);
			this.cbStructure.Name = "cbStructure";
			this.cbStructure.Size = new System.Drawing.Size(147, 24);
			this.cbStructure.TabIndex = 0;
			this.cbStructure.Text = "Struktur";
			this.cbStructure.UseVisualStyleBackColor = true;
			// 
			// cbProvider
			// 
			this.cbProvider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbProvider.AutoCompleteCustomSource.AddRange(new string[] {
									"Micorsoft.JET.OleDB 4.0",
									"Microsoft.ACE.OleDB 12.0",
									"SQLite"});
			this.cbProvider.FormattingEnabled = true;
			this.cbProvider.Items.AddRange(new object[] {
									"Micorsoft.JET.OleDB 4.0",
									"Microsoft.ACE.OleDB 12.0",
									"SQLite"});
			this.cbProvider.Location = new System.Drawing.Point(49, 161);
			this.cbProvider.Name = "cbProvider";
			this.cbProvider.Size = new System.Drawing.Size(242, 21);
			this.cbProvider.TabIndex = 8;
			this.cbProvider.SelectedIndexChanged += new System.EventHandler(this.CbProviderSelectedIndexChanged);
			// 
			// lblProv
			// 
			this.lblProv.Location = new System.Drawing.Point(4, 163);
			this.lblProv.Name = "lblProv";
			this.lblProv.Size = new System.Drawing.Size(49, 15);
			this.lblProv.TabIndex = 9;
			this.lblProv.Text = "Provider";
			// 
			// btnAbbort
			// 
			this.btnAbbort.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnAbbort.Location = new System.Drawing.Point(4, 191);
			this.btnAbbort.Name = "btnAbbort";
			this.btnAbbort.Size = new System.Drawing.Size(75, 23);
			this.btnAbbort.TabIndex = 10;
			this.btnAbbort.Text = "Abbr.";
			this.btnAbbort.UseVisualStyleBackColor = true;
			// 
			// btnOk
			// 
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Location = new System.Drawing.Point(216, 188);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(75, 23);
			this.btnOk.TabIndex = 11;
			this.btnOk.Text = "OK";
			this.btnOk.UseVisualStyleBackColor = true;
			// 
			// clsOptions
			// 
			this.AcceptButton = this.btnOk;
			this.CancelButton = this.btnAbbort;
			this.ClientSize = new System.Drawing.Size(296, 228);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnAbbort);
			this.Controls.Add(this.cbProvider);
			this.Controls.Add(this.lblProv);
			this.Controls.Add(this.gbAmount);
			this.Controls.Add(this.gbAreas);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(267, 264);
			this.Name = "clsOptions";
			this.ShowIcon = false;
			this.Text = "Vergleichsoptionen";
			this.gbAreas.ResumeLayout(false);
			this.gbAmount.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.CheckBox cbDifferences;
		private System.Windows.Forms.CheckBox cbDescription;
		private System.Windows.Forms.CheckBox cbOrder;
		private System.Windows.Forms.CheckBox cbData;
		private System.Windows.Forms.Label lblProv;
		private System.Windows.Forms.ComboBox cbProvider;
		private System.Windows.Forms.Button btnAbbort;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.CheckBox cbStructure;
		private System.Windows.Forms.GroupBox gbAmount;
		private System.Windows.Forms.CheckBox cbSystem;
		private System.Windows.Forms.CheckBox cbAccess;
		private System.Windows.Forms.CheckBox cbTable;
		private System.Windows.Forms.CheckBox cbQueries;
		private System.Windows.Forms.CheckBox cbProc;
		private System.Windows.Forms.GroupBox gbAreas;
		
	}
}
