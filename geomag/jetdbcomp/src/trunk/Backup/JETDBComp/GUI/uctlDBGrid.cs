﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#region using
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data;
using JETDBComp.Model;
#endregion

namespace JETDBComp.GUI
{
	public partial class uctlDBGrid : UserControl
	{
		#region private member
		private static DataGridViewCellStyle mdgsPrimaryKey = null;
		private static DataGridViewCellStyle mdgsRemoved;
		private static DataGridViewCellStyle mdgsAdded;
		private static DataGridViewCellStyle mdgsChanged;
		private static DataGridViewCellStyle mdgsUnchanged;
		private DataTable mdtRed;
		private DataTable mdtGreen;
		#endregion
		int semaphore = 0;

		#region events
		public delegate void dlgStrStr( string strOld, string strNew);
		public event dlgStrStr ChangedCellSelected;
		#endregion
		
		public void SetDataSources( DataTable red, DataTable green, DataTable merged){
			this.mdtGreen =green;
			this.mdtRed =red;
			this.mDGV.DataSource=merged;
			
			// Sortieren nach der ersten PK-Spalte
			if (this.mDGV.DataSource != null){
				semaphore++;
				DataColumn[] pkey = merged.PrimaryKey;
				if (pkey.Length > 0){
					mDGV.Sort(mDGV.Columns[pkey[0].ColumnName],ListSortDirection.Ascending );
				}
				semaphore--;
			}
			// Explizit die Spaltenbreiten setzen und anschließend wieder änderbar machen;
			foreach(DataGridViewColumn gc in mDGV.Columns){
				gc.Width=0;
				gc.AutoSizeMode=DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
				this.mDGV.AutoResizeColumn(gc.Index,DataGridViewAutoSizeColumnMode.AllCellsExceptHeader);
				gc.Width=gc.Width;
				gc.AutoSizeMode=DataGridViewAutoSizeColumnMode.None;
			}
			this.mDGV.AutoSizeColumnsMode=DataGridViewAutoSizeColumnsMode.None;
		}

		
		#region construction and initialization
		public uctlDBGrid()
		{
			InitializeComponent();
			mBrush = new System.Drawing.SolidBrush(SystemColors.Window);
		}
		
		private void UctlDBGridLoad(object sender, EventArgs e)
		{
			if (mdgsRemoved == null){
				mdgsRemoved = new DataGridViewCellStyle();
				mdgsRemoved.BackColor = System.Drawing.Color.FromArgb(255, 128, 128);
				mdgsRemoved.ForeColor = System.Drawing.Color.Black;
				mdgsRemoved.NullValue = null;
				mdgsRemoved.Font=mDGV.Font;
				mdgsRemoved.SelectionBackColor = System.Drawing.Color.Maroon;
				mdgsRemoved.SelectionForeColor = System.Drawing.Color.FromArgb(255, 128, 0);
			}
			
			if(mdgsAdded == null){
				mdgsAdded =  new DataGridViewCellStyle();
				mdgsAdded.BackColor = System.Drawing.Color.FromArgb(128, 255, 128);
				mdgsAdded.ForeColor = System.Drawing.Color.Black;
				mdgsAdded.Font=mDGV.Font;
				mdgsAdded.SelectionBackColor = System.Drawing.Color.FromArgb(0, 64, 0);
				mdgsAdded.SelectionForeColor = System.Drawing.Color.FromArgb(235, 233, 154);
			}
			
			if(mdgsChanged == null){
				mdgsChanged =  new DataGridViewCellStyle();
				mdgsChanged.BackColor = System.Drawing.Color.FromArgb(255, 255, 128);
				mdgsChanged.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
				mdgsChanged.ForeColor = System.Drawing.Color.FromArgb(64, 64, 0);
				mdgsChanged.SelectionBackColor = System.Drawing.Color.Olive;
				mdgsChanged.SelectionForeColor = System.Drawing.Color.Yellow;
			}

			if(mdgsUnchanged == null){
				mdgsUnchanged =  new DataGridViewCellStyle();
				mdgsUnchanged.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
				mdgsUnchanged.Font=mDGV.Font;
				mdgsUnchanged.ForeColor = System.Drawing.Color.FromArgb(0, 0, 0);
				mdgsUnchanged.SelectionBackColor = System.Drawing.Color.FromArgb(12, 12, 84);
				mdgsUnchanged.SelectionForeColor = System.Drawing.Color.FromArgb(255, 255, 255);
			}
			
			if (mdgsPrimaryKey == null){
				mdgsPrimaryKey = mDGV.ColumnHeadersDefaultCellStyle.Clone();
				mdgsPrimaryKey.Font = new Font( 	mDGV.DefaultCellStyle.Font.FontFamily,
				                               mDGV.DefaultCellStyle.Font.Size,
				                               FontStyle.Bold | FontStyle.Underline );
			}
		}
		
		protected override void Dispose(bool disposing){
			if (mBrush != null)
				mBrush.Dispose();
			if (mdgsAdded != null)
				if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#endregion
		
		private DataRow getDataRow(DataGridViewRow gridRow){
			DataRowView drv;
			DataRow dr = null;
			if (typeof(DataRowView).IsInstanceOfType (gridRow.DataBoundItem)){
				drv = (DataRowView)gridRow.DataBoundItem;
				dr = drv.Row ;
			}
			return dr;
		}
		
		#region event handler

		private void MDGVDataError(object sender, DataGridViewDataErrorEventArgs e){
			// ignore any error.
		}
		
		private void MDGVDataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e){
			semaphore ++;
			try{
				if (semaphore > 1) return;
				// underline Primary Key
				if (mDGV.DataSource.GetType().Equals(typeof(DataTable))){
					DataColumn[] pkey = ((DataTable)mDGV.DataSource).PrimaryKey;
					foreach (DataColumn dc in pkey){
						mDGV.Columns[dc.ColumnName].HeaderCell.Style = mdgsPrimaryKey;
						
					}
					// neue und alte Spalten färben.
					foreach (DataColumn dc in ((DataTable)mDGV.DataSource).Columns){
						if (dc.Ordinal==0 )
							continue;
						if ((mdtRed!=null) && (mdtRed.Columns[dc.ColumnName] == null)){
							if (mDGV.Columns[dc.ColumnName].HeaderCell.Style == mdgsPrimaryKey){
								mDGV.Columns[dc.ColumnName].HeaderCell.Style = mdgsAdded.Clone();
								mDGV.Columns[dc.ColumnName].HeaderCell.Style.Font=mdgsPrimaryKey.Font;
							}else
								mDGV.Columns[dc.ColumnName].HeaderCell.Style = mdgsAdded;
						}else if ((mdtGreen!=null) && (mdtGreen.Columns[dc.ColumnName] == null )){
							if (mDGV.Columns[dc.ColumnName].HeaderCell.Style == mdgsPrimaryKey){
								mDGV.Columns[dc.ColumnName].HeaderCell.Style = mdgsRemoved.Clone();
								mDGV.Columns[dc.ColumnName].HeaderCell.Style.Font=mdgsPrimaryKey.Font;
							}else
								mDGV.Columns[dc.ColumnName].HeaderCell.Style = mdgsRemoved;
						}
						
					}
				}

			}finally{
				semaphore --;
			}
		}

		private void MDGVSelectionChanged(object sender, EventArgs e)
		{
			try{
				DataGridViewSelectedCellCollection cells = mDGV.SelectedCells ;
				if (mDGV.SelectedCells.Count == 1){
					DataRow dr = getDataRow(mDGV.SelectedCells[0].OwningRow);
					if (dr[0].ToString().Equals("<>")){
						clsDifference diff = (clsDifference)dr[0];
						if ((diff.ContainsKey(cells[0].OwningColumn.Index)) &&
						    (diff[cells[0].OwningColumn.Index].Equals('d'))){
							
							string strNewValue = dr.ItemArray[cells[0].OwningColumn.Index].ToString();
							
							dr = mdtRed.Rows.Find(clsDataTable.primaryValue(mdtRed.PrimaryKey,dr));
							
							string strOldValue = dr.ItemArray[mdtRed.Columns[cells[0].OwningColumn.Name].Ordinal].ToString();
							if (ChangedCellSelected != null)
								ChangedCellSelected(strOldValue,strNewValue);
							return;
						}
					}
					if (ChangedCellSelected != null)
						ChangedCellSelected("","");
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		
		private static SolidBrush mBrush= null;
		
		private	void MDGVCellPainting(object sender, DataGridViewCellPaintingEventArgs e)
		{
			try{
				if ((e.RowIndex >=0)&&(e.ColumnIndex > 0)){
					DataGridViewCellStyle current =mdgsUnchanged;
					DataRow dr = getDataRow(mDGV.Rows[e.RowIndex]);
					if (dr == null)return;
					object state = dr[0];
					switch(state.ToString()){
						case "+":
							current = mdgsAdded ;
							break;
						case "-":
							current = mdgsRemoved ;
							break;
						case "<>":
							clsDifference diff = (clsDifference)state;
							if (diff.ContainsKey(e.ColumnIndex)){
								switch(diff[e.ColumnIndex]){
									case '=':
										current =mdgsUnchanged ;
										break;
									case 'd':
										current =mdgsChanged ;
										break;
									case '+':
										current =mdgsAdded ;
										break;
									case '-':
										current=mdgsRemoved ;
										break;
								}
							}else
								current = mdgsChanged ;
							break;
						case "=":
							current = mdgsUnchanged ;
							break;
					}
					
					if ( mDGV.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected ){
						mBrush.Color=current.SelectionBackColor;
					}else
						mBrush.Color=current.BackColor;
					
					e.Graphics.FillRectangle(mBrush,e.CellBounds);
					RectangleF r= new RectangleF(e.CellBounds.Left,e.CellBounds.Top,e.CellBounds.Width ,e.CellBounds.Height);
					if (current.Font != null){
						if ( mDGV.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected ){
							mBrush.Color = current.SelectionForeColor;
						}else
							mBrush.Color = current.ForeColor ;
						StringFormat sf = new StringFormat(StringFormatFlags.NoWrap);
						sf.Alignment=StringAlignment.Near;
						sf.LineAlignment =StringAlignment.Center;
						e.Graphics.DrawString(e.FormattedValue.ToString() ,current.Font,mBrush,r,sf);
					}
					e.Paint(e.CellBounds ,DataGridViewPaintParts.Border);
					e.Handled = true;
				}else
					e.Handled=false;
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		#endregion
	}

}