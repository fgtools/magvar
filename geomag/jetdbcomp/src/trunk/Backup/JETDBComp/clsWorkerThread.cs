﻿/*
    JET DATABASE COMPARE allows You to compare two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Windows.Forms;
using System.Threading;

namespace JETDBComp
{


	/// <summary>
	/// my own implementation of the backgroundworker of .net 4.0
	/// (needs .net 2.0 only)
	/// 
	/// subclass this class,
	/// override the Method "Task".
	/// "Start" and if you want "Abort" the task.
	/// restart it with "Start" again.
	/// Pause and Continue are not available!
	/// 
	/// read the state (boolean property running)
	/// 
	/// handle the Events:
	/// intermediate (informs the Gui that its the best time to update the display)
	/// finished (informs the Gui that the Work is completely done)
	/// aborted (informs the Gui that the Task was not completely done but aborted)
	/// </summary>
	public abstract class clsWorkerThread
	{
		#region " Privater Anteil "
		
		private Form mobjForm;
		private Thread mWorkThread;
		
		private void DoWork(){
			try{
				Task();
				if (Finished != null)
					lock(this){
					mobjForm.Invoke(Finished,new object[]{this, EventArgs.Empty});
				}
			}catch(Exception){
				if (Aborted != null){
					lock(this){
						try{
							mobjForm.Invoke(Aborted,new object[]{this, EventArgs.Empty});
						}catch(Exception){
						}
					}
				}
				
			}
		}
		#endregion
		
		#region "öffentlich: für Nutzung durch die GUI"

		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="objForm">Die GUI, die  werden soll</param>
		public clsWorkerThread(Form objForm){
			mobjForm=objForm;
			mWorkThread = new Thread(new ThreadStart(this.DoWork));
		}

		/// <summary>
		/// Wrapper: Starts the internal Thread;
		/// </summary>
		public void Start(){
			lock(this){
				MessageBox.Show(mWorkThread.ThreadState.ToString(),"ThreadState");
				switch(mWorkThread.ThreadState){
					case ThreadState.Aborted:
					case ThreadState.Stopped:
						mWorkThread = new Thread(new ThreadStart(this.DoWork));
						mWorkThread.Start();
						break;
					case ThreadState.Unstarted:
						mWorkThread.Start();
						break;
					default:
						break;
				}
			}
		}
		/// <summary>
		/// Wrapper: Aborts the internal Thread;
		/// </summary>
		public void Abort(){
			lock(this){
				MessageBox.Show(mWorkThread.ThreadState.ToString(),"ThreadState");
				switch(mWorkThread.ThreadState ){
					case ThreadState.AbortRequested:
					case ThreadState.Aborted:
						// already aborted;
						break;
					case ThreadState.StopRequested:
					case ThreadState.SuspendRequested:
						
					case ThreadState.Running:
					case ThreadState.Stopped:
					case ThreadState.Suspended:
					case ThreadState.Background:
					case ThreadState.Unstarted:
					case ThreadState.WaitSleepJoin:
						mWorkThread.Abort();
						break;
					default:
						break;
				}
			}
		}
		
		/// <summary>
		/// state of the internal Thread
		/// </summary>
		public bool Running{
			get{
				switch(mWorkThread.ThreadState ){
					case ThreadState.AbortRequested:
					case ThreadState.Aborted:
						return false;
					case ThreadState.SuspendRequested:
					case ThreadState.Suspended:
					case ThreadState.Background:
					case ThreadState.Running:
					case ThreadState.WaitSleepJoin:
						return true;
					case ThreadState.StopRequested:
					case ThreadState.Stopped:
						return false;
					case ThreadState.Unstarted:
						return false;
					default:
						return false;
				}
			}
		}

		/// <summary>
		/// Please the GUI to Update some Values;
		/// </summary>
		public event EventHandler Intermediate;
		
		/// <summary>
		/// informs the GUI that the Work is done;
		/// </summary>
		public event EventHandler Finished;

		/// <summary>
		/// informs the GUI that the Work was aborted;
		/// </summary>
		public event EventHandler Aborted;

		#endregion
		
		#region "protected: für nutzung in Sub-Klassen"

		/// <summary>
		/// Informiere die GUI, das es was neues anzuzeigen gibt...
		/// </summary>
		protected void InvokeIntermediate(){
			if (Intermediate != null)
				mobjForm.Invoke(Intermediate,new object[]{this, EventArgs.Empty});
		}
		
		/// <summary>
		/// Hier soll die eigentliche Arbeit (unterbrochen mit einpaar (Thread.Sleep(...)-Anweisungen)
		/// implementiert werden.
		/// </summary>
		protected abstract void Task();
		#endregion
	}

}
