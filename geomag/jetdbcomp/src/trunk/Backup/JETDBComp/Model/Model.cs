/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas M�ller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.Data.OleDb;
using System.Data;
using System.Collections.Generic;
#endregion

namespace JETDBComp.Model
{
	public class clsBusinessLayer : IDisposable
	{
		
		#region private members
		private clsDAL mDAL;
		
		private string mstrFilenameMaster;
		private string mstrFilenameTarget;
		private bool mblnSystem=true;
		private bool mblnAccess=true;
		private bool mblnTables =true;
		private bool mblnViews =true;
		private bool mblnProcedures=true;
		private bool mblnDescription=true;
		private bool mblnOnlyDifferences=true;
		private bool mblnPosition=true;
		private bool mblnData = true;
		private const string COLUMNS = "Spalten";
		#endregion

		#region propeties
		public string FilenameMaster {
			set {
				if ((value == null) || (System.IO.File.Exists(value)))
					mstrFilenameMaster = value;
			}
			get { return mstrFilenameMaster; }
		}
		public string FilenameTarget {
			set {
				if ((value == null) || (System.IO.File.Exists(value)))
					mstrFilenameTarget = value;
			}
			get { return mstrFilenameTarget; }
		}

		public bool OptionData{
			get{
				return mblnData;
			}
			set{
				mblnData=value;
			}
		}
		public bool OptionSystem{
			get {
				return mblnSystem;
			}
			set{
				mblnSystem=value;
			}
		}
		public bool OptionAccess{
			get{
				return mblnAccess;
			}
			set{
				mblnAccess=value;
			}
		}
		public bool OptionTables{
			get{
				return mblnTables;
			}
			set{
				mblnTables= value;
			}
		}
		public bool OptionViews{
			get{
				return mblnViews;
			}
			set{
				mblnViews= value;
			}
		}
		public bool OptionProcedures{
			get{
				return mblnProcedures;
			}
			set{
				mblnProcedures= value;
			}
		}
		public bool OptionDescription{
			get{
				return mblnDescription;
			}
			set{
				mblnDescription = value;
			}
		}
		public bool OptionOnlyDifferences{
			get{
				return mblnOnlyDifferences;
			}
			set{
				mblnOnlyDifferences = value;
			}
		}
		public bool OptionPosition{
			get{
				return mblnPosition;
			}
			set{
				mblnPosition=value;
			}
		}

		public clsDAL DAL{
			get{
				if (mDAL == null) mDAL= new clsDAL();
				
				return mDAL;
			}
			set{
				mDAL=value;
			}
		}
	
		#endregion
		
		#region construktor destruktor
		public clsBusinessLayer(clsDAL dal)
		{
			this.mDAL=dal;
		}
		
		public void Dispose(){
			if (mDAL != null){
				this.mDAL.Dispose();
				this.mDAL=null;
			}
		}
		#endregion


		public void analyse (DBTreeNode tn,string strFilename){
			mDAL.loadMetadata(strFilename);

			// reads Schema from the mdb file
			Dictionary<Guid,DataTable> dicSchemas =
				mDAL.Metadata;

			try{
				tn.Clear();
				if (dicSchemas != null){
					DBTreeNode Schemas = new DBTreeNode("Schemas");
					// add the Schema information into the treeview
					foreach(Guid guid in dicSchemas.Keys){
						TableNode tbNode = new TableNode();
						tbNode.Text = clsDAL.getSchemaName(guid);
						tbNode.Key=tbNode.Text;
						Schemas.Add( tbNode );
						tbNode.Table = dicSchemas[guid];
					}
					tn.Add(Schemas);
					foreach(DBTreeNode node in this.analyse(dicSchemas )){
						tn.Add(node);
					}
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		
		#region "private schema subroutines"

		// Aus den Daten in den �bergebenen dictionary wird eine Baumstruktur generiert, Die genau eine Datenbank darstellt.
		private DBTreeNode analyse(Dictionary<Guid,DataTable> dicSchemas){
			
			DBTreeNode result = new DBTreeNode ("Schemas");
			TypeNode.schemaProvidedTypes(dicSchemas );

			if ( OptionTables || OptionSystem || OptionAccess || OptionViews){
				schemaTables      (result,dicSchemas[OleDbSchemaGuid.Tables       ]); // Tabellennamen anlegen
				schemaColumns     (result,dicSchemas[OleDbSchemaGuid.Columns      ]); // Spalten der Tabellen ermitteln
				schemaIndices     (result,dicSchemas[OleDbSchemaGuid.Indexes      ]); // Prim�rschl�ssel und andere Tabelleninterne Indizes ermitteln
				schemaForeignKeys (result,dicSchemas[OleDbSchemaGuid.Foreign_Keys ]); // Fremdschl�ssel -  Beziehungen zu andern Tabellen.
			}
				
			if( OptionViews )
				schemaQueries     (result,dicSchemas[OleDbSchemaGuid.Views        ]); // abgespeicherte Abfragen analysieren
			else
				result.Remove(result.Find("Views"));
			
			if (OptionProcedures )
				schemaProcedures  (result,dicSchemas[OleDbSchemaGuid.Procedures   ]); // Prozeduren aufz�hlen
			
			schemaUsageConstraints(result,dicSchemas[OleDbSchemaGuid.Constraint_Column_Usage]);
			schemaRefConstraints(result,dicSchemas[OleDbSchemaGuid.Referential_Constraints]);
			
			if ( OptionTables || OptionSystem || OptionAccess ){
				schemaTableConstraints(result,dicSchemas[OleDbSchemaGuid.Table_Constraints ]);
				schemaCheckConstraints(result,dicSchemas[OleDbSchemaGuid.Check_Constraints ]);
			}
			

			if(!OptionProcedures ){
				result.Remove(result.Find("Procedures"));
			}
			if(!OptionTables){
				result.Remove(result.Find("Tabellen"));
			}
			if (!OptionSystem){
				result.Remove(result.Find("System-Tabellen"));
			}
			if(!OptionAccess ){
				result.Remove(result.Find("Access"));
			}
			return result;
			
		}
		
		private void AddConstraintProperties(DBTreeNode tnConstraint, DataRow dr){
			
			if (tnConstraint != null){
				foreach (DataColumn dc in dr.Table.Columns  ){
					switch(dc.ColumnName){
						case "CONSTRAINT_NAME":
						case "CONSTRAINT_TYPE":
						case "TABLE_NAME":
						case "COLUMN_NAME":
							continue;
						case "UNIQUE_CONSTRAINT_NAME":
							string strUniqName=dr[dc.ColumnName].ToString();
							if (tnConstraint.Key != strUniqName)
								tnConstraint.Text+=" ("+strUniqName+")";
							continue;
						case "CHECK_CLAUSE":
							tnConstraint.Add(dr[dc.ColumnName].ToString(),dr[dc.ColumnName].ToString());
							continue;
						case "DESCRIPTION":
							if (!this.OptionDescription) continue;
							break;
					}
					if (dc.ColumnName.EndsWith("RULE") && dr[dc.ColumnName].Equals("NO ACTION")) continue;
					//-------------------------------------
					if (!dr.IsNull(dc.ColumnName)){
						string strText =dr[dc.ColumnName].ToString();
						if (strText.Equals("True")){
							tnConstraint.Add(strText = dc.ColumnName);
							continue;
						}else if (strText.Equals("False"))
							continue;
						tnConstraint.Add(dc.ColumnName+" := \""+strText+"\"");
					}
				}
			}
		}
		
		/// <summary>
		/// F�gt den Namen jeder Tabelle in die Richtige Gruppe ein
		/// </summary>
		/// <param name="parent">Der Knoten dessen Unterknoten "Access", "System-Tabellen", "Tabellen" und "Views" angelegt werden sollen</param>
		/// <param name="dtData">Die Schematabelle, die die Tabellen-Namen beschreibt.</param>
		private void schemaTables(DBTreeNode parent,DataTable dtData ){
			
			DBTreeNode tnAccess = parent.Add("ACCESS TABLE","Access",enumImages.AccessTable);
			DBTreeNode tnSystem = parent.Add("SYSTEM TABLE","System-Tabellen",enumImages.SystemTable);
			DBTreeNode tnTables = parent.Add("TABLE","Tabellen",enumImages.Table);
			DBTreeNode tnViews  = parent.Add("VIEW","Views",enumImages.View);
			DBTreeNode tbLinks  = parent.Add("LINK","Verkn�pfungen",enumImages.Link); // links to tables in other mdb-files
							
			TableNode tnName;
			
			foreach (DataRow dr in dtData.Rows){
				tnName = new TableNode(dr) ;

				string strType = dr["TABLE_TYPE"].ToString();
				DBTreeNode tn;
				if ( (tn = parent[strType]) != null){
					tnName.Image=tn.Image;
					tn.Add(tnName );
				} else {
					tnName.Image = enumImages.SystemTable;
					tnName.Add("TableTyp: "+dr["TABLE_TYPE"].ToString());
					parent["TABLE"].Add(tnName);
				}
			}

			foreach (DBTreeNode tn in parent){
				tn.Key = tn.Text;
			}
			
		}
		
		private void schemaColumns(DBTreeNode parent,DataTable dtData){
			
			ColumnNode tnColumn;
			TableNode tnTable;
			
			foreach (DataRow dr in dtData.Rows){
				tnTable =null;
				
				// den Knoten der Tabelle ermitteln, zu der die Spalte geh�rt.
				string strTableName =dr["TABLE_NAME"].ToString();
				
				// in den vier untergruppen "Access" "System" "Tabellen" und "Views" die Tabelle suchen
				// zu der die untersuchte Spalte (dr) geh�rt.
				foreach (DBTreeNode group in parent ){
					if (group[strTableName] != null)
						if (group[strTableName].GetType().Equals(typeof(TableNode )))
							tnTable=(TableNode)group[strTableName];
					if (tnTable != null)
						break;
				}
				
				// falls die Tabelle fehlt, eine Pseudotabelle einf�gen.
				if (tnTable == null){
					tnTable = new TableNode();
					tnTable.Text=strTableName;
					tnTable.Key=tnTable.Text;
					tnTable.Image=enumImages.SystemTable ;
					parent["Tabellen"].Add (tnTable );
				}
				if ( tnTable[COLUMNS] == null)
					tnTable.Add(COLUMNS,COLUMNS,enumImages.Column);
				
				
				tnColumn = new ColumnNode (dr);
				if (this.OptionPosition){
					tnColumn.Add("Position: "+tnColumn.Position).Visible =false;
				}
				tnColumn.insertInto(tnTable);
				
				if((bool)dr["COLUMN_HASDEFAULT"]){
					tnColumn.Add("Default: "+dr["COLUMN_DEFAULT"].ToString());
				}
				columnType (dr, tnColumn );
				if(!(bool)dr["IS_NULLABLE"]){
					if (tnColumn[tnColumn.Count-1]["Null"] != null)
						tnColumn.Add("NOT NULL");
				}
				if (OptionDescription ){
					if(! dr.IsNull ("DESCRIPTION")){
						tnColumn.Add("\""+dr["DESCRIPTION"].ToString()+"\"" );
					}
				}
				DBTreeNode tnFlag = tnColumn.Add("Flags: "+dr["COLUMN_FLAGS"].ToString());
				tnFlag.Visible = false;
			}
			
		}
		/// <summary>
		///  Prim�rschl�ssel, die �ber mehr als eine Spalte gehen.
		///  eindeutige Schl�ssel, die keine Prim�rschl�ssel sind.
		/// uneindeutige Schl�ssel.
		///  Fremdschl�ssel werden hier nicht betrachtet. 
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="dtData"></param>
		private void schemaIndices(DBTreeNode parent,DataTable dtData){
			
			DBTreeNode tnColumn;
			TableNode tnTable;
			string strName;

			foreach (DataRow dr in dtData.Rows){
				tnTable =null;
				try{
					strName =dr["TABLE_NAME"].ToString();
					
					foreach (DBTreeNode group in parent ){
						tnTable=(TableNode)group[strName];
						if (tnTable != null)
							break;
					}
					if (tnTable== null){
						tnTable = new TableNode();
						tnTable.Text = strName;
						tnTable.Key = strName;
						tnTable.Image = enumImages.SystemTable;
						parent["Tabellen"].Add(tnTable );
						tnTable.Add(COLUMNS,COLUMNS,enumImages.Column );
					}
					//tnTable=tnTable[COLUMNS];
					
					strName =dr["COLUMN_NAME"].ToString();
					tnColumn = tnTable[COLUMNS][strName];
					if (tnColumn== null){
						tnColumn = tnTable[COLUMNS].Add(strName);
						tnColumn.Image=enumImages.Unknown;
					}
					
					if (!tnColumn.Parent.Text.Equals( dr["INDEX_NAME"].ToString())){
						strName = dr["INDEX_NAME"].ToString();
						tnColumn.Add(strName,strName,enumImages.Index);
					}
					if((bool)dr["PRIMARY_KEY"]){
						tnColumn.Image=enumImages.PrimaryKey;
					}else{
						tnColumn.Image=enumImages.Index;
					}
					
					if((bool)dr["UNIQUE"]){
						/*if ((tnColumn.Image ==enumImages.Index ) ||
						    (tnColumn.Image == enumImages.UniqKey )
						   )
							tnColumn.Image=enumImages.UniqKey;
						else */
						if (tnColumn.Image != enumImages.PrimaryKey ){
							tnColumn.Add("Eindeutig");
							tnColumn.Image=enumImages.UniqKey;
						}
					}
				}catch(Exception ex){
					clsLogging.GEH(ex);
				}
			}
			
		}

		private void schemaForeignKeys(DBTreeNode parents,DataTable dtData){
			FKNode tnFK;
			
			foreach (DataRow dr in dtData.Rows){
				try{
					
					tnFK = new FKNode(dr);
					tnFK.Init(parents);
					
				}catch(Exception ex){
					clsLogging.GEH(ex);
				}
			}
		}
		
		private void schemaQueries(DBTreeNode parent,DataTable dtData){
			if (parent["Views"] != null){
				foreach (DataRow dr in dtData.Rows){
					string strName = dr["TABLE_NAME"].ToString() ;
					string strDefinition = dr["VIEW_DEFINITION"].ToString();
				
					TableNode tnView = null;
					tnView = (TableNode)parent["Views"][strName];
					if (tnView == null){
						tnView=new TableNode();
						tnView.Text=strName;
						tnView.Key=strName;
						tnView.Image = enumImages.View;
						tnView.Metadata = dr;
						parent["Views"].Add( tnView);
						try{
							tnView.Table = mDAL.query (strDefinition);
						}catch(Exception ex){
							clsLogging.GEH(ex);
						}
					}
					if (!dr.IsNull("DESCRIPTION"))
						tnView.Insert(0,new DBTreeNode(dr["DESCRIPTION"].ToString()));
					DBTreeNode tnDefinition = tnView.Add("Definition");
					clsSQLParser objParser = new clsSQLParser (mDAL);
					objParser.parseSQL(strDefinition,tnDefinition);
					tnDefinition.Tag=strDefinition;
				}
			}
		}

		private void schemaProcedures(DBTreeNode parent,DataTable dtData){
			parent = parent.Add("Procedures");
			parent.Key="Procedures";
			parent.Image = enumImages.Procedure ;
			DBTreeNode tnProcedure;
			clsSQLParser objParser;
			foreach (DataRow dr in dtData.Rows){
				string strName = dr["PROCEDURE_NAME"].ToString() ;
				tnProcedure=parent.Add(strName);
				tnProcedure.Image = enumImages.Procedure  ;
				
				tnProcedure.Add("Typ: "+dr["PROCEDURE_TYPE"].ToString());
				if (!dr.IsNull("DESCRIPTION"))
					tnProcedure.Add(dr["DESCRIPTION"].ToString());
				DBTreeNode tnSQL = tnProcedure.Add("Definition");

				objParser = new clsSQLParser (mDAL);
				objParser.parseSQL( dr["PROCEDURE_DEFINITION"].ToString(),tnSQL);
				tnSQL.Tag = dr["PROCEDURE_DEFINITION"].ToString();
			}
			
		}
		
		// constraint Column Usage
		private void schemaUsageConstraints(DBTreeNode parent,DataTable dtData){
			DBTreeNode tnConstraint;
			foreach (DataRow dr in dtData.Rows){
				tnConstraint = null;
				
				string strName = dr["CONSTRAINT_NAME"].ToString() ;
				string strTable = dr["TABLE_NAME"].ToString();
				string strColumn = dr["COLUMN_NAME"].ToString();
				TableNode tnTable = null;
				foreach (DBTreeNode tnGroup in parent){
					if (tnGroup[strTable] != null){
						tnTable=(TableNode)tnGroup[strTable];
						break;
					}
				}
				if (tnTable == null){
					tnTable=new TableNode();
					tnTable.Text=strTable;
					tnTable.Key=strTable;
					tnTable.Image=enumImages.SystemTable;
					if (parent["Tabellen"] != null)
						parent["Tabellen"].Add(tnTable);
				}
				if (tnTable[COLUMNS] == null)
					tnTable.Add(COLUMNS,COLUMNS,enumImages.Column );
				if(tnTable[COLUMNS][strColumn ] == null)
					tnTable[COLUMNS].Add(strColumn,strColumn,enumImages.Column);
				
				if ((tnConstraint = tnTable[COLUMNS][strColumn][strName]) == null) {
					tnConstraint = tnTable[COLUMNS][strColumn].Add(strName,strName,enumImages.Condition);
				}
				
				this.AddConstraintProperties(tnConstraint,dr);
				
			}
		}
		
		// reference Constraints
		private void schemaRefConstraints(DBTreeNode parent,DataTable dtData){
			DBTreeNode tnConstraint;
			DBTreeNode tnCommon;
			
			tnCommon = new DBTreeNode("Bedingungen "+clsDAL.getSchemaName(OleDbSchemaGuid.Referential_Constraints));
			tnCommon.Key= "Constraints";
			tnCommon.Image=enumImages.Condition;
			
			foreach (DataRow dr in dtData.Rows){

				string strName = dr["CONSTRAINT_NAME"].ToString() ;
				
				DBTreeNode[]tnList = parent.Find(strName);

				tnConstraint = null;
				if (tnList.Length ==0){
					clsLogging.LOG(enumMsgType.Fail,"Gesucht: "+strName,"");
				}else if (tnList.Length ==1){
					tnConstraint = tnList[0];
				}else{
					string strMsg ="Mehrere Kandidaten Gefunden:";
					DBTreeNode tnPath;
					foreach(DBTreeNode tn in tnList){
						tnPath = tn;
						string strPath = tnPath.Text;
						while (tnPath.Parent !=null){
							tnPath =tnPath.Parent;
							strPath = tnPath.Text+" / "+strPath;
						}
						strMsg+="\n"+strPath;
					}
					clsLogging.LOG(enumMsgType.Success,"Gesucht: "+strName,strMsg);
					tnConstraint = tnCommon[strName];
					if (tnConstraint == null){
						tnConstraint=tnCommon.Add(strName,strName,enumImages.Condition);
					}
				}
				
				this.AddConstraintProperties(tnConstraint,dr);
			}
			if (tnCommon.Count>0)
				parent.Add(tnCommon);

		}

		private void ForeignKeyConstraints(DataRow dr,DBTreeNode parent)
		{
			DBTreeNode tnConstraint = null;
			DBTreeNode tnTable=null;
			
			string strTable = dr["TABLE_NAME"].ToString();
			
			foreach (DBTreeNode tnGroups in parent  ){
				tnTable=tnGroups[strTable ];
				if (tnTable != null)
					break;
			}
			
			string strConstraint = dr["CONSTRAINT_NAME"].ToString();
			
			// wenn es mehrere gleichnamige FKs gibt, zusammenfassen.
			List<DBTreeNode> lstConstraints =null;
			lstConstraints = new List<DBTreeNode>() ;

			foreach( DBTreeNode tn in tnTable[COLUMNS]){
				tnConstraint = tn[strConstraint];
				if( tnConstraint != null ){
					lstConstraints.Add(tnConstraint);
				}
			}
			
			if (lstConstraints.Count == 1)
				tnConstraint = lstConstraints[0];
			else if (lstConstraints.Count > 0){
				string fk ="Foreign Keys";
				tnConstraint = tnTable[fk];
				if (tnConstraint==null)
					tnConstraint = tnTable.Add(fk,fk);

				foreach (DBTreeNode group in lstConstraints){
					foreach( DBTreeNode tn in group){
						tn.Parent.Remove(tn);
						tnConstraint.Add(tn);
					}
				}
			}else{
				tnConstraint =  tnTable.Add(strConstraint,strConstraint,enumImages.Unknown);
			}
			
			this.AddConstraintProperties(tnConstraint,dr);
		}
		
		private void CellValueConstraints(DataRow dr,DBTreeNode parent){
			DBTreeNode tnTable=null;
			DBTreeNode tnConstraint = null;
			string strColumn=null;
			string strTable = dr["TABLE_NAME"].ToString();
			string strConstraint = dr["CONSTRAINT_NAME"].ToString();
			string[] strParts = strConstraint.Split('.');
			
			// zugeh�rige Tabelle ermitteln.
			foreach (DBTreeNode tnGroups in parent  ){
				tnTable=tnGroups[strTable ];
				if (tnTable != null)
					break;
			}
			
			for(int i=0; i+1< strParts.Length ;i++){
				strParts[i]=strParts[i].Trim("[]".ToCharArray());
			}
			if (strParts.Length == 2){
				strColumn = strParts[0];
				strConstraint =strParts[1];
				
				tnConstraint = tnTable[COLUMNS][strColumn][strConstraint];
				if (tnConstraint == null){
					tnConstraint=tnTable[COLUMNS][strColumn].Add(strConstraint,strConstraint,enumImages.Star );
				}
				tnConstraint.Image=enumImages.Check ;
			} else {
				throw new Exception ("Check exceptions sollten im  Constraint name genau einen Punkt haben.");
			}

			this.AddConstraintProperties(tnConstraint,dr);
		}
		
	
		private void UnknownContraintTypes(DataRow dr,DBTreeNode parent){
			string strName = dr["CONSTRAINT_NAME"].ToString();
			string strType = dr["CONSTRAINT_TYPE"].ToString();
			string strTable = dr["TABLE_NAME"].ToString();
		
			// Betroffene Tabelle suchen:
			DBTreeNode tnTable = null;
			foreach (DBTreeNode tnGroups in parent  ){
				tnTable=tnGroups[strTable ];
				if (tnTable != null)
					break;
			}
			
			// Falls betroffene Tabelle nicht vorhanden: Einf�gen
			if (tnTable == null)
				tnTable = parent["Tables"].Add(strTable,strTable,enumImages.SystemTable);
			// hinweis auf unbekannte Constraint-Art
			tnTable.Add(strType+"."+strName,"?) "+strType+": \"" + strName +"\"",enumImages.Unknown);
		}
		//table Constraints

		// Prim�rschl�ssel, eindeutige Schl�ssel, Fremdschl�ssel, Anforderungen (Wert-Konstraints)
		private void schemaTableConstraints(DBTreeNode parent,DataTable dt){
			foreach (DataRow dr in dt.Rows){
				switch(dr["CONSTRAINT_TYPE"].ToString()){
					case "PRIMARY KEY":
					case "UNIQUE":
						break;
					case "FOREIGN KEY":
						ForeignKeyConstraints(dr, parent);
						break;
					case "CHECK":
						CellValueConstraints(dr, parent);
						break;
					default:
						UnknownContraintTypes(dr, parent);
						break;
				}

			}
		}

		// Check_Constraints
		private void schemaCheckConstraints(DBTreeNode parent,DataTable dtData){
			DBTreeNode tnConstraint;
			foreach (DataRow dr in dtData.Rows){

				string strName = dr["CONSTRAINT_NAME"].ToString() ;
				string[] strParts = strName.Split('.');
				
				string strTable = strParts[0].Trim("[]".ToCharArray());
				string strColumn = strParts[1].Trim("[]".ToCharArray());
				string strConstraint = strParts[2].Trim("[]".ToCharArray());
				
				DBTreeNode tnTable=null;
				foreach (DBTreeNode group in parent){
					tnTable = group[strTable];
					if (tnTable != null)
						break;
				}
				tnConstraint = tnTable[COLUMNS][strColumn][strConstraint];
				if (tnConstraint.Parent.Image == enumImages.Column )
					tnConstraint.Parent.Image=enumImages.Condition;
				
				if (tnTable == null){
					strName="["+strColumn+"]."+strConstraint;
					tnTable = parent["Tabellen"].Add(strName,strName,enumImages.SystemTable );
					tnTable = tnTable.Add(COLUMNS,COLUMNS,enumImages.Column);
					tnConstraint = tnTable
						.Add(strColumn,strColumn,enumImages.Column)
						.Add(strConstraint,strConstraint,enumImages.Check) ;
				}
				
				
				if (!dr.IsNull("DESCRIPTION")){
					string strDescription = dr["DESCRIPTION"].ToString();
					tnConstraint.Add('"'+strDescription+'"');
				}
				this.AddConstraintProperties(tnConstraint,dr);
				
			}
			
		}

		
		//called by SchemaColumns only
		private void columnType (System.Data.DataRow dr,DBTreeNode tnColumn ){
			int intTypeCode =int.Parse(dr["DATA_TYPE"].ToString());
			try{
				Dictionary <string,TypeNode> dicType = TypeNode.Types[intTypeCode]  ;
				if (dicType.Count == 1){
					foreach(TypeNode tn in  dicType.Values ){
						tnColumn.Add((TypeNode)tn.Clone());
					}
				}else{
					
					int intCurrentLength = int.Parse(dr["CHARACTER_OCTET_LENGTH"].ToString()) ;
					TypeNode bestFit = null;
					foreach (TypeNode tnSubType in dicType.Values ){
						int ColumnSize = tnSubType.MaxLength;
						int intSubTypeLength = tnSubType.MaxLength  ;
						
						if (intSubTypeLength >= intCurrentLength ){
							if (bestFit == null){
								bestFit = tnSubType;
							} else {
								int intBestLength = ColumnSize;
								
								if (intBestLength > intSubTypeLength )
									bestFit = tnSubType;
							}
						}
					}
					if (bestFit == null){
						tnColumn.Add("Type is not Known") ;
					}
					else
					{
						bestFit =(TypeNode)bestFit.Clone();
						tnColumn.Add(bestFit);
						bestFit.CurrentLength =intCurrentLength;
					}
				}
				
			}catch(Exception ex){
				clsLogging.GEH(ex);
				tnColumn.Add("Unbekannter Datentyp: "+intTypeCode.ToString ());
			}
		}

		#endregion
		
	}


}
