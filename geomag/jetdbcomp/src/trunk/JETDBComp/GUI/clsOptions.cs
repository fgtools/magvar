﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.Drawing;
using System.Windows.Forms;
#endregion

namespace JETDBComp.GUI
{
	public partial class clsOptions : Form
	{
		public clsOptions()
		{
			InitializeComponent();
			this.cbProvider.Items.Add ("Microsoft.JET.OleDB.4.0");
			this.cbProvider.Items.Add ("Microsoft.ACE.OleDB.12.0");
		}
		public bool SystemTables{
			get {
				return this.cbSystem.Checked; 
			}
			set {
				this.cbSystem.Checked = value;
			}
		}
		
		public bool AccessTables{
			get{
				return this.cbAccess.Checked; 
			}
			set {
				this.cbAccess.Checked = value;
			}
		}
		
		public bool Table{
			get{
				return this.cbTable.Checked;
			}
			set {
				this.cbTable.Checked = value;
			}
		}
		
		public bool Views{
			get{
				return this.cbQueries.Checked;
			}
			set {
				this.cbQueries.Checked = value;
			}
		}
		
		public bool Procedures{
			get{
				return this.cbProc.Checked;
			}
			set { 
				this.cbProc.Checked = value;
			}

		}
		
		public string Provider{
			get{
				return this.cbProvider.Text;
			}
			set {
				this.cbProvider.Text = value;
			}
		}
		
		public bool Order{
			get{
				return this.cbOrder.Checked;
			}
			set {
				this.cbOrder.Checked = value;
			}
		}
		
		public bool Description{
			get{
				return this.cbDescription.Checked;  
			}
			set {
				this.cbDescription.Checked = value;
			}
		}
		
		public bool CompareData{
			get{
				return this.cbData.Checked;
			}
			set {
				this.cbData.Checked = value;
			}
		}

		public bool DifferencesOnly{
			get{
				return this.cbDifferences.Checked;
			}
			set {
				this.cbDifferences.Checked = value;
			}
		}

		
		
		
		void CbProviderSelectedIndexChanged(object sender, EventArgs e)
		{
			
		}
	}
}
