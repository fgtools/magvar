﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace JETDBComp.GUI
{
	partial class clsCKDialog
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(clsCKDialog));
			this.cblColumns = new System.Windows.Forms.CheckedListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnAbort = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cblColumns
			// 
			this.cblColumns.FormattingEnabled = true;
			this.cblColumns.Location = new System.Drawing.Point(2, 45);
			this.cblColumns.Name = "cblColumns";
			this.cblColumns.Size = new System.Drawing.Size(336, 184);
			this.cblColumns.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(0, 3);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(339, 42);
			this.label1.TabIndex = 1;
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.MouseLeave += new System.EventHandler(this.Label1MouseLeave);
			this.label1.MouseEnter += new System.EventHandler(this.Label1MouseEnter);
			// 
			// btnOk
			// 
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Location = new System.Drawing.Point(196, 239);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(142, 29);
			this.btnOk.TabIndex = 2;
			this.btnOk.Text = "Ok, erneut vergleichen.";
			this.btnOk.UseVisualStyleBackColor = true;
			// 
			// btnAbort
			// 
			this.btnAbort.DialogResult = System.Windows.Forms.DialogResult.Abort;
			this.btnAbort.Location = new System.Drawing.Point(2, 239);
			this.btnAbort.Name = "btnAbort";
			this.btnAbort.Size = new System.Drawing.Size(75, 29);
			this.btnAbort.TabIndex = 3;
			this.btnAbort.Text = "Abbruch";
			this.btnAbort.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Location = new System.Drawing.Point(-5, 233);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(350, 2);
			this.label2.TabIndex = 5;
			this.label2.Text = "label2";
			// 
			// clsPKDialog
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(340, 270);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.btnAbort);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cblColumns);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(348, 297);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(348, 297);
			this.Name = "clsPKDialog";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "Vergleichsschlüssel wählen";
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.CheckedListBox cblColumns;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnAbort;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Label label1;
	}
}
