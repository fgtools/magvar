﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace JETDBComp.GUI
{
	partial class uctlDBNamePanel
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		public uctlDBNamePanel()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblDescription = new System.Windows.Forms.Label();
			this.txtFilename = new System.Windows.Forms.TextBox();
			this.btnOpen = new System.Windows.Forms.Button();
			this.helpProvider1 = new System.Windows.Forms.HelpProvider();
			this.SuspendLayout();
			// 
			// lblDescription
			// 
			this.lblDescription.AllowDrop = true;
			this.lblDescription.Location = new System.Drawing.Point(3, 5);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(70, 18);
			this.lblDescription.TabIndex = 5;
			this.lblDescription.Text = "Filename";
			this.lblDescription.DragDrop += new System.Windows.Forms.DragEventHandler(this.FileDragDrop);
			this.lblDescription.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FileDragBegin);
			this.lblDescription.DragEnter += new System.Windows.Forms.DragEventHandler(this.FileDragEnter);
			// 
			// txtFilename
			// 
			this.txtFilename.AllowDrop = true;
			this.txtFilename.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.txtFilename.Location = new System.Drawing.Point(51, 2);
			this.txtFilename.Name = "txtFilename";
			this.txtFilename.Size = new System.Drawing.Size(114, 20);
			this.txtFilename.TabIndex = 4;
			this.txtFilename.DragDrop += new System.Windows.Forms.DragEventHandler(this.FileDragDrop);
			this.txtFilename.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtFilenameKeyUp);
			this.txtFilename.DragEnter += new System.Windows.Forms.DragEventHandler(this.FileDragEnter);
			// 
			// btnOpen
			// 
			this.btnOpen.AllowDrop = true;
			this.btnOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOpen.Location = new System.Drawing.Point(168, 2);
			this.btnOpen.Margin = new System.Windows.Forms.Padding(0);
			this.btnOpen.Name = "btnOpen";
			this.btnOpen.Size = new System.Drawing.Size(22, 19);
			this.btnOpen.TabIndex = 3;
			this.btnOpen.Text = "...";
			this.btnOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnOpen.UseVisualStyleBackColor = true;
			this.btnOpen.Click += new System.EventHandler(this.Open);
			this.btnOpen.DragDrop += new System.Windows.Forms.DragEventHandler(this.FileDragDrop);
			this.btnOpen.DragEnter += new System.Windows.Forms.DragEventHandler(this.FileDragEnter);
			// 
			// uctlDBNamePanel
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.txtFilename);
			this.Controls.Add(this.lblDescription);
			this.Controls.Add(this.btnOpen);
			this.Name = "uctlDBNamePanel";
			this.Size = new System.Drawing.Size(192, 24);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FileDragDrop);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FileDragBegin);
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.FileDragEnter);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.HelpProvider helpProvider1;
		private System.Windows.Forms.Button btnOpen;
		private System.Windows.Forms.TextBox txtFilename;
		private System.Windows.Forms.Label lblDescription;
	}
}
