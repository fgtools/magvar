﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using JETDBComp.Model;
using System.Collections.Generic;
using System.Threading;
using System.Data;
#endregion

namespace JETDBComp.GUI
{
	/// <summary>
	/// Description of uctlStructure.
	/// </summary>
	public partial class uctlStructure : UserControl
	{

		#region "private member"
		private const string DATATABLE_ARRAY ="System.Data.DataTable[]";
		private DBTreeNode mtnMaster;
		private DBTreeNode mtnTarget;
		private clsBusinessLayer mobjBL; 
		#endregion
		
		public void Init(clsBusinessLayer bl){
			mobjBL = bl;
		}
			
		public delegate void dlgShowResult(DBTreeNode resultTree);
		
		private void ShowResult(DBTreeNode resultTree){
			this.treeView1.Nodes.Clear();
			foreach (DBTreeNode mtn in resultTree)
				this.treeView1.Nodes.Add(mtn.GUI(true));
			
		}
		
		private void Calculate(){
			int differences = 0;
			try{
				clsCompare objCompare = new clsCompare(this.mobjBL.FilenameMaster,this.mobjBL.FilenameTarget);
				clsCompare.OptionCompareData= this.mobjBL.OptionData;
				differences = objCompare.compare(mtnMaster, mtnTarget);
				if (clsCompare.OptionOnlyDifferences){
					List<DBTreeNode> nodes = new List<DBTreeNode>();
					nodes.Add(mtnMaster);
					nodes.Add(mtnTarget);
					do{
						for (int i = nodes[0].Count -1 ;i >= 0 ; i--){
							DBTreeNode tn =  nodes[0][i];
							if (tn.State == enumStates.Ok){
								nodes[0].Remove(tn);
							} else nodes.Add(tn);
						}
						nodes.Remove(nodes[0]);
					} while ( nodes.Count > 0);
				}
			} catch(Exception ex){
				clsLogging.GEH(ex);
			}finally {
				clsLogging.LOG(enumMsgType.Info,"Vergleich",differences.ToString()+" Unterschiede");
			}
			clsLogging.LOG(enumMsgType.Info, "Fertig Berechnet","innerhalb des Thread");
			try{
				if (InvokeRequired)
					this.BeginInvoke(new dlgShowResult(ShowResult),new object[]{mtnMaster});
				else
					ShowResult(mtnMaster);
			} catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		
		public void SetFileNames(string[] filenames){

			if (this.mobjBL.FilenameMaster == filenames[0])
				filenames[0] = null;
			if (this.mobjBL.FilenameTarget == filenames[1])
				filenames[1] = null;
			
			if (filenames[0] != null){
				this.mobjBL.FilenameMaster = filenames[0];
				if (!(mtnMaster == null))
					mtnMaster.Clear();
				mtnMaster = new DBTreeNode();
			}
			if (filenames[1] != null){
				this.mobjBL.FilenameTarget = filenames[1];
				if (!(mtnTarget == null))
					mtnTarget.Clear();
				mtnTarget = new DBTreeNode();
			}
		}
		
		public void Compare(){
			if (mobjBL == null)return;
			if ((this.mobjBL.FilenameMaster == null)||(this.mobjBL.FilenameMaster.Length <1)||(this.mobjBL.FilenameTarget == null)||(this.mobjBL.FilenameTarget.Length <1)){
				MessageBox.Show("Wählen Sie zunächst die Datenbanken!");
				return;
			}
			//begin in parallel
			analyse(this.mobjBL.FilenameMaster,mtnMaster);
			analyse(this.mobjBL.FilenameTarget,mtnTarget);
			//end in parallel
			Calculate();
		}
		
		private void analyse( string filename,DBTreeNode representation){
			if (System.IO.File.Exists(filename))
				this.mobjBL.analyse(representation, filename);
			if (this.mobjBL.OptionData){
				clsLoadData objLoad= new clsLoadData();
				objLoad.loadAllTablesData(representation,filename);
			}
		}
		
		#region "event-handler"
		private void CloseClick(object sender, EventArgs e){
			this.treeView1.CollapseAll();
		}
		
		private void ExpandClick(object sender, EventArgs e){
			this.treeView1.ExpandAll();
		}
		
		
		// zeigt das ausgewählte Baumstruktur-Element in der Tabelle an.
		void TreeView1AfterSelect(object sender, TreeViewEventArgs e){
			this.uctlDBGrid1.SuspendLayout();
			DataTable olddata = null;
			DataTable newdata = null;
			try{
				object data  = treeView1.SelectedNode.Tag;
				if (typeof(TableNode).IsInstanceOfType(data)){
					object objTag =((TableNode)data).Tag;
				}
				if (typeof(DBTreeNode).IsInstanceOfType(data)){
					DBTreeNode tnNode = (DBTreeNode)data;
					if (tnNode !=null){
						DBTreeNode tn =tnNode;
						while ((tn !=null)&&(tn.State == enumStates.None))
							tn =tn.Parent;
						if ((tnNode.Tag != null)&&(tnNode.Tag.GetType().ToString().Equals(DATATABLE_ARRAY)) ) {
							DataTable[] tables = (DataTable[])tnNode.Tag;
							if (tables.Length == 3){
								this.uctlDBGrid1.SetDataSources(((DataTable[])tnNode.Tag)[0],
								                                ((DataTable[])tnNode.Tag)[1],
								                                ((DataTable[])tnNode.Tag)[2]);
								return;
							}else{
								olddata=((DataTable[])tnNode.Tag)[0];
								newdata=((DataTable[])tnNode.Tag)[1];
							}
						}else if((tn!=null)&&(tn.State == enumStates.New )){
							newdata = JETDBComp.Model.clsDataTable.GetValidData(tnNode.Tag);
							if (newdata == null)
								newdata = JETDBComp.Model.clsDataTable.GetValidData(tnNode);
						}else {
							olddata = JETDBComp.Model.clsDataTable.GetValidData(tnNode.Tag);
							if (olddata == null)
								olddata = JETDBComp.Model.clsDataTable.GetValidData(tnNode);
						}
					}
					DataTable result = new DataTable();
					JETDBComp.Model.clsDataTable.compareDataTable((DataTable)olddata,(DataTable)newdata,ref result ,true);
					tnNode.Tag = new DataTable[]{olddata,newdata,result};
					this.uctlDBGrid1.SetDataSources((DataTable)olddata,
					                                (DataTable)newdata,
					                                result);
				}
			}catch (Exception ex){
				clsLogging.GEH(ex);
			}finally{
				this.uctlDBGrid1.ResumeLayout();
			}
		}
		#endregion
		
	}
}
