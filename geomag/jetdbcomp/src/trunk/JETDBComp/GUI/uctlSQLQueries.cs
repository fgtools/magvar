/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas M�ller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#region using
using System;
using System.ComponentModel;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using JETDBComp.Model;
#endregion

namespace JETDBComp.GUI
{
	public partial class uctlSQLQueries : UserControl{

		// the connection to the databasefile
		private clsDAL mobjDal;
		
		#region private methods
		// executes a single sql-command
		private void executeQuery(string strSQL){
			try{
				if (strSQL.Trim().ToUpper().StartsWith("SELECT")){
					mdgQuery.DataSource = mobjDal.query(strSQL);
				}else{
					// UDATE, INSTERT, DELETE
					// CREATE, DROP, ALTER
					mdgQuery.DataSource = null;
					mobjDal.update(strSQL);
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		
		// splits an SQL-Script into an array of single commands.
		private string[] SplitIntoCommands(string strSQL){
			List<string> result = new List<string>();
			try{
				string[] strLines =	strSQL.Split(new char[]{'\n'},StringSplitOptions.RemoveEmptyEntries);
				
				strSQL="";
				foreach (string strLine in strLines){
					// remove comments.
					if (!strLine.Trim().StartsWith("--")){
						if (strSQL=="")
							strSQL=strLine;
						else
							strSQL+="\n"+strLine.Trim();
					}
					// execute the first / n-th command
					if (strSQL.EndsWith(";")){
						if (strSQL.Length > 1)
							result.Add(strSQL);
						strSQL="";
					}
				}
				if (strSQL.Length > 1)
					result.Add(strSQL);
				
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
			return result.ToArray();
		}
		#endregion

		// define, which databases can be inspected. 
		public void SetDatabaseList(string[] values){
			this.cbFilenames.Items.Clear();
			this.cbFilenames.Items.AddRange(values);
		}
	
		#region event handler
		// analyses the input and calls "executeQuery" for each sql-command
		private void BtnQueryClick(object sender, EventArgs e){
			string strSQL;
			string strFilename;
			try{
				strFilename= this.cbFilenames.Text;
				if (System.IO.File.Exists(strFilename)){
					mobjDal.Filename = strFilename;
					// first try to execute the selected part.
					strSQL = txtQuery.SelectedText.Trim();
					if (strSQL.Equals("")){
						// if nothing is selected, select all.
						strSQL= txtQuery.Text.Trim();
					}
					foreach (string strCommand in SplitIntoCommands(strSQL)){
						this.executeQuery(strCommand);
					}
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		
		// initializes the Data Abstraction Layer
		private void UctlSQLQueriesLoad(object sender, EventArgs e)
		{
			this.mobjDal = new clsDAL();
		}
		
		//strg-Enter executes the command:
		private void TxtQueryKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Modifiers == Keys.Control )
				if (e.KeyCode == Keys.Enter)
			{
				e.Handled=true;
				this.btnQuery.Select();
				BtnQueryClick(null,null);
			}
		}
		#endregion
	}
}
