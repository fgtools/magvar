﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Data;
using JETDBComp.Model;
#endregion

namespace JETDBComp.GUI
{
	public partial class uctlSchemata : UserControl
	{
		public uctlSchemata()
		{
			mDAL = new clsDAL();
			InitializeComponent();	
		}

		private clsDAL mDAL = null;
		private Dictionary<Guid,DataTable> tables = null;

		public void SetFilenames (String[] aryDatabaseNames){
			cbDatabase.Items.Clear();
			cbDatabase.Items.AddRange(aryDatabaseNames);
		}
		
		private void CbSchemaSelectedIndexChanged(object sender, EventArgs e)
		{
			try{
				object item = cbSchema.SelectedItem;
				clsSchemaPair pair = (clsSchemaPair)item;
				this.lblTableName.Text = pair.Data.ToString();
				dgvTable.DataSource=pair.Data;
			}catch (Exception ex){
				clsLogging.GEH(ex);
			}
		}
		
		private void CbDatabaseSelectedIndexChanged(object sender, EventArgs e){
			try{
				cbSchema.Items.Clear();
				if (cbDatabase.SelectedItem.ToString() != ""){
					mDAL.loadMetadata(cbDatabase.SelectedItem.ToString());
					tables=	mDAL.Metadata;
					foreach (Guid Key in tables.Keys){
						cbSchema.Items.Add(new clsSchemaPair(Key, tables[Key] ));
					}
				}
			}catch (Exception ex){
				clsLogging.GEH(ex);
			}
		}
		
	}
	
	class clsSchemaPair{
		private Guid mguidID;
		private string mstrName;
		private DataTable mdtData;
		
		//contructor
		public clsSchemaPair(Guid ID, DataTable Data){
			mguidID = ID;
			mdtData = Data;
			mstrName = clsDAL.getSchemaName(ID);
		}
		
		public override string ToString(){
			return mstrName;
		}
		public DataTable Data{
			get {
				return mdtData;
			}
		}
	}
}
