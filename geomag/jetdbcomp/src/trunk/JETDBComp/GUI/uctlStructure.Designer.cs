﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 #region using
 using JETDBComp.Model;
 #endregion
namespace JETDBComp.GUI
{
	partial class uctlStructure
	{
		
		
		public uctlStructure()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			mobjBL.Dispose();
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uctlStructure));
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel1 = new System.Windows.Forms.Panel();
			this.uctlDBGrid1 = new JETDBComp.GUI.uctlDBGrid();
			this.splitter2 = new System.Windows.Forms.Splitter();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.milStatus = new System.Windows.Forms.ImageList(this.components);
			this.menuStrip2 = new System.Windows.Forms.MenuStrip();
			this.mnuClose = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.menuStrip2.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitter1
			// 
			this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.splitter1.Location = new System.Drawing.Point(213, 0);
			this.splitter1.MinSize = 31;
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(5, 370);
			this.splitter1.TabIndex = 8;
			this.splitter1.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.uctlDBGrid1);
			this.panel1.Controls.Add(this.splitter2);
			this.panel1.Controls.Add(this.textBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(218, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(285, 370);
			this.panel1.TabIndex = 9;
			// 
			// uctlDBGrid1
			// 
			this.uctlDBGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.uctlDBGrid1.Location = new System.Drawing.Point(0, 0);
			this.uctlDBGrid1.Name = "uctlDBGrid1";
			this.uctlDBGrid1.Padding = new System.Windows.Forms.Padding(1);
			this.uctlDBGrid1.Size = new System.Drawing.Size(285, 282);
			this.uctlDBGrid1.TabIndex = 9;
			// 
			// splitter2
			// 
			this.splitter2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitter2.Location = new System.Drawing.Point(0, 282);
			this.splitter2.MinSize = 31;
			this.splitter2.Name = "splitter2";
			this.splitter2.Size = new System.Drawing.Size(285, 5);
			this.splitter2.TabIndex = 8;
			this.splitter2.TabStop = false;
			// 
			// textBox1
			// 
			this.textBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.textBox1.Location = new System.Drawing.Point(0, 287);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(285, 83);
			this.textBox1.TabIndex = 10;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.treeView1);
			this.panel2.Controls.Add(this.menuStrip2);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(213, 370);
			this.panel2.TabIndex = 12;
			// 
			// treeView1
			// 
			this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView1.Location = new System.Drawing.Point(0, 24);
			this.treeView1.Name = "treeView1";
			this.treeView1.Size = new System.Drawing.Size(213, 346);
			this.treeView1.StateImageList = this.milStatus;
			this.treeView1.TabIndex = 1;
			this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView1AfterSelect);
			// 
			// milStatus
			// 
			this.milStatus.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("milStatus.ImageStream")));
			this.milStatus.TransparentColor = System.Drawing.Color.Transparent;
			this.milStatus.Images.SetKeyName(0, "none.ico");
			this.milStatus.Images.SetKeyName(1, "Ok.ico");
			this.milStatus.Images.SetKeyName(2, "removed.ico");
			this.milStatus.Images.SetKeyName(3, "New.ico");
			this.milStatus.Images.SetKeyName(4, "changed.ico");
			// 
			// menuStrip2
			// 
			this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.mnuClose,
									this.mnuOpen});
			this.menuStrip2.Location = new System.Drawing.Point(0, 0);
			this.menuStrip2.Name = "menuStrip2";
			this.menuStrip2.Size = new System.Drawing.Size(213, 24);
			this.menuStrip2.TabIndex = 13;
			this.menuStrip2.Text = "menuStrip2";
			// 
			// mnuClose
			// 
			this.mnuClose.Name = "mnuClose";
			this.mnuClose.Size = new System.Drawing.Size(77, 20);
			this.mnuClose.Text = "- schließen";
			this.mnuClose.Click += new System.EventHandler(this.CloseClick);
			// 
			// mnuOpen
			// 
			this.mnuOpen.Name = "mnuOpen";
			this.mnuOpen.Size = new System.Drawing.Size(65, 20);
			this.mnuOpen.Text = "+ öffnen";
			this.mnuOpen.Click += new System.EventHandler(this.ExpandClick);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
			// 
			// uctlStructure
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel2);
			this.Name = "uctlStructure";
			this.Size = new System.Drawing.Size(503, 370);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.menuStrip2.ResumeLayout(false);
			this.menuStrip2.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.ToolStripMenuItem mnuClose;
		private System.Windows.Forms.ToolStripMenuItem mnuOpen;
		private System.Windows.Forms.ImageList milStatus;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.MenuStrip menuStrip2;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Splitter splitter2;
		private JETDBComp.GUI.uctlDBGrid uctlDBGrid1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.TreeView treeView1;
	}
}
