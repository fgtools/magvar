﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using JETDBComp.Model;
#endregion

namespace JETDBComp.GUI
{
	public partial class uctlDBNamePanel : UserControl
	{
		/// <summary>
		///  this member contains a Filename of an existing File or Directory
		///  or it contains an empty String.
		/// </summary>
		private string mstrFilename;
		
		public event System.EventHandler FileChanged;

		// Every instance of this class should use the same File Dialog:
		private static System.Windows.Forms.OpenFileDialog openFileDialog = null;
		
		#region public properies
		/// <summary>
		/// public property
		/// 
		/// Accepts only correct Filenames and Empty Strings.
		/// </summary>
		public string Filename{
			get{
				return mstrFilename;
			}
			set{
				if ( (value == null) || (value=="")){
					this.txtFilename.Text="";
					mstrFilename="";
				}else if (System.IO.File.Exists(value)){
					this.txtFilename.Text = value;
					mstrFilename=value;
					if (FileChanged != null)
						FileChanged(this,System.EventArgs.Empty);
				}
			}
		}
		
		/// <summary>
		/// this property sets the Text on the left hand side of the input field.
		/// </summary>
		public string Label{
			get{
				return this.lblDescription.Text;
			}
			set{
				this.lblDescription.Text=value;
			}
		}
		
		#endregion
		
		#region drag n drop
		
		// Takes the Dropped File
		void FileDragDrop(object sender, DragEventArgs e)
		{
			try{
				string[] aryFilenames = (string[])e.Data.GetData("FileDrop");
				this.Filename = aryFilenames[0];
				clsLogging.LOG(enumMsgType.Info,"File Dropped",this.Label +" Filename:  " + aryFilenames[0]);
			}catch (Exception ex){
				clsLogging.GEH(ex);
			}
			
		}
		
		// Allows Drag, if exactly one File is dragged.
		void FileDragEnter(object sender, DragEventArgs e)
		{
			try{
				int intNumberOfFiles = ((string[])e.Data.GetData("FileDrop")).Length;
				if (intNumberOfFiles == 1)
					e.Effect = DragDropEffects.Copy;
				else
					e.Effect = DragDropEffects.None;
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		
		// BEGIN DRAG Copying the file by pulling the Label out of the application
		private void FileDragBegin(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			try{
				// defining the list of filenames to be copied
				System.Collections.Specialized.StringCollection scFilenames;
				scFilenames = new System.Collections.Specialized.StringCollection();

				// filling the list of filenames
				scFilenames.Clear();
				scFilenames.Add(mstrFilename);
				
				// creating an OLE objekt
				Microsoft.VisualStudio.Shell.OleDataObject obj;
				obj = new Microsoft.VisualStudio.Shell.OleDataObject();
				
				// adding the list of filenames into the OLE-object
				obj.SetFileDropList(scFilenames);
				
				// initiating the Drag function using the created OLE Object
				this.DoDragDrop(obj, DragDropEffects.Copy );
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}

		#endregion

		#region other eventhandler
		
		// Opens an OpenFileDialog to select an other database file
		public void Open(object sender, EventArgs e)
		{
			try{
				if (openFileDialog == null)
					uctlDBNamePanel.openFileDialog = new System.Windows.Forms.OpenFileDialog();
				
				openFileDialog.CheckFileExists = false;
				openFileDialog.Multiselect=false;
				openFileDialog.Title= "Datenbankdatei öffnen...";
				
				openFileDialog.FileName=this.mstrFilename;
				
				if (openFileDialog.ShowDialog() == DialogResult.OK){
					this.Filename = openFileDialog.FileNames[0];
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}

		#endregion
		
		void TxtFilenameKeyUp(object sender, KeyEventArgs e)
		{
			if ((e.Modifiers == 0) && ((e.KeyCode == Keys.Enter)||(e.KeyCode == Keys.Return))){
				this.Filename = this.txtFilename.Text;
			}
		}
		

	}
}
