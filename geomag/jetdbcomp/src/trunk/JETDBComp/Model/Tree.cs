﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.Collections.Generic ;
using System.Windows.Forms ;
using System.Data;
#endregion
namespace JETDBComp.Model
{
	//Symbols for the Treeview of the combined DataStructure of both Datafiles.
	public enum enumImages : int{
		None = 0, // leeres Symbol
		
		AccessTable =1,
		SystemTable =2,
		Table = 3,
		View = 4,
		Procedure = 5,
		
		Column = 6,
		PrimaryKey = 7,
		ForeignKey = 8,
		UniqKey = 9,
		Index = 10,

		Condition = 11,
		Check = 12,
		Unknown = 13,
		Star = 14,
		Link = 14
	}
	
	public enum enumStates :int{
		None=0,
		Ok=1,
		Removed=2,
		New=3,
		Changed=4,
	}
	
	public class DBTreeNode :IEnumerable<DBTreeNode>  {
		#region private attributes
		private string mstrTitle;
		private DBTreeNode mmtnParent=null;
		private string mstrKey;
		
		private bool mHidden = false;
		
		private enumImages  mImage;
		private enumStates mState;
		
		private List<DBTreeNode> children;
		private object mTag;
		#endregion

		#region properties
		public DBTreeNode Parent {
			get { return mmtnParent; }
		}

		public string Text {
			get { return mstrTitle; }
			set { mstrTitle = value; }
		}
		public string Key {
			get { return mstrKey; }
			set { mstrKey = value;
				if (value == null)
					throw new ArgumentException("the Key must not be null.");
			}
		}
		public bool Visible {
			get { return ! mHidden; }
			set { mHidden = ! value; }
		}
		public enumImages Image {
			get { return mImage; }
			set { mImage = value; }
		}
		public enumStates State {
			get { return mState; }
			set { mState = value; }
		}
		public object Tag {
			get { return mTag; }
			set { mTag = value; }
		}

		public DBTreeNode this[int index]{
			get{
				return children[index];
			}
		}
		
		public DBTreeNode this[string key]{
			get{
				foreach ( DBTreeNode tn in children )
					if (tn.Key==key)
						return tn;
				return null;
			}
		}
		
		public int Count{
			get{
				return this.children.Count;
			}
		}
		#endregion

		public virtual DBTreeNode Clone(){
			DBTreeNode result = new DBTreeNode();
			result.mstrTitle = mstrTitle;
			
			result.Key = this.Key;
			
			result.mHidden = mHidden;
			
			result.mImage = mImage;
			result.mState = mState;
			result.mTag = mTag;
			
			foreach(DBTreeNode child in children){
				result.children.Add( child.Clone());
			}
			return result;
		}
		
		
		public DBTreeNode (){
			children = new List<DBTreeNode> ();
			Key=Guid.NewGuid().ToString();
			mImage=enumImages.None ;
			mState= enumStates.None ;
			Tag=null;
		}
		public DBTreeNode (string key):this(){
			this.Key=key;
			mstrTitle=key;
		}
		public DBTreeNode (string key, enumImages image):this(){
			this.Key=key;
			mstrTitle=key;
			mImage=Image;
		}

		#region handle Children

		public List<DBTreeNode>Nodes{
			get{
				return children;
			}
		}
		public DBTreeNode Add(string strText){
			return this.Add(strText,strText);
		}
		public DBTreeNode Add(string strKey,string strText){
			DBTreeNode result = new DBTreeNode();
			result.Text=strText;
			result.Key=strKey;
			children.Add(result);
			result.mmtnParent=this;
			return result;
		}
		public DBTreeNode Add(string strKey,string strText,enumImages image){
			DBTreeNode result = this.Add(strKey,strText);
			result.Image = image;
			return result;
		}
		public int Add(DBTreeNode node){
			children.Add(node);
			node.mmtnParent = this;
			return children.IndexOf(node);
		}
		
		public void Insert(int pos,DBTreeNode node){
			children.Insert(pos,node);
			node.mmtnParent=this;
		}
		
		public void Clear(){
			foreach (DBTreeNode tn in children )
				tn.mmtnParent=null;
			children.Clear();
		}
		public void Remove(DBTreeNode[] nodes){
			foreach(DBTreeNode tn in nodes)
				this.Remove(tn);
		}
		public void Remove(DBTreeNode node){
			if (children.Contains(node)){
				children.Remove(node);
			}
		}

		public DBTreeNode[] Find(String key){
			List<DBTreeNode> result = new List<DBTreeNode>();
			
			//Depth first suche.
			foreach(DBTreeNode node in children ){
				if (node.Key.Equals(key))
					result.Add(node);
				result.AddRange(node.Find(key));
			}
			
			return result.ToArray();
		}
		#endregion
		
		public virtual TreeNode GUI( bool showhidden){
			if (mHidden &&(!showhidden ))return null;
			
			TreeNode tnGUI =  new TreeNode(mstrTitle);
			tnGUI.Name=this.Key;
			tnGUI.ImageIndex=(int)mImage;
			tnGUI.SelectedImageIndex =(int)mImage;
			tnGUI.StateImageIndex = (int)mState;
			tnGUI.Tag=this;
			if (mHidden) tnGUI.ForeColor = System.Drawing.Color.Silver ;
			
			
			foreach (DBTreeNode tn in children){
				TreeNode child = tn.GUI(showhidden);
				if (child !=null)
					tnGUI.Nodes.Add(child);
			}
			return tnGUI;
		}
		

		public IEnumerator<DBTreeNode> GetEnumerator() {
			foreach( DBTreeNode item in children ) {
				yield return item;
			}
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}

	}

	// represents a foreign key
	public class FKNode :DBTreeNode{
		
		#region private member
		
		private string mstrfkTable;
		
		private string mstrfkColumn;
		
		private string mstrpkTable;
		
		private string mstrpkColumn;
		
		private const string COLUMN = "Spalten";

		#endregion
		
		#region " Public Properties "
		public DBTreeNode refTable{
			get {
				return this[mstrpkTable];
			}
		}

		public DBTreeNode refColumn{
			get{
				return this[mstrpkTable][mstrpkColumn];
			}
		}
		#endregion

		#region constructor

		public FKNode (DataRow dr):base(){
			mstrfkTable = dr["FK_TABLE_NAME"].ToString();
			mstrfkColumn = dr["FK_COLUMN_NAME"].ToString();
			mstrpkTable =dr["PK_TABLE_NAME"].ToString();
			mstrpkColumn = dr["PK_COLUMN_NAME"].ToString();
			string strName =dr["FK_NAME"].ToString();
			this.Text = strName;
			this.Key = strName;
			this.Image=enumImages.ForeignKey;
			
			this.Add(mstrpkTable).Image= enumImages.Table;

			strName =dr["UPDATE_RULE"].ToString();
			if (!strName.Equals("NO ACTION"))
				this.Add( "On Update: "+strName);
			strName =dr["DELETE_RULE"].ToString();
			if (!strName.Equals("NO ACTION"))
				this.Add( "On Delete: "+strName);
			
		}

		protected FKNode(){}

		#endregion
		
		public void Init(DBTreeNode tnParent){
			try{
				DBTreeNode tnTable = null;
				foreach ( DBTreeNode tnTables in tnParent){
					tnTable= tnTables[mstrpkTable];
					if (tnTable != null)
						break;
				}
				DBTreeNode tnColumn;
				if (tnTable != null){
					tnColumn = tnTable[COLUMN][mstrpkColumn];
					if (tnColumn != null)
						this.refTable.Add(tnColumn.Clone());
				}
				tnTable=null;
				foreach ( DBTreeNode tnTables in tnParent){
					tnTable=tnTables[mstrfkTable];
					if (tnTable != null) break;
				}
				if (tnTable != null){
					tnColumn = tnTable[COLUMN][mstrfkColumn];
					if (tnColumn!=null){
						if (tnColumn[this.Key]!=null){
							foreach(DBTreeNode tn in tnColumn[this.Key]){
								tn.Parent.Remove(tn);
								this.Add(tn);
							}
							tnColumn.Remove(tnColumn[this.Key]);
						}
						if (this.Parent == null)
							tnColumn.Add(this);
						tnColumn.Image=enumImages.ForeignKey ;
					}
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		
	}

	// represents a table or the result of a view.
	public class TableNode : DBTreeNode{
		
		#region private member
		private DataTable mdtData;
		private DataRow mdrMetadata;
		#endregion
		
		#region public properies
		public DataTable Table{
			get{
				return mdtData;
			}
			set{
				mdtData = value;
			}
		}
		
		public DataRow Metadata{
			get{
				return this.mdrMetadata ;
			}
			set{
				this.mdrMetadata = value;
			}
		}
		#endregion

		#region constructor
		public TableNode(){
		}
		
		public TableNode(DataRow dr): base(dr != null?dr["TABLE_NAME"].ToString():"unnamed",enumImages.Table ){
			mdtData = null;
			mdrMetadata = dr;
			if (dr != null)
				this.Key = dr["TABLE_NAME"].ToString() ;
			
		}
		
		#endregion
		
		#region private methods
		private string[] collectNodeText(DBTreeNode parent){
			string str = "";
			foreach (DBTreeNode tn in parent)
				str += tn.Text;
			str = str.Trim();
			if (str[str.Length-1]==';')
				str= " "+str.Substring(0,str.Length-1);
			string[] elements = str.Split(',');
			string[] results = new string[elements.Length];
			for( int i = 0 ; i < elements.Length; i++)
				results[i]= elements[i].Trim();
			return results;
		}
		#endregion

		public void evaluatePK(){
			
			if ((this.Table != null)&&(this.Table.PrimaryKey.Length== 0)){
				List<DataColumn> pKey = new List<DataColumn>();
				foreach( DBTreeNode column in this["Spalten"]){
					if ((column.Image == enumImages.PrimaryKey )||( column["PrimaryKey"] != null ))
						pKey.Add(this.Table.Columns[column.Key]);
				}
				if (pKey.Count > 0)
					this.Table.PrimaryKey =pKey.ToArray();
				else{
					if ((this["Definition"] != null) && (this["Definition"]["GROUP"] != null)){
						string[] sqlGroup = collectNodeText(this["Definition"]["GROUP"]);
						string[] sqlSelect= collectNodeText(this["Definition"]["SELECT"]);
						
						foreach(string column in sqlGroup){
							string columnName = column.Trim();
							if (columnName.LastIndexOf('.')>=0)
								columnName = columnName.Substring(columnName.LastIndexOf('.')+1);
							if (this.Table.Columns[columnName]!= null){
								pKey.Add(this.Table.Columns[columnName]);
							}else{
								foreach (string value in sqlSelect){
									if (value.Contains(columnName)){
										columnName= value.Substring(value.LastIndexOf(' '));
										columnName = columnName.Trim(new Char[]{'[',']',' '});
										if (this.Table.Columns[columnName]!= null){
											pKey.Add(this.Table.Columns[columnName]);
										}
									}
								}
							}
						}
					}
					if(pKey.Count == 0){
						DataColumn[] all = new DataColumn[this.Table.Columns.Count];
						foreach(DataColumn dc in this.Table.Columns){
							all[dc.Ordinal]=dc;
							try{
								this.Table.PrimaryKey =all;
								break;
							}catch(Exception){
							}

						}
						
					}else{
						try{
							this.Table.PrimaryKey =pKey.ToArray();
						}catch (Exception){
							DBTreeNode gb = this.Add("GB");
							foreach (DataColumn dc in pKey){
								gb.Add(dc.ColumnName);
							}
						}
					}
				}
			}
		}

	}

	// describes the type of a column
	public class TypeNode : DBTreeNode{
		#region static properties and functions.
		private static Dictionary <int, Dictionary<string,TypeNode>> mdicTypes;

		public static Dictionary <int, Dictionary<string,TypeNode>> Types {
			get { return mdicTypes; }
		}
		
		// evaluates which datatypes are supported by the database
		public static void schemaProvidedTypes( Dictionary<Guid,DataTable> htSchemas){
			try{
				if (mdicTypes == null)
					mdicTypes = new Dictionary <int, Dictionary<string,TypeNode>>();
				DataTable dtData;
				dtData = htSchemas[System.Data.OleDb.OleDbSchemaGuid.Provider_Types];
				Dictionary<string,TypeNode> htNr;
				foreach (DataRow dr in dtData.Rows){
					
					TypeNode tn = new TypeNode(dr);
					
					if (mdicTypes.ContainsKey(tn.DataType)){
						htNr = mdicTypes[tn.DataType];
					}else{
						htNr = new Dictionary<string,TypeNode>();
						mdicTypes.Add(tn.DataType,htNr);
					}
					if (!htNr.ContainsKey (tn.TypeName))
						htNr.Add(tn.TypeName,tn);
					
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
		}
		#endregion

		#region private
		private string mstrTypeName;
		private int    mintDataType;
		private int    mintCurrentLength;
		private int    mintMaxLength;
		private string mstrPrefix;
		private string mstrSuffix;
		private bool   mblnNullable;
		private string mstrCreateParamName;
		private DataRow mdr;
		#endregion
		
		#region properties
		public int DataType {
			get { return mintDataType; }
		}
		
		public string TypeName {
			get {
				return mstrTypeName + "/" + mintMaxLength.ToString();
			}
		}
		
		public int CurrentLength {
			set {
				mintCurrentLength = value;
				this.Text = this.ToString();
			}
			get { return mintCurrentLength; }
		}
		
		public int MaxLength {
			get { return mintMaxLength; }
		}
		
		
		#endregion
		
		#region constructor and clones
		
		public override DBTreeNode Clone()
		{
			return new TypeNode(this.mdr);
		}
		
		TypeNode():base(){
		}
		
		private TypeNode(DataRow drMetaData){

			this.mdr=drMetaData;
			
			mstrTypeName = mdr["TYPE_NAME"].ToString();
			mintDataType = int.Parse(mdr["DATA_TYPE"].ToString());
			
			if (! mdr.IsNull("CREATE_PARAMS")){
				mstrCreateParamName+=mdr["CREATE_PARAMS"].ToString();
				
			}
			mintMaxLength += int.Parse(mdr["COLUMN_SIZE"].ToString());
			//mintMinLength=0;
			mintCurrentLength =0;
			
			mstrPrefix = null;
			mstrSuffix = null;
			if (! mdr.IsNull("LITERAL_PREFIX"))
				mstrPrefix =  mdr["LITERAL_PREFIX"].ToString();
			if (! mdr.IsNull("LITERAL_SUFFIX"))
				mstrSuffix = mdr["LITERAL_SUFFIX"].ToString();
			mblnNullable =  (bool)mdr["IS_NULLABLE"];
			this.Text = this.ToString();
			this.Key=this.ToString();
		}
		
		#endregion
		
		public  override string ToString(){
			string result = mstrTypeName;
			if (mstrCreateParamName != null)
				result +="("+this.CurrentLength +")";
			if (! mblnNullable)
				result += " not NULL";
			return result;
		}
		
	}

	public class ColumnNode :DBTreeNode{
		
		#region private member
		private int mintPosition;
		private DataRow mdr;
		#endregion
		
		#region public properties
		
		public int Position{
			get{
				return mintPosition;
			}
		}
		
		public DataRow DataRow {
			get {
				return mdr;
			}
		}
		#endregion
		
		#region constructors
		
		ColumnNode():base(){}
		
		public ColumnNode(DataRow drColumn):base(){
			this.Image =enumImages.Column;
			string strName= drColumn["COLUMN_NAME"].ToString();
			this.Text= strName;
			this.Key = strName;
			this.mintPosition = int.Parse(drColumn["ORDINAL_POSITION"].ToString());
			this.mdr = drColumn;
		}
		#endregion
		
		//adds a column into a Table at the right position
		public void insertInto (TableNode tnTable){
			DBTreeNode columns = tnTable["Spalten"];
			if (columns[this.Key] != null)
				return;
			int count = columns.Count;
			if (count == 0) {
				columns.Add(this);
				return;
			}
			int current = count >> 1;
			int step = count >> 2;
			int othersPosition;
			do{
				while (current >=count)  count--;
				othersPosition = ((ColumnNode)columns[current]).Position;
				if (othersPosition == this.Position)
					throw new ArgumentException ("zwei Spalten können nicht an der selben Position sein.");
				if (othersPosition > this.Position){
					current -= step;
					step >>= 1;
				}
				if (othersPosition < this.Position){
					current += step;
					step >>=1;
				}
			} while (step > 0);

			othersPosition = ((ColumnNode)columns[current]).Position;
			if (othersPosition > this.Position){
				do{
					current --;
					if (current > -1)
						othersPosition = ((ColumnNode)columns[current]).Position;
				}while((othersPosition > this.Position)&&(current>=0));
				
				columns.Insert(current+1,this);
			}else if(othersPosition == this.Position)
				throw new ArgumentException ("zwei Spalten können nicht an der selben Position sein.");
			else if (othersPosition < this.Position){
				do{
					current ++;
					if (current < columns.Count)
						othersPosition = ((ColumnNode)columns[current]).Position;
				}while((othersPosition < this.Position) && (current< columns.Count ));
				//assured: othersPosition > this.Position
				if (current == columns.Count)
					columns.Add(this);
				else
					columns.Insert(current,this);
			}
		}
	}

}
