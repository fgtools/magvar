/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas M�ller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
#endregion

namespace JETDBComp.Model
{
	public class clsDAL : IDisposable
	{
		#region "private member"
		
		// the default connection information for connecting JET Databases
		private string mstrConnectionString;
		
		private OleDbCommand mcmdOle = null;
		
		private Dictionary<Guid,DataTable> mdicMetadata = null;

		private string[] marKeywords = null;
		
		private string mstrFilename = null;
		
		#endregion
		
		#region "properties"
		
		//RO
		public Dictionary<Guid,DataTable> Metadata{
			get{
				return mdicMetadata;
			}
		}
		
		//RO
		public string[] Keywords{
			get{
				return this.marKeywords;
			}
		}
		
		public  string Filename  {
			set{
				if (System.IO.File.Exists(value))
					this.mstrFilename = value;
				else
					throw new System.IO.FileNotFoundException("The Path to the Database seems to be wrong: \n'"+value+"'");
			}
			get{
				return this.mstrFilename;
			}
		}

		#endregion
		
		#region constructor destructor
		
		public clsDAL(){
			mstrConnectionString = "User Id=Admin;provider=Microsoft.Jet.OLEDB.4.0;data source=";
		}

		public void Dispose(){
			if (mcmdOle != null) mcmdOle.Dispose();
			if (mdicMetadata != null){
				foreach (DataTable dt in mdicMetadata.Values){
					if (dt != null)
						dt.Dispose();
				}
				mdicMetadata.Clear();
			}
		}
		
		#endregion
		
		public void loadMetadata(string strFilename){
			OleDbConnection conOle = null;
			try{
				this.Filename= strFilename;
				conOle = this.Open();
				if (mdicMetadata != null){
					mdicMetadata.Clear();
					mdicMetadata=null;
				}
				mdicMetadata = loadMetadata(conOle);
				marKeywords = loadKeyWords(conOle);
			} catch (Exception ex){
				clsLogging.GEH(ex);
			}
			finally{
				this.Close(conOle);
			}
		}
		private static Dictionary<Guid, string> gDicSchemaNames =null;

		public static string getSchemaName(Guid schemaGuid){
			try{
				if (gDicSchemaNames == null){
					gDicSchemaNames = new Dictionary<Guid, string>();
					gDicSchemaNames.Add (OleDbSchemaGuid.Assertions,	            "Assertions");
					gDicSchemaNames.Add (OleDbSchemaGuid.Catalogs,	                "Catalogs");
					gDicSchemaNames.Add (OleDbSchemaGuid.Character_Sets,	        "Character Sets");
					gDicSchemaNames.Add (OleDbSchemaGuid.Check_Constraints,	        "Check Constraints");
					gDicSchemaNames.Add (OleDbSchemaGuid.Check_Constraints_By_Table,"Chk.Constr.by Table");
					gDicSchemaNames.Add (OleDbSchemaGuid.Collations,                "Collations");
					gDicSchemaNames.Add (OleDbSchemaGuid.Column_Domain_Usage,	    "Col. Dom. Usage");
					gDicSchemaNames.Add (OleDbSchemaGuid.Column_Privileges,	        "Column Privileges");
					gDicSchemaNames.Add (OleDbSchemaGuid.Columns,                   "Columns");
					gDicSchemaNames.Add (OleDbSchemaGuid.Constraint_Column_Usage,   "Constraint Column Usage");
					gDicSchemaNames.Add (OleDbSchemaGuid.Constraint_Table_Usage,    "Constraint Table Usage");
					gDicSchemaNames.Add (OleDbSchemaGuid.DbInfoKeywords,            "DbInfoKeywords");
					gDicSchemaNames.Add (OleDbSchemaGuid.DbInfoLiterals,            "dbInfoLiterals");
					gDicSchemaNames.Add (OleDbSchemaGuid.Foreign_Keys,              "Foreigen Keys");
					gDicSchemaNames.Add (OleDbSchemaGuid.Indexes,                   "Indexes");
					gDicSchemaNames.Add (OleDbSchemaGuid.Key_Column_Usage,          "Key Column Usage");
					gDicSchemaNames.Add (OleDbSchemaGuid.Primary_Keys,              "Primary Keys");
					gDicSchemaNames.Add (OleDbSchemaGuid.Procedure_Columns,         "Procedure Columns");
					gDicSchemaNames.Add (OleDbSchemaGuid.Procedure_Parameters,      "Procedure Parameters");
					gDicSchemaNames.Add (OleDbSchemaGuid.Procedures,                "Procedures");
					gDicSchemaNames.Add (OleDbSchemaGuid.Provider_Types,            "Provided Types");
					gDicSchemaNames.Add (OleDbSchemaGuid.Referential_Constraints,   "Referential Constraints");
					gDicSchemaNames.Add (OleDbSchemaGuid.SchemaGuids,               "SchemaGuids");
					gDicSchemaNames.Add (OleDbSchemaGuid.Schemata,                  "Schemata");
					gDicSchemaNames.Add (OleDbSchemaGuid.Sql_Languages,             "SQL-Dialekte");
					gDicSchemaNames.Add (OleDbSchemaGuid.Statistics,                "Statistics");
					gDicSchemaNames.Add (OleDbSchemaGuid.Table_Constraints,         "Table Constraints");
					gDicSchemaNames.Add (OleDbSchemaGuid.Table_Privileges,          "Table Privileges");
					gDicSchemaNames.Add (OleDbSchemaGuid.Table_Statistics,          "Table Statistics");
					gDicSchemaNames.Add (OleDbSchemaGuid.Tables,                    "Tables");
					gDicSchemaNames.Add (OleDbSchemaGuid.Tables_Info,               "Table Info");
					gDicSchemaNames.Add (OleDbSchemaGuid.Translations,              "Translations");
					gDicSchemaNames.Add (OleDbSchemaGuid.Trustee,                   "Trustee");
					gDicSchemaNames.Add (OleDbSchemaGuid.Usage_Privileges,          "User Privileges");
					gDicSchemaNames.Add (OleDbSchemaGuid.View_Column_Usage,         "View Column Usage");
					gDicSchemaNames.Add (OleDbSchemaGuid.View_Table_Usage,          "View Table Usage");
					gDicSchemaNames.Add (OleDbSchemaGuid.Views,                     "Views");
					gDicSchemaNames.Add (Guid.Empty,                                "Meta Data Collentions");
					gDicSchemaNames.Add (new Guid("947bb102-5d43-11d1-bdbf-00c04fb92675"),                     "Computer & Login / JET_SCHEMA_USERROSTER ");
					gDicSchemaNames.Add (new Guid("8703b612-5d43-11d1-bdbf-00c04fb92675"),                     "Diskaccess Statistics / JET_SCHEMA_ISAMSTATS ");
					gDicSchemaNames.Add (new Guid("e2082df0-54ac-11d1-bdbb-00c04fb92675"),"JET_SCHEMA_REPLPARTIALFILERLIST");
					gDicSchemaNames.Add (new Guid("e2082df2-54ac-11d1-bdbb-00c04fb92675"),"JET_SCHEMA_REPLCONFLICTTAGBLES*");
					}
				
				string name="-unbekanntes Schema:" + schemaGuid.ToString() + "-";
				if (gDicSchemaNames.ContainsKey(schemaGuid ))
					name= gDicSchemaNames[schemaGuid];
				return name;
				
			}catch(Exception ex){
				clsLogging.GEH(ex);
				return ex.Message;
			}
		}

		public void loadTableData(DBTreeNode tn,OleDbConnection conOle)
		{
			TableNode tnName;
			string strCommand;
			
			if (typeof(TableNode).IsInstanceOfType (tn)){
				tnName = (TableNode)tn;
				strCommand ="Select * From ["+tnName.Key+"]";
				try{
					tnName.Table = this.query(conOle, strCommand );
					tnName.evaluatePK();
				}catch(OleDbException ex){
					clsLogging.LOG (new clsMessage(enumMsgType.Fail , ex.Message, "SQL ", strCommand+"\n"+conOle.ConnectionString ));
					tnName.Table=null;
				}
			}
			
		}
		
		#region sql and dml
		
		//conOle: eine offene Verbindung, die offen bleiben soll
		public DataTable query (OleDbConnection conOle, string strSql){
			DataTable  result = new DataTable();
			OleDbDataAdapter daOle = null;
			try{
				mcmdOle.CommandText = strSql;
				daOle = new OleDbDataAdapter();
				daOle.SelectCommand = mcmdOle;
				daOle.Fill(result);
			}finally{
				if (null != daOle) daOle.Dispose();
				daOle = null;
			}
			return result;
			
		}
		
		public DataTable query( string strSql){
			OleDbConnection conOle = null;
			DataTable  result = null;
			try{
				conOle = this.Open();
				result = this.query (conOle,strSql);
			}finally{
				this.Close(conOle);
			}
			return result;
		}

		//  Sendet den SQL-Befehl zum �ndern der Datenbank an die selbe.
		public int update( string strSql){
			OleDbConnection conOle = null;
			try{
				conOle = this.Open();
				mcmdOle = conOle.CreateCommand();
				mcmdOle.CommandText = strSql;
				return mcmdOle.ExecuteNonQuery();
			}catch(Exception ex){
				clsLogging.GEH(ex);
				return 0;
			}finally{
				this.Close(conOle);
			}
		}
		
		#endregion
		
		#region " private functions "

		#region open and close
		public OleDbConnection Open(){
			OleDbConnection conOle = null;
			try{
				conOle = new OleDbConnection(mstrConnectionString + mstrFilename);
				conOle.Open();
				mcmdOle = conOle.CreateCommand();
				return conOle;
			}catch(Exception ex){
				clsLogging.GEH(ex);
				return null;
			}
		}
		
		public void Close (OleDbConnection conOle){
			if ( null != mcmdOle)
				mcmdOle.Dispose();
			if (null != conOle){
				if (conOle.State != ConnectionState.Closed)
					conOle.Close();
				conOle.Dispose();
			}
		}
		#endregion
		
		//  L�d die von der Datenbank angegebene Schl�sselw�rter.
		private string[] loadKeyWords(OleDbConnection conOle){
			DataTable schema;
			schema = conOle.GetOleDbSchemaTable(OleDbSchemaGuid.DbInfoKeywords , new object[0]);
			List<string> strKeyWords;
			strKeyWords = new List<string>(schema.Rows.Count);
			foreach ( DataRow dr in schema.Rows ){
				strKeyWords.Add(dr["Keyword"].ToString());
			}
			return strKeyWords.ToArray();
		}
		
		// wird nur einmal aus der Datenbank geladen.
		private Dictionary<Guid,DataTable> loadMetadata(OleDbConnection conOle){
			Dictionary<Guid,DataTable> dicResult = null;
			try{
				
				DataTable schemaTable;
				List<Guid> schemaGuids = new List<Guid>();
				
				// zuerst nach der Liste der zur Verf�gung stehenden Guids fragen
				schemaTable = conOle.GetOleDbSchemaTable(OleDbSchemaGuid.SchemaGuids, null);

				// die liste aufbauen
				foreach( DataRow dr in schemaTable.Rows){
					if(! dr.IsNull("Schema")){
						if (!OleDbSchemaGuid.SchemaGuids.Equals(dr["Schema"])){
							schemaGuids.Add((Guid)( dr["Schema"]));
						}
					}
				}
				schemaGuids.Remove(new Guid("c8b522ef-5cf3-11ce-ade5-00aa0044773d"));
				schemaGuids.Remove(new Guid("e2082df0-54ac-11d1-bdbb-00c04fb92675"));
				schemaGuids.Remove(new Guid("e2082df2-54ac-11d1-bdbb-00c04fb92675"));
				
				dicResult = new Dictionary<Guid,DataTable>();
				DataTable schema;
				foreach (Guid guidValue in schemaGuids){
					try{
						schema = conOle.GetOleDbSchemaTable(guidValue, null);
						
						if (schema.PrimaryKey.Length == 0){
							List<DataColumn> newpk = new List<DataColumn>();
							foreach(DataColumn col in schema.Columns){
								if (col.ColumnName.ToUpper().Contains("NAME")){
									newpk.Add(col);
									try{
										col.AllowDBNull=false;
										try{
											schema.PrimaryKey = newpk.ToArray();
											break;
										}catch(ArgumentException ){
										}catch(Exception e ){
											string strCols="";
											foreach(DataColumn column in newpk){
												strCols +=", "+column.ColumnName;
											}
											if (strCols.Length >0){
												strCols = "["+strCols.Substring(2)+"]";
											}else
												strCols=" [-]";
											clsLogging.LOG(enumMsgType.Error,e.Message,strCols+"\n"+e.StackTrace);
										}
									}catch(Exception){}
									
								}
							}
						}

						
						dicResult.Add(guidValue, schema);
					}catch(System.Data.OleDb.OleDbException ex){
						clsLogging.LOG(enumMsgType.Fail, getSchemaName(guidValue),
						               ex.Message +"\n"+ex.StackTrace  );
					}
				}
				schema =conOle.GetSchema();
				dicResult.Add(Guid.Empty ,schema);
				
				
				
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
			return dicResult;
		}
		
		#endregion
	}
}