/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas M�ller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;

namespace JETDBComp
{
	public enum enumMsgType : int{
		Info,
		Error,
		Warning,
		Success,
		Fail
	}

	public class clsMessage{
		
		#region private member

		private enumMsgType mmtMessageType;

		private string mstrSection;
		
		private string mstrTitle;

		private string mstrDescription;
		
		private DateTime mdtTimeStamp;

		#endregion
		
		#region public properies
		
		public enumMsgType Type {
			get { return mmtMessageType; }
		}

		public string Section {
			get { return mstrSection; }
		}
		
		public string Title {
			get { return mstrTitle; }
		}
		
		public string Description {
			get { return mstrDescription; }
		}
		
		public DateTime TimeStamp {
			get { return mdtTimeStamp; }
		}
		
		#endregion

		#region constructor
		/// <summary>
		/// Logging-Eintrag
		/// </summary>
		/// <param name="mtType">Kind of entry: info, error, warning, success oder fail.</param>
		/// <param name="strSection">Category</param>
		/// <param name="strTitle">First Line</param>
		/// <param name="strDescription">More Detailed Description.</param>
		public clsMessage(enumMsgType mtType, string strSection, string strTitle, string strDescription){
			mmtMessageType=mtType;
			mstrSection=strSection;
			mstrTitle=strTitle;
			mstrDescription = strDescription;
			mdtTimeStamp= System.DateTime.UtcNow;
		}
		#endregion
	}
	
	
	/// <summary>
	/// Description of MyClass.
	/// </summary>
	public class clsLogging
	{
		
		#region " Event Message"
		public delegate void dlgMessage(clsMessage msg);
		public static event dlgMessage Message;
		#endregion
		
		#region  private Singleton pattern
		private static readonly clsLogging mobjLogging = new clsLogging();

		private clsLogging(){
		}
		
		private static clsLogging Instance{
			get {
				return mobjLogging;
			}
		}
		
		#endregion
		
		#region public static functions
		
		// composes a Message and invokes a "Message"-Event
		public static void LOG(enumMsgType mtType,string strTitle, string strMessage){
			clsMessage msg = new clsMessage(mtType, "Log", strTitle, strMessage);
			if (Message != null)
				Message(msg);
		}

		// invokes the "Message"-Event
		public static void LOG(clsMessage msg){
			if (Message != null)
				Message(msg);
		}
		
		// composes an Error-Message and invokes ehe Message Event.
		public static void GEH(Exception ex){
			clsMessage msg = new clsMessage(enumMsgType.Error, "GEH",ex.Message,ex.GetType().ToString()+"\n"+ex.StackTrace);
			try{
				if (Message != null)
					Message(msg);
			} catch (Exception )
			{
				throw ex ;
			}
		}

		#endregion
		
	}
}
