﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace JETDBComp
{
	partial class MainForm
	{
		// Designer variable used to keep track of non-visual components.
		private System.ComponentModel.IContainer components = null;
		
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		// This method is required for Windows Forms designer support.
		// Do not change the method contents inside the source code editor. The Forms designer might
		// not be able to load this method if it was changed manually.
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.uctlTarget = new JETDBComp.GUI.uctlDBNamePanel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tcTabs = new System.Windows.Forms.TabControl();
			this.tpStructure = new System.Windows.Forms.TabPage();
			this.uctlStructure1 = new JETDBComp.GUI.uctlStructure();
			this.tpSQL = new System.Windows.Forms.TabPage();
			this.muctlSQL = new JETDBComp.GUI.uctlSQLQueries();
			this.tpScript = new System.Windows.Forms.TabPage();
			this.tpLog = new System.Windows.Forms.TabPage();
			this.uctlLogBook1 = new JETDBComp.GUI.uctlLogBook();
			this.tpInfo = new System.Windows.Forms.TabPage();
			this.uctlInfo1 = new JETDBComp.GUI.uctlInfo();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuMaster = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuTarget = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuOption = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuCompare = new System.Windows.Forms.ToolStripMenuItem();
			this.uctlMaster = new JETDBComp.GUI.uctlDBNamePanel();
			this.panel1.SuspendLayout();
			this.tcTabs.SuspendLayout();
			this.tpStructure.SuspendLayout();
			this.tpSQL.SuspendLayout();
			this.tpLog.SuspendLayout();
			this.tpInfo.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// uctlTarget
			// 
			this.uctlTarget.AllowDrop = true;
			this.uctlTarget.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.uctlTarget.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.uctlTarget.Filename = "";
			this.uctlTarget.Label = "Target";
			this.uctlTarget.Location = new System.Drawing.Point(335, 0);
			this.uctlTarget.Name = "uctlTarget";
			this.uctlTarget.Size = new System.Drawing.Size(320, 27);
			this.uctlTarget.TabIndex = 1;
			this.uctlTarget.FileChanged += new System.EventHandler(this.UctlMasterFileChanged);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.uctlMaster);
			this.panel1.Controls.Add(this.uctlTarget);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 24);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(668, 29);
			this.panel1.TabIndex = 2;
			// 
			// tcTabs
			// 
			this.tcTabs.Controls.Add(this.tpStructure);
			this.tcTabs.Controls.Add(this.tpSQL);
			this.tcTabs.Controls.Add(this.tpScript);
			this.tcTabs.Controls.Add(this.tpLog);
			this.tcTabs.Controls.Add(this.tpInfo);
			this.tcTabs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tcTabs.Location = new System.Drawing.Point(0, 53);
			this.tcTabs.Name = "tcTabs";
			this.tcTabs.SelectedIndex = 0;
			this.tcTabs.Size = new System.Drawing.Size(668, 451);
			this.tcTabs.TabIndex = 3;
			// 
			// tpStructure
			// 
			this.tpStructure.Controls.Add(this.uctlStructure1);
			this.tpStructure.Location = new System.Drawing.Point(4, 22);
			this.tpStructure.Name = "tpStructure";
			this.tpStructure.Size = new System.Drawing.Size(660, 425);
			this.tpStructure.TabIndex = 2;
			this.tpStructure.Text = "Struktur";
			this.tpStructure.UseVisualStyleBackColor = true;
			// 
			// uctlStructure1
			// 
			this.uctlStructure1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.uctlStructure1.Location = new System.Drawing.Point(0, 0);
			this.uctlStructure1.Name = "uctlStructure1";
			this.uctlStructure1.Size = new System.Drawing.Size(660, 425);
			this.uctlStructure1.TabIndex = 0;
			// 
			// tpSQL
			// 
			this.tpSQL.Controls.Add(this.muctlSQL);
			this.tpSQL.Location = new System.Drawing.Point(4, 22);
			this.tpSQL.Name = "tpSQL";
			this.tpSQL.Size = new System.Drawing.Size(660, 425);
			this.tpSQL.TabIndex = 3;
			this.tpSQL.Text = "SQL";
			this.tpSQL.UseVisualStyleBackColor = true;
			// 
			// muctlSQL
			// 
			this.muctlSQL.Dock = System.Windows.Forms.DockStyle.Fill;
			this.muctlSQL.Location = new System.Drawing.Point(0, 0);
			this.muctlSQL.Name = "muctlSQL";
			this.muctlSQL.Size = new System.Drawing.Size(660, 425);
			this.muctlSQL.TabIndex = 0;
			// 
			// tpScript
			// 
			this.tpScript.Location = new System.Drawing.Point(4, 22);
			this.tpScript.Name = "tpScript";
			this.tpScript.Size = new System.Drawing.Size(660, 425);
			this.tpScript.TabIndex = 4;
			this.tpScript.Text = "Script";
			this.tpScript.UseVisualStyleBackColor = true;
			// 
			// tpLog
			// 
			this.tpLog.Controls.Add(this.uctlLogBook1);
			this.tpLog.Location = new System.Drawing.Point(4, 22);
			this.tpLog.Name = "tpLog";
			this.tpLog.Size = new System.Drawing.Size(660, 425);
			this.tpLog.TabIndex = 5;
			this.tpLog.Text = "Log";
			this.tpLog.UseVisualStyleBackColor = true;
			// 
			// uctlLogBook1
			// 
			this.uctlLogBook1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.uctlLogBook1.Location = new System.Drawing.Point(0, 0);
			this.uctlLogBook1.Name = "uctlLogBook1";
			this.uctlLogBook1.Size = new System.Drawing.Size(635, 405);
			this.uctlLogBook1.TabIndex = 0;
			this.uctlLogBook1.Error += new JETDBComp.GUI.uctlLogBook.dlgNewEvent(this.UctlLogBook1Error);
			// 
			// tpInfo
			// 
			this.tpInfo.Controls.Add(this.uctlInfo1);
			this.tpInfo.Location = new System.Drawing.Point(4, 22);
			this.tpInfo.Name = "tpInfo";
			this.tpInfo.Padding = new System.Windows.Forms.Padding(3);
			this.tpInfo.Size = new System.Drawing.Size(660, 425);
			this.tpInfo.TabIndex = 0;
			this.tpInfo.Text = "Info";
			this.tpInfo.UseVisualStyleBackColor = true;
			// 
			// uctlInfo1
			// 
			this.uctlInfo1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.uctlInfo1.Location = new System.Drawing.Point(3, 3);
			this.uctlInfo1.Name = "uctlInfo1";
			this.uctlInfo1.Size = new System.Drawing.Size(654, 419);
			this.uctlInfo1.TabIndex = 0;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.mnuFile,
									this.mnuOption,
									this.mnuCompare});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(668, 24);
			this.menuStrip1.TabIndex = 5;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// mnuFile
			// 
			this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.mnuMaster,
									this.mnuTarget,
									this.mnuAbout});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Size = new System.Drawing.Size(46, 20);
			this.mnuFile.Text = "Datei";
			// 
			// mnuMaster
			// 
			this.mnuMaster.Name = "mnuMaster";
			this.mnuMaster.Size = new System.Drawing.Size(211, 22);
			this.mnuMaster.Text = "Master Datenbank wählen";
			this.mnuMaster.Click += new System.EventHandler(this.MnuMasterClick);
			// 
			// mnuTarget
			// 
			this.mnuTarget.Name = "mnuTarget";
			this.mnuTarget.Size = new System.Drawing.Size(211, 22);
			this.mnuTarget.Text = "Target Datenbank wählen";
			this.mnuTarget.Click += new System.EventHandler(this.MnuTargetClick);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Name = "mnuAbout";
			this.mnuAbout.Size = new System.Drawing.Size(211, 22);
			this.mnuAbout.Text = "Über...";
			this.mnuAbout.Click += new System.EventHandler(this.MnuAboutClick);
			// 
			// mnuOption
			// 
			this.mnuOption.Name = "mnuOption";
			this.mnuOption.Size = new System.Drawing.Size(69, 20);
			this.mnuOption.Text = "Optionen";
			this.mnuOption.Click += new System.EventHandler(this.MnuOptionClick);
			// 
			// mnuCompare
			// 
			this.mnuCompare.Name = "mnuCompare";
			this.mnuCompare.Size = new System.Drawing.Size(81, 20);
			this.mnuCompare.Text = "Vergleichen";
			this.mnuCompare.Click += new System.EventHandler(this.mnuCompareClick);
			// 
			// uctlMaster
			// 
			this.uctlMaster.AllowDrop = true;
			this.uctlMaster.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.uctlMaster.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.uctlMaster.Filename = "";
			this.uctlMaster.Label = "Master";
			this.uctlMaster.Location = new System.Drawing.Point(13, 0);
			this.uctlMaster.Name = "uctlMaster";
			this.uctlMaster.Size = new System.Drawing.Size(320, 27);
			this.uctlMaster.TabIndex = 0;
			this.uctlMaster.FileChanged += new System.EventHandler(this.UctlMasterFileChanged);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(668, 504);
			this.Controls.Add(this.tcTabs);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.Text = "JETDBComp";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.SizeChanged += new System.EventHandler(this.MainFormSizeChanged);
			this.panel1.ResumeLayout(false);
			this.tcTabs.ResumeLayout(false);
			this.tpStructure.ResumeLayout(false);
			this.tpSQL.ResumeLayout(false);
			this.tpLog.ResumeLayout(false);
			this.tpInfo.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.ToolStripMenuItem mnuCompare;
		private System.Windows.Forms.ToolStripMenuItem mnuFile;
		private System.Windows.Forms.ToolStripMenuItem mnuMaster;
		private System.Windows.Forms.ToolStripMenuItem mnuTarget;
		private System.Windows.Forms.ToolStripMenuItem mnuAbout;
		private System.Windows.Forms.ToolStripMenuItem mnuOption;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.TabControl tcTabs;
		private JETDBComp.GUI.uctlSQLQueries muctlSQL;
		private JETDBComp.GUI.uctlLogBook uctlLogBook1;
		private System.Windows.Forms.TabPage tpLog;
		private System.Windows.Forms.TabPage tpScript;
		private System.Windows.Forms.TabPage tpSQL;
		private JETDBComp.GUI.uctlStructure uctlStructure1;
		private System.Windows.Forms.TabPage tpStructure;
		private JETDBComp.GUI.uctlInfo uctlInfo1;
		private System.Windows.Forms.TabPage tpInfo;
		private System.Windows.Forms.Panel panel1;
		private JETDBComp.GUI.uctlDBNamePanel uctlTarget;
		private JETDBComp.GUI.uctlDBNamePanel uctlMaster;
		
	}
}
