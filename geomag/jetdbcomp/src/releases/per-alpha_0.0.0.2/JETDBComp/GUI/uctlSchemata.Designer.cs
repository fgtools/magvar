﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace JETDBComp.GUI
{
	partial class uctlSchemata
	{

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbDatabase = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.cbSchema = new System.Windows.Forms.ComboBox();
			this.dgvTable = new System.Windows.Forms.DataGridView();
			this.lblTableName = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvTable)).BeginInit();
			this.SuspendLayout();
			// 
			// cbDatabase
			// 
			this.cbDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbDatabase.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.cbDatabase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDatabase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cbDatabase.FormattingEnabled = true;
			this.cbDatabase.Items.AddRange(new object[] {
									"Master",
									"Target"});
			this.cbDatabase.Location = new System.Drawing.Point(72, -1);
			this.cbDatabase.Name = "cbDatabase";
			this.cbDatabase.Size = new System.Drawing.Size(214, 21);
			this.cbDatabase.TabIndex = 0;
			this.cbDatabase.SelectedIndexChanged += new System.EventHandler(this.CbDatabaseSelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(1, 2);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(63, 15);
			this.label1.TabIndex = 1;
			this.label1.Text = "Datenbank:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.Window;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.cbDatabase);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(2, 2);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(289, 23);
			this.panel1.TabIndex = 2;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.SystemColors.Window;
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.cbSchema);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(2, 25);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(289, 23);
			this.panel2.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(1, 2);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(63, 15);
			this.label2.TabIndex = 1;
			this.label2.Text = "Schema:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// cbSchema
			// 
			this.cbSchema.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
									| System.Windows.Forms.AnchorStyles.Left) 
									| System.Windows.Forms.AnchorStyles.Right)));
			this.cbSchema.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.cbSchema.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbSchema.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cbSchema.Items.AddRange(new object[] {
									"Master",
									"Target"});
			this.cbSchema.Location = new System.Drawing.Point(72, -1);
			this.cbSchema.Name = "cbSchema";
			this.cbSchema.Size = new System.Drawing.Size(214, 21);
			this.cbSchema.TabIndex = 0;
			this.cbSchema.SelectedIndexChanged += new System.EventHandler(this.CbSchemaSelectedIndexChanged);
			// 
			// dgvTable
			// 
			this.dgvTable.AllowUserToAddRows = false;
			this.dgvTable.AllowUserToDeleteRows = false;
			this.dgvTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
			this.dgvTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
			this.dgvTable.BackgroundColor = System.Drawing.SystemColors.ControlDark;
			this.dgvTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dgvTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvTable.Location = new System.Drawing.Point(2, 71);
			this.dgvTable.Name = "dgvTable";
			this.dgvTable.ReadOnly = true;
			this.dgvTable.Size = new System.Drawing.Size(289, 334);
			this.dgvTable.TabIndex = 4;
			// 
			// lblTableName
			// 
			this.lblTableName.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblTableName.Location = new System.Drawing.Point(2, 48);
			this.lblTableName.Name = "lblTableName";
			this.lblTableName.Size = new System.Drawing.Size(289, 23);
			this.lblTableName.TabIndex = 5;
			this.lblTableName.Text = "label3";
			this.lblTableName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// uctlSchemata
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dgvTable);
			this.Controls.Add(this.lblTableName);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "uctlSchemata";
			this.Padding = new System.Windows.Forms.Padding(2);
			this.Size = new System.Drawing.Size(293, 407);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvTable)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label lblTableName;
		private System.Windows.Forms.DataGridView dgvTable;
		private System.Windows.Forms.ComboBox cbSchema;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbDatabase;
	}
}
