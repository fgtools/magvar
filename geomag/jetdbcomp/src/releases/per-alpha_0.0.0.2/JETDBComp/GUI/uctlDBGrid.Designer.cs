﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace JETDBComp.GUI
{
	partial class uctlDBGrid
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.mDGV = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize)(this.mDGV)).BeginInit();
			this.SuspendLayout();
			// 
			// mDGV
			// 
			this.mDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
			this.mDGV.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.mDGV.DefaultCellStyle = dataGridViewCellStyle1;
			this.mDGV.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mDGV.GridColor = System.Drawing.SystemColors.Control;
			this.mDGV.Location = new System.Drawing.Point(1, 1);
			this.mDGV.Name = "mDGV";
			this.mDGV.ReadOnly = true;
			this.mDGV.RowHeadersVisible = false;
			this.mDGV.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.mDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.mDGV.ShowCellErrors = false;
			this.mDGV.ShowEditingIcon = false;
			this.mDGV.ShowRowErrors = false;
			this.mDGV.Size = new System.Drawing.Size(517, 396);
			this.mDGV.StandardTab = true;
			this.mDGV.TabIndex = 9;
			this.mDGV.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.MDGVCellPainting);
			this.mDGV.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.MDGVDataError);
			this.mDGV.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.MDGVDataBindingComplete);
			this.mDGV.SelectionChanged += new System.EventHandler(this.MDGVSelectionChanged);
			// 
			// uctlDBGrid
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.mDGV);
			this.Name = "uctlDBGrid";
			this.Padding = new System.Windows.Forms.Padding(1);
			this.Size = new System.Drawing.Size(519, 398);
			this.Load += new System.EventHandler(this.UctlDBGridLoad);
			((System.ComponentModel.ISupportInitialize)(this.mDGV)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.DataGridView mDGV;
	}
}
