﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#region using
using System;
using System.Collections.Generic; 
#endregion

namespace JETDBComp.Model
{
	/// <summary>
	/// Description of clsDifference.
	///
	/// </summary>
	public class clsDifference: Dictionary<int, char>{
		public clsDifference ():base(){}
		
		/// <summary>
		/// dictionary of differences.
		/// returns "<>" if its elements describe any differences
		/// returns "=" if all elements describe unchanged values
		/// </summary>
		/// <returns></returns>
		public override string ToString(){
			if (this.ContainsValue('d'))
				return "<>";
			if (this.ContainsValue('-'))
				return "<>";
			if (this.ContainsValue('+'))
				return "<>";
			return "=";
		}
	}
}
