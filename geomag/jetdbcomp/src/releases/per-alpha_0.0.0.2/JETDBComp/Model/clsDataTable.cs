﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.Data;
using System.Collections.Generic;
using System.Reflection;
#endregion


namespace JETDBComp.Model
{
	public class clsDataTable
	{

		#region public functions
		public static object[] primaryValue(DataColumn[] PrimaryKey, DataRow dr){
			object[] value = new object[PrimaryKey.Length ];
			for (int position = 0 ; position < PrimaryKey.Length;position++){
				if (dr.Table.Columns.Contains(PrimaryKey[position].ColumnName))
					value[position]=dr[PrimaryKey[position].ColumnName];
				else
					value[position]=null;
			}
			return value;
		}
		
		// returns a number of differences
		public static int compareDataTable( DataTable dtRed, DataTable dtGreen,ref DataTable dtNew,bool blnOnlyDifferences){
			int delta = 0;
			try{
				DataTable baseTable = null;
				if (dtGreen != null){
					baseTable=dtGreen;
				}else if (dtRed != null){
					baseTable=dtRed;
					delta=1;
				}else{
					dtNew = null;
					return 0;
				}
				
				
				delta += compareDataColumns(dtRed,baseTable,ref dtNew);
				
				if (dtGreen !=null){
					foreach (DataRow dr in dtGreen.Rows){
						try{
							dtNew.Rows.Add(expandArray("+",dr.ItemArray,dtGreen.Columns,dtNew.Columns ));
						}catch(System.Data.ConstraintException sdce){
							dtNew.PrimaryKey=null;
							clsMessage objMsg = new clsMessage(enumMsgType.Fail, "SQL",sdce.Message ,sdce.StackTrace );
							clsLogging.LOG(objMsg);
						}
						delta ++;
					}
				}
				
				if (dtRed != null){

					DataRow found;
					foreach (DataRow dr in dtRed.Rows){
						found = null;
						
						if((dtGreen !=null)&&(dtGreen.PrimaryKey.Length!=0)){
							found = dtNew.Rows.Find(primaryValue(dtNew.PrimaryKey,dr));
							if (found != null){
								object[] x = found.ItemArray;
								x[0]="=";
								Dictionary<int,char> difference = compare(dr,found);
								x[0] =difference;
								found.ItemArray = x;
								delta --;
							}
							found = dtGreen.Rows.Find(primaryValue (dtGreen.PrimaryKey,dr));
						}
						if ((dtGreen == null)||(dtGreen.PrimaryKey.Length==0)||(found == null)){
							
							dtNew.Rows.Add(expandArray("-",dr.ItemArray,dtRed.Columns,dtNew.Columns));
							delta++;
						}else{
							bool change=false;
							foreach (DataColumn dc in dr.Table.Columns ){
								object drCell = dr[dc.ColumnName];
								object fCell =null;
								if (found.Table.Columns.Contains(dc.ColumnName))
									fCell = found[dc.ColumnName];
								
								if ((drCell!=null)&&(fCell!=null)){
									if (!drCell.ToString().Equals( fCell.ToString())){
										change=true;
										delta++;
									}
								}else{
									if (!((drCell==null)&&(fCell==null))){
										change=true;
										delta++;
									}
								}
							}
							if ((!change)&& (blnOnlyDifferences )){
								found = dtNew.Rows.Find(primaryValue (dtNew.PrimaryKey ,found));
								if (found != null)
									dtNew.Rows.Remove(found);
							}
						}
					}
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
			return delta;
		}


		#endregion
		
		#region compareDataTable
	
		private static object[] expandArray(string firstValue,
		                                    object[] objOldData,
		                                    DataColumnCollection oldColumns,
		                                    DataColumnCollection newColumns){
			object[] result = new object[newColumns.Count];
			result[0]=firstValue;
			foreach (DataColumn dc in oldColumns){
				int position = newColumns[dc.ColumnName].Ordinal;
				result[position] = objOldData[dc.Ordinal];
			}
			return result;
		}

		private static clsDifference compare(DataRow red, DataRow target){
			clsDifference result = new clsDifference();
			foreach( DataColumn dcRed in red.Table.Columns ){
				if (dcRed != null){
					int position = ((DataColumn)target.Table.Columns[dcRed.ColumnName]).Ordinal;
					int intRed = dcRed.Ordinal;
					if  ( intRed < 0 )
						result.Add(position,'-');
					else if (position >=0){
						if (red.ItemArray[intRed] == null){
							if (target.ItemArray[position] != null){
								result.Add(position, 'd');
							}
						}else if (!red.ItemArray[intRed].Equals( target.ItemArray[position] )){
							result.Add(position, 'd');
						}else
							result.Add(position, '=');
					}
				}
			}
			return result;
		}
	
		private static int compareDataColumns (DataTable dtRed, DataTable dtGreen,ref DataTable dtNew){
			int delta = 0;
			try{
				dtNew.Columns.Add("",typeof(object));
				// gemeinsame und neue Spalten
				//dtNew.Columns[0].Caption="";
				dtNew.Columns[0].ColumnName='\u0394'.ToString() ;// UniCode-Zeichen für Delta
				foreach (DataColumn dc in dtGreen.Columns){
					dtNew.Columns.Add(dc.ColumnName,dc.DataType );
					delta ++;
				}
				// primärschlüssel
				List<string> pkNames = new List<string>();
				List<DataColumn> pk = new List<DataColumn>();
				for (int i =0 ; i < dtGreen.PrimaryKey.Length ;i++){
					pkNames.Add(dtGreen.PrimaryKey[i].ColumnName );
					pk.Add(dtNew.Columns[dtGreen.PrimaryKey[i].ColumnName ]);
					delta++;
				}
				
				if (dtRed != null){
					// gelöschte Spalten hinten anhängen.
					foreach (DataColumn dc in dtRed.Columns){
						if (dtNew.Columns[dc.ColumnName] == null){
							dtNew.Columns.Add(dc.ColumnName,dc.DataType );
							delta ++;
						}else
							delta --;
					}
					// primärschlüssel vergleichen
					for (int i = 0 ; i < dtRed.PrimaryKey.Length; i++){
						string pkName = dtRed.PrimaryKey[i].ColumnName;
						
						if (pkNames.Contains(pkName)){
							delta --;
						}else{
							pk.Add(dtNew.Columns[pkName]);
							delta ++;
						}
					}
				}
				dtNew.PrimaryKey=pk.ToArray() ;
				foreach (DataColumn dc in dtNew.PrimaryKey ){
					dc.AllowDBNull = true;
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
			return delta;
		}
		
		#endregion
		
		
		#region GetValidData

				// versucht die Daten als Tabelle darzustellen wenn es nicht bereits eine ist.
		public static DataTable GetValidData( object data){
			if ( data == null ){
				return null;
			}else if (data.GetType().ToString().Equals("System.Data.DataTable[]")){
				return ((DataTable[])data)[0];
			}else if ((typeof(DataTable).IsInstanceOfType(data))){
				return (DataTable)data;
			}else if (typeof(TableNode).IsInstanceOfType(data)){
				if (((TableNode)data).Table  != null ) {
					return  ((TableNode)data).Table;
				}
			}else{
				if (typeof(DBTreeNode).IsInstanceOfType(data))
					if (((DBTreeNode)data).Tag != null ) {
					object obj = ((DBTreeNode)data).Tag;
					if (obj.GetType().ToString().Equals("System.Object[]"))
						if(((object[]) obj)[0] != data)
							data =GetValidData(obj );
				}
				if(data.GetType().Equals(typeof(string))){
					return StringDefaultData(data);
				}else
					return ReflectiveDefaultData (data);
			}
			return null;
		}

		// Darstellung von Zeichenketten mit Word-Wrap nach 100 Zeichen
		private static DataTable StringDefaultData(object data){
			DataTable dt = new DataTable(data.ToString());
			dt.Columns.Clear();
			dt.Columns.Add("Nr",typeof(int));
			dt.Columns.Add("Script",typeof(string));
			dt.PrimaryKey= new DataColumn[]{ dt.Columns[0]};
			
			if (data != null){
				int row = 1;
				foreach (string wholeline in ((string)data).Split('\n')){
					string line= wholeline;
					while (line.Length > 100){
						string beginning = line.Substring(0,100);
						int lastSpace = beginning.LastIndexOf(" ");
						if  (lastSpace >10)
							beginning= line.Substring(0,lastSpace);
						dt.Rows.Add(new string[]{row.ToString(),beginning });
						row++;
						line = line.Substring(beginning.Length);
					}
					dt.Rows.Add(new string[]{row.ToString(),line });
					row++;
				}
			}
			return dt;
			
		}
		
		// Darstellung der Public Properties eines beliebigen Objektes per Reflection
		private static DataTable ReflectiveDefaultData(object data){
			DataTable dt = new DataTable(data.ToString());
			dt.Columns.Clear();
			dt.Columns.Add("Datentyp",typeof(string));
			dt.Columns.Add("Attribut", typeof(string));
			dt.Columns.Add("Value", typeof( object ));
			dt.PrimaryKey = new DataColumn[]{dt.Columns[0],dt.Columns[1]};
			dt.Rows.Add(new string[]{"","Type",data.GetType().ToString()});
			
			if (data != null){
				PropertyInfo[] properties =	data.GetType().GetProperties();
				foreach (PropertyInfo info in properties){
					string strType = info.PropertyType.Name;
					string strAttribut = info.Name;
					string strValue = "...not Available...";
					if ( info.CanRead ) {
						MethodInfo mi = info.GetGetMethod(false);
						if (mi != null){
							ParameterInfo[] pi = mi.GetParameters();
							if ((pi == null)||(pi.Length == 0)) {
								object obj =mi.Invoke( data,null);
								if (obj != null)
									strValue = obj.ToString();
								else strValue = "    (NULL)";
							}else{
								strValue="(";
								foreach (ParameterInfo pinfo in pi){
									strValue += pinfo.ParameterType
										+": "+pinfo.Name
										+", ";
								}
								strValue=strValue.Substring(0,strValue.Length -2)+")";
								strAttribut=strAttribut+strValue;
								strValue="--not available--";
							}
						}
					}
					dt.Rows.Add(new string[]{strType,strAttribut,strValue });
					
					
				}
			}
			return dt;
		}
		
		
		#endregion
	}
}
