﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Data;
namespace JETDBComp.Model
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class clsCompare	{
		#region "private member"
		private string FilenameMaster;
		private string FilenameTarget;
		private clsDAL mDAL;
		private static bool mblnCompareData;
		private static bool mblnOnlyDifferences;
		#endregion
		
		public clsCompare(string Master,string Target){
			mDAL= new clsDAL();
			FilenameMaster=Master;
			FilenameTarget=Target;
		}
		
		#region public Properties
		public static bool OptionCompareData{
			get{
				return mblnCompareData;
			}
			set{
				mblnCompareData = value;
			}
		}
		public static bool OptionOnlyDifferences{
			get{
				return mblnOnlyDifferences;
			}
			set{
				mblnOnlyDifferences=value;
			}
		}
		#endregion
		
		//hier
		public int compare(DBTreeNode first, DBTreeNode second){
			try{
				
				foreach(DBTreeNode child in first)
					clearState(child);
				foreach(DBTreeNode child in second)
					clearState(child);
				
				return this.compareRecursive(first,second);
				
			}catch (Exception ex){
				clsLogging.GEH(ex);
				return -1;
			}
		}
		
		#region "private Functions"
		private void clearState(DBTreeNode tnParent){
			tnParent.State=enumStates.None ;
			foreach(DBTreeNode child in tnParent)
				clearState(child);
		}

		private int doCompareData(TableNode tnLeft, TableNode tnRight){
			int differences = 0;
			try{
				DataTable dtMerged = new DataTable("merged");
				int result = TagArray(tnLeft,tnRight);
				DBTreeNode tn = tnLeft.Add("Daten");
				tn.Tag="; Anzahl Unterschiede: "+result.ToString();
				if (result == 0 )
					tn.State = enumStates.Ok;
				else{
					tn.State = enumStates.Changed;
					differences +=result;
					tnLeft.State = enumStates.Changed;
					tnRight.State = enumStates.Changed;
				}
			}catch (Exception ex){
				clsLogging.GEH(ex);
			}

			return differences;
		}
		
		//hier
		private int compareRecursive(DBTreeNode first,DBTreeNode second){
			try{
				int differences = 0;

				//gleiche markieren
				foreach (DBTreeNode tnLeft in first){
					string strName= tnLeft.Key;
					foreach (DBTreeNode tnRight in second){
						if ( tnRight.State != enumStates.None )continue;
						if (tnRight.Key == strName) {
							tnLeft.State = enumStates.Ok;
							tnRight.State = enumStates.Ok;
							int result = this.compareRecursive(tnLeft,tnRight );
							// unter schiedliche markieren
							if(result > 0){
								differences += result;
								tnLeft.State = enumStates.Changed;
								tnRight.State = enumStates.Changed;
							}
							if ((OptionCompareData) &&
							    (typeof(TableNode).IsInstanceOfType( tnLeft )) &&
							    (typeof(TableNode).IsInstanceOfType( tnRight ))){
								
								differences += doCompareData((TableNode)tnLeft,(TableNode)tnRight);
							}
							break;
						}
					}
					// gelöschte markieren.
					if (tnLeft.State == enumStates.None){
						differences +=1;
						tnLeft.State = enumStates.Removed;
					}
				}
				
				// neue markieren.
				foreach(DBTreeNode tnRight in second){
					if (tnRight.State == enumStates.None){
						differences +=1;
						tnRight.State = enumStates.New;
					}
				}
				mergeresults (first,second);
				return differences;
				
			}catch (Exception ex){
				clsLogging.GEH(ex);
				return 0;
			}
		}

		

		private void mergeresults(DBTreeNode first,DBTreeNode second){
			try{
				// versuchen, die neuen an der richtigen Stelle in die Datenstruktur der masterdatenbank einzufügen.
				int posRight = second.Count -1;
				int posLeft = first.Count -1;
				
				//PS: der Durchlauf ist vom größten Index zum Kleinsten, damit
				//beim entfernen oder einfügen die veränderte Numerierung nicht berücksichtigt
				//werden braucht.
				
				DBTreeNode tnLeft;
				DBTreeNode tnRight;
				while ((posRight>=0)||(posLeft>=0)){
					
					// Initialisierung dieses Schleifendurchlaufs:
					
					if (posLeft >= 0){
						tnLeft=first[posLeft];
					}else tnLeft= null;
					if (posRight >=0){
						tnRight = second[posRight];
					}else tnRight = null;

					
					// Knoten von "Second" in die Struktur von "First" einfügen:
					if ((tnRight != null) && (tnRight.State  == enumStates.New)){
						second.Remove(tnRight);
						if ((posLeft+1)<first.Count){
							if (posLeft  >  -1)
								first.Insert(posLeft+1,tnRight);
							else
								first.Insert(0,tnRight);
						}else{
							first.Add(tnRight);
						}
						posRight--;
						continue;
					}
					
					// Elemente, die nur in "First" vorkommen, nicht vergleichen:
					if ((tnLeft != null) && (tnLeft.State == enumStates.Removed)){
						// nicht verschieben
						posLeft--;
						continue;
					}
					
					// Gleiche Knoten akzeptieren:
					if ((tnRight != null )
					    &&(tnLeft != null)
					    && (tnRight.State == tnLeft.State)
					   )
						if (tnRight.Key == tnLeft.Key){
						TagArray( tnLeft, tnRight);
						posLeft--;
						posRight--;
						continue;
					}
					
					// eventuell muss der passende Knoten noch gesucht werden:
					if (tnRight != null){
						posRight--;
						if (posLeft+1 < first.Count){
							if (first[posLeft+1].Key == tnRight.Key){
								TagArray( first[posLeft+1], tnRight);
								continue;
							}
						}
					}
					
					if (tnLeft != null){
						tnLeft.Image=enumImages.Star;
						posLeft--;
						if (posRight+1 < second.Count){
							if (second[posRight+1].Key == tnLeft.Key){
								posRight++;
								TagArray( tnLeft, second[posRight]);
							}
						}
					}
				}
			}catch (Exception ex){
				clsLogging.GEH(ex);
			}
		}

		// hängt an die Tag Eigenschaft von tnLeft drei Tabellen
		private int TagArray( DBTreeNode tnLeft, DBTreeNode tnRight){
			int differences =0;
			try{
				DataTable objLeft = clsDataTable.GetValidData(tnLeft.Tag);
				DataTable objRight = clsDataTable.GetValidData(tnRight.Tag);
				
				if ((objRight == null)&&(objLeft == null)){
					objRight = clsDataTable.GetValidData(tnRight);
					objLeft = clsDataTable.GetValidData(tnLeft);
				}
				
				if ((tnLeft.Tag==null)||
				    (!tnLeft.Tag.GetType().ToString().Equals(typeof(DataTable).ToString()+"[]"))){
					
					DataTable result = new DataTable();
					
					differences = clsDataTable.compareDataTable( (DataTable)objLeft,(DataTable)objRight,
					                                            ref result,OptionOnlyDifferences );
					tnLeft.Tag = new DataTable[]{(DataTable)objLeft,(DataTable)objRight, result};
					
				}
			}catch (Exception ex){
				clsLogging.GEH(ex);
			}
			return differences;
		}
		#endregion
		
		
	}
	
}
