/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas M�ller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#region using
using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Windows.Forms;
#endregion


namespace JETDBComp.Model
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class clsSQLParser
	{
		#region private members
		private List<string> malKeyWords= new List<string>();
		private List<string> malSectionWords= new List<string>();
		private Dictionary<string,string> mdicPreAndSufFixes = null;
		
		private const string PRE = "LITERAL_PREFIX";
		private const string SUF = "LITERAL_SUFFIX";
		#endregion
		
		// initializes Private Members.
		public clsSQLParser(clsDAL dal)
		{
			malKeyWords.AddRange(	new String[] {"INTO","INNER","LEFT","RIGHT","JOIN","ON",
			                   	"AND","NOT","OR","DISTINCTROW","DISTINCT"});
			foreach (string word in dal.Keywords){
				malKeyWords.Add( word.ToUpper() );
			}
			
			mdicPreAndSufFixes = new Dictionary<string,string>();
			mdicPreAndSufFixes.Add("[","]");
			mdicPreAndSufFixes.Add("\"","\"");
			
			foreach (DataRow dr in ((DataTable)dal.Metadata[OleDbSchemaGuid.Provider_Types]).Rows ){
				if ( ! dr.IsNull(PRE)){
					string suffix;
					string prefix = dr[PRE].ToString();
					if (! dr.IsNull(SUF))
						suffix = dr[SUF].ToString();
					else
						suffix="";
					if (!mdicPreAndSufFixes.ContainsKey(prefix)){
						mdicPreAndSufFixes.Add(prefix,suffix);
					}
				}
			}
			
			malSectionWords.AddRange(	new String[] {"PARAMETERS","SELECT","INSERT","UPDATE",
			                       	"FROM","WHERE","ORDER","BY","HAVING","GROUP"});

		}
		
		#region private functions
		
		private int parseToken(ref int offset, string strBuffer){

			bool ends_with_Prefix = false;
			string strToken=strBuffer.Substring(0,offset+1);
			string debug= strToken.Substring(Math.Max (0,strToken.Length-10));
			string prefix = null;
			string suffix = null;
			
			foreach (string p in this.mdicPreAndSufFixes.Keys){
				ends_with_Prefix = strToken.EndsWith(p);
				if (ends_with_Prefix ) {
					prefix = p;
					suffix = mdicPreAndSufFixes[prefix].ToString();
					break;
				}
			}
			
			if (ends_with_Prefix)
			{
				if (suffix.Length > 0){
					int pos = strBuffer.IndexOf(suffix,offset+1);
					debug =  strBuffer.Substring(offset,pos-offset+1);
					offset = pos+suffix.Length -1;
					return  debug.Length;
				}else{
					int pos1 =  (strBuffer+" ").IndexOf(" ",offset+1);
					int pos2 = strBuffer.IndexOf(",",offset+1);
					int pos=0;
					if (pos2<0) pos =pos1;
					else pos = Math.Min(pos1,pos2);
					
					debug =  strBuffer.Substring(offset,pos-offset);
					offset = pos+suffix.Length -1;
					return  debug.Length;
				}
			}
			return 0;
		}
		
		private DBTreeNode addSectionNode(string strToken,int offset,DBTreeNode section)
		{
			if (strToken.Substring(0,offset).Trim()!=""){
				section.Add(strToken.Substring(0,offset).Trim());
				section=section.Parent.Add(strToken.Substring(offset).Trim().ToUpper() );
			}else if (section.Count > 0){
				section=section.Parent.Add(strToken.Substring(offset).Trim().ToUpper() );
			} else{
				section.Text =(section.Text+' '+strToken).Trim();
				if (section.Key.Trim()=="")
					section.Key=section.Text;
			}
			return section;
		}
		
		private void addLineNode(ref string strToken,ref int offset,DBTreeNode section){
			string prefix = strToken.Substring(0,offset).Trim();
			string trim = strToken.Substring(offset).Trim();
			string upper = trim.ToUpper();
			
			if (prefix != ""){
				section.Add(prefix);
				section.Add(trim);
			}else if (section.Count>0) {
				prefix = section[section.Count-1].Text.Trim().ToUpper();
				if ((malKeyWords.Contains( prefix )) &&
				    (malKeyWords.Contains(upper))){
					section[section.Count-1].Text = prefix+' '+upper;
				}else
					section.Add(trim);
			}else
				section.Add(trim);
			strToken="";
			offset=0;
		}
		
		private int parseSubStatement(string strToken, int offset, string strBuffer, int position,DBTreeNode section){
			strToken = strToken.Trim();
			if (strToken.Length > 1){
				string subToken = strToken.Substring(0,strToken.Length-1);
				int dummy=0;
				this.addLineNode(ref subToken,ref dummy,section);
			}
			string substatement= strBuffer.Substring(position+1);
			DBTreeNode subelements = section.Add("(");
			int substatementlength = parseSQL(substatement,subelements);
			
			if ((subelements.Count == 1)&&(subelements[0].Text == "")){
				
				while (subelements[0].Count > 0){
					DBTreeNode node=subelements[0][0];
					subelements[0].Remove(node);
					subelements.Add(node);
				}
				subelements.Remove(subelements[0]);
			}
			position+=substatementlength+1;
			section.Add(")");
			return position;
		}
		
		#endregion
		private static bool isNoLetter(char value){
			int intvalue = (int)value & (~32);
			return ( intvalue < 65 )
				|| ( 90 < intvalue );
		}
		
		public int parseSQL(String strSQLString,DBTreeNode parent){
			string strToken = "";
			DBTreeNode section = parent.Add("");
			int offset =0;
			for (int position=0; position < strSQLString.Length;position++){
				char value = strSQLString[position];
				if (value == '\n') value =' ';
				strToken = strToken + value;
				//if (value==' ') continue;
				if (((int)value) < 65)
					offset=strToken.Length;
				
				int a = 0;
				int oldPosition = position;
				if ((value=='x') || isNoLetter(value) )
					a = parseToken(ref position,strSQLString );
				
				if (a > 0) {
					strToken += strSQLString.Substring(oldPosition+1,position-oldPosition);
					offset = strToken.Length - a;
					string debug = strToken.Substring(offset);
					this.addLineNode(ref strToken,ref offset,section);
					continue;
				}
				
				if (value == '('){
					position = parseSubStatement(strToken,offset,strSQLString, position,section);
					strToken="";
					offset=0;
				}else if (malSectionWords.Contains(strToken.Substring(offset).Trim().ToUpper())){
					if ((position+1 < strSQLString.Length)&&( ((int)strSQLString[position+1])<65))
					{
						section = this.addSectionNode(strToken,offset,section);
						strToken="";
						offset=0;
					}
				}else if (malKeyWords.Contains(strToken.Substring(offset).ToUpper()))
					if ((position+1 < strSQLString.Length)&&( ((int)strSQLString[position+1])<65))
				{
					this.addLineNode(ref strToken,ref offset,section);
				}
				if (value==')'){
					strToken=strToken.Trim();
					if (strToken.Length>1){
						offset=0;
						strToken = strToken.Substring(0,strToken.Length-1);
						this.addLineNode(ref strToken,ref offset,section);
						
					}
					return position;
				}
				if (value == ','){
					offset=0;
					this.addLineNode(ref strToken,ref offset,section);
				}
			}
			section.Add(strToken);
			return strSQLString.Length;
		}

	}
}
