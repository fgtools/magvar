﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Data.OleDb;

namespace JETDBComp.Model
{
	/// <summary>
	/// Description of clsLoadData.
	/// </summary>
	public class clsLoadData
	{
		private clsDAL mDAL;
		
		public clsLoadData(){
			mDAL= new clsDAL();
		}


		#region load table data
		public void loadAllTablesData(DBTreeNode root,string strFilename){
			this.mDAL.Filename =strFilename;
			OleDbConnection conOle = mDAL.Open();
			try{
				foreach(DBTreeNode group in root){
					string strName =group.Key;
					switch(strName){
						case "Tabellen":
						case "Views":
						case "Procedures":
							loadGroupsData(conOle, group);
							break;
						case "Schemas":
							// data already loaded --> nothing to do.
							break;
						default:
							// load method not implemented yet
							clsLogging.LOG(enumMsgType.Info,"["+group.Key+"]","load method not implemented yet");
							break;
					}
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}finally{
				mDAL.Close(conOle);
				conOle =null;
			}
		}

		private void  loadGroupsData(OleDbConnection conOle,DBTreeNode group){
			string strType;
			foreach (DBTreeNode tn in group){
				strType=group.Key;
				try{

					this.mDAL.loadTableData(tn,conOle);
				}catch(Exception sqlex){
					clsLogging.LOG(enumMsgType.Fail,
					               sqlex.Message,sqlex.GetType().ToString()+'\n'+sqlex.Message+
					               '\n'+tn.State.ToString());
				}
			}
		}
		#endregion
		
	}
}
