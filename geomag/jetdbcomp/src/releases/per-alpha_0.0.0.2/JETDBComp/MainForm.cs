﻿/*
    JET DATABASE COMPARE allows comparing two arbitrary MDB-Files.
    Copyright (C) 2009  Andreas Möller

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#region using
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using JETDBComp.Model;
#endregion

namespace JETDBComp
{
	public partial class MainForm : Form
	{
		
		public MainForm()
		{
			InitializeComponent();
			mobjBL= new clsBusinessLayer( new clsDAL());
			this.uctlStructure1.Init(mobjBL);
		}
		
		// Linke und Rechte Seite
		private void MainFormSizeChanged(object sender, System.EventArgs e)
		{
			this.panel1.SuspendLayout();
			this.uctlMaster.Left=0;
			this.uctlMaster.Width =(this.Width-10) >> 1;
			this.uctlTarget.Left=this.uctlMaster.Width + 2;
			this.uctlTarget.Width=this.Width-10 - this.uctlMaster.Width ;
			this.panel1.ResumeLayout();
		}
		
		void UctlMasterFileChanged(object sender, EventArgs e)
		{
			string[] filenames = new string[2];
			filenames[0] = this.uctlMaster.Filename;
			filenames[1] = this.uctlTarget.Filename;
			muctlSQL.SetDatabaseList(filenames);
			uctlStructure1.SetFileNames(filenames);
		}
		
		void UctlLogBook1Error()
		{
			this.tcTabs.SelectedTab=this.tpLog;
		}
		

		
		void MainFormLoad(object sender, EventArgs e)
		{
			this.tcTabs.TabPages.Remove(this.tpScript);
		}
		
		
		void mnuCompareClick(object sender, EventArgs e)
		{
			uctlStructure1.Compare();
			this.tcTabs.SelectedTab=this.tpStructure;
		}
		

		
		void MnuAboutClick(object sender, EventArgs e)
		{
			this.tcTabs.SelectedTab   = this.tpInfo;
		}
		
		void MnuMasterClick(object sender, EventArgs e)
		{
			uctlMaster.Open(sender,e);
		}
		
		void MnuTargetClick(object sender, EventArgs e)
		{
			uctlTarget.Open(sender,e);
		}
		
		private Model.clsBusinessLayer mobjBL;

		void MnuOptionClick(object sender, EventArgs e)
		{
			GUI.clsOptions objOptions = null;
			try{
				objOptions = new GUI.clsOptions();
				objOptions.SystemTables = mobjBL.OptionSystem;
				objOptions.AccessTables = mobjBL.OptionAccess;
				objOptions.Table = mobjBL.OptionTables;
				objOptions.Views = mobjBL.OptionViews;
				objOptions.Procedures = mobjBL.OptionProcedures;
				objOptions.Order = mobjBL.OptionPosition;
				objOptions.Description = mobjBL.OptionDescription;
				objOptions.CompareData = mobjBL.OptionData;
				objOptions.DifferencesOnly=mobjBL.OptionOnlyDifferences;
				objOptions.Provider = "Micorsoft.JET.OleDB 4.0";		
				if( objOptions.ShowDialog() == DialogResult.OK){
					mobjBL.OptionSystem	=	objOptions.SystemTables;
					mobjBL.OptionAccess	=	objOptions.AccessTables;
					mobjBL.OptionTables	=	objOptions.Table;
					mobjBL.OptionViews	=	objOptions.Views;
					mobjBL.OptionProcedures	=	objOptions.Procedures;
					mobjBL.OptionPosition	=	objOptions.Order;
					mobjBL.OptionDescription	=	objOptions.Description;
					mobjBL.OptionData = objOptions.CompareData;
					mobjBL.OptionOnlyDifferences = objOptions.DifferencesOnly; 
				}
			}catch(Exception ex){
				clsLogging.GEH(ex);
			}
			finally{
				if (objOptions == null){
					objOptions.Dispose();
				}
			}
		}
	}
}
