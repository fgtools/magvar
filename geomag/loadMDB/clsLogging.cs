﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace loadMDB
{
    public enum enumMsgType : int
    {
        Info,
        Error,
        Warning,
        Success,
        Fail
    }

    public class clsMessage
    {

        #region private member

        private enumMsgType mmtMessageType;

        private string mstrSection;

        private string mstrTitle;

        private string mstrDescription;

        private DateTime mdtTimeStamp;

        #endregion

        #region public properies

        public enumMsgType Type
        {
            get { return mmtMessageType; }
        }

        public string Section
        {
            get { return mstrSection; }
        }

        public string Title
        {
            get { return mstrTitle; }
        }

        public string Description
        {
            get { return mstrDescription; }
        }

        public DateTime TimeStamp
        {
            get { return mdtTimeStamp; }
        }

        #endregion

        #region constructor
        /// <summary>
        /// Logging-Eintrag
        /// </summary>
        /// <param name="mtType">Kind of entry: info, error, warning, success oder fail.</param>
        /// <param name="strSection">Category</param>
        /// <param name="strTitle">First Line</param>
        /// <param name="strDescription">More Detailed Description.</param>
        public clsMessage(enumMsgType mtType, string strSection, string strTitle, string strDescription)
        {
            mmtMessageType = mtType;
            mstrSection = strSection;
            mstrTitle = strTitle;
            mstrDescription = strDescription;
            mdtTimeStamp = System.DateTime.UtcNow;
        }
        #endregion
    }


    /// <summary>
    /// Description of MyClass.
    /// </summary>
    public class clsLogging
    {

        #region " Event Message"
        public delegate void dlgMessage(clsMessage msg);
        public static event dlgMessage Message;
        #endregion

        #region  private Singleton pattern
        private static readonly clsLogging mobjLogging = new clsLogging();

        private clsLogging()
        {
        }

        private static clsLogging Instance
        {
            get
            {
                return mobjLogging;
            }
        }

        #endregion

        #region public static functions

        // composes a Message and invokes a "Message"-Event
        public static void LOG(enumMsgType mtType, string strTitle, string strMessage)
        {
            clsMessage msg = new clsMessage(mtType, "Log", strTitle, strMessage);
            if (Message != null)
                Message(msg);
            else
                Console.WriteLine(msg.Section + " " + msg.Title + " " + msg.Description);
        }

        // invokes the "Message"-Event
        public static void LOG(clsMessage msg)
        {
            if (Message != null)
                Message(msg);
            else
                Console.WriteLine(msg.Section + " " + msg.Title + " " + msg.Description);
        }

        // composes an Error-Message and invokes ehe Message Event.
        public static void GEH(Exception ex)
        {
            clsMessage msg = new clsMessage(enumMsgType.Error, "GEH", ex.Message, ex.GetType().ToString() + "\n" + ex.StackTrace);
            try
            {
                if (Message != null)
                    Message(msg);
                else
                    Console.WriteLine(msg.Section + " " + msg.Title + " " + msg.Description);
            }
            catch (Exception)
            {
                throw ex;
            }
        }

        #endregion

    }
}

