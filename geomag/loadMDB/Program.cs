﻿

using System;
using System.Collections.Generic;
//using System.Collections.Generic.List;
using System.Linq;
using System.Text;
using System.Data;  // for DataTable
using System.Data.OleDb;

/* =======================================
 * First thought was to do this manually, BUT then got the idea to completely enumerate
 *   the table, so this NOT needed
 *  string[] Tables = new string[] { "ALTRESULT", "CONTINENTS", "COUNTRY", "CROSSREF",
 *                "FIELDTESTS", "INFORMATION", "JOURNAL", "PMAGRESULT", "REFERENCE", "ROCKUNIT",
 *               "SUMMARY", "TERRANE", "TIMESCALE", "VERSION"  };
 *  string[] VERSION = new string[] { "NO", "MONTH_YEAR", "DATA", "COMMENTS" };
 *  string[] TIMESCALE = new string[] { "NO", "SYSTEM PERIOD", "SERIES EPOCH", "STAGE", "SYMBOL", "TOP", "BASE" };
 *  string[] PMAGRESULT = new string[] { "ROCKUNITNO", "RESULTNO", "COMPONENT", "LOMAGAGE", "HIMAGAGE", "TESTS",
 *               "TILT", "SLAT", "SLONG", "B", "N", "DEC", "INC", "KD", "ED95", "PLAT", "PLONG", "PTYPE",
 *               "DP", "DM", "ANTIPODAL", "N_NORM", "D_NORM", "I_NORM", "K_NORM", "ED_NORM",
 *               "N_REV", "I_REV", "K_REV", "ED_REV", "DEMAGCODE", "TREATMENT", "LABDETAILS", 
 *               "ROCKMAG", "N_TILT", "D_UNCOR", "I_UNCOR", "K1", "ED1", "D_COR", "I_COR", "K2",
 *               "ED2", "COMMENTS", "STATUS" }; // 9259 results
 */

// ==============================
namespace loadMDB
{
    public struct Columns
    {
        public string name;
        public int type;
    }

    public class TableInfo
    {
        public string name;
        public List<Columns> cols;
        public string csv;
        public TableInfo(string TableName)
        {
            name = TableName;
            cols = new List<Columns>();
            csv = "";
        }
        public void AddColumn(string name, int type)
        {
            Columns col;
            col.name = name;
            col.type = type;
            cols.Add(col);
        }
        public void AddCSV(string stg)
        {
            csv = stg;
        }
    }

    class Program
    {
        static System.IO.StreamWriter file = null;

        static void GetTableInfo(OleDbConnection conOle, List<TableInfo> ti)
        {
            DataRowView drvCols;
            DataTable dtCols;
            DataView dvColumns;
            TableInfo tabInfoObj;
            string sTableNm;
            //string sNamePrefix;
            string sColName, str;
            int nColType;
            int i, max, j, ncol;
            // Get the list of tables that there are. 
            DataTable schemaTab = conOle.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                new Object[] { null, null, null, "TABLE" });
            max = schemaTab.Rows.Count;
            str = String.Format("GetOleDbSchemaTable returned {0} tables.", max);
            prt(str);
            for (i = 0; i < max; i++)
            {
                sTableNm = schemaTab.Rows[i].ItemArray[2].ToString();
                //sNamePrefix = sTableNm.Substring(0, 4);
                if (sTableNm != "_SKIP_THIS_TABLE")
                {   // && (sNamePrefix != "temp")) {
                    tabInfoObj = new TableInfo(sTableNm);
                    // Get the column name and its type and sort them 
                    // according to their position in the table. 
                    dtCols = conOle.GetOleDbSchemaTable(OleDbSchemaGuid.Columns,
                        new Object[] { null, null, sTableNm, null });
                    dvColumns = new DataView(dtCols);
                    dvColumns.Sort = "ORDINAL_POSITION";
                    // Get the column names and their types.
                    ncol = dvColumns.Count;
                    str = String.Format("{0}: Table {1}, with {2} columns.", (i + 1), sTableNm, ncol);
                    prt(str);
                    str = "";
                    for (j = 0; j < ncol; j++)
                    {
                        // Get the name of the column. 
                        drvCols = dvColumns[j];
                        sColName = drvCols.Row.ItemArray[3].ToString();
                        // Get columns data type code and save it off. 
                        nColType = Convert.ToInt32(drvCols.Row.ItemArray[11]);
                        tabInfoObj.AddColumn(sColName, nColType);
                        if (str.Length > 0)
                            str += ",";
                        str += sColName;
                    }
                    prt(str);
                    tabInfoObj.AddCSV(str);
                    ti.Add(tabInfoObj);
                } // if
            }
        }

        static void prt(string msg)
        {
            if (file == null) {
                file = new System.IO.StreamWriter("tempcslog.txt");
            }
            Console.WriteLine(msg);
            file.WriteLine(msg);
        }

        static DataTable GetSchemaTable(OleDbConnection connection)
        {

            DataTable schemaTable = connection.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables,
                    new object[] { null, null, null, "TABLE" });
            return schemaTable;
        }

        static void writeCSVFile(string file, string csv)
        {
            System.IO.StreamWriter f = new System.IO.StreamWriter(file);
            f.Write(csv);
            f.Close();
        }

        static void Main(string[] args)
        {
            string str;
            int i;
            int count = 0;
            //DataTable dt;
            string tn, tmp, csv_stg, csv_file;
            clsDAL mDAL = new clsDAL();
            int cnt2 = 0;
            List<string> DataBases = new List<string>();
            List<TableInfo> TableInfo = new List<TableInfo>();
            string eol = "\r\n";    // TODO: Change this per OS
            foreach (string arg in args)
            {
                count++;
                str = String.Format("args[" + count + "]: {0}", arg);
                prt(str);
                mDAL.Filename = arg;
                try
                {
                    OleDbConnection conOle = mDAL.Open();
                    str = String.Format("ServerVersion: {0} \nDatabase: {1}",
                            conOle.ServerVersion, conOle.Database);
                    prt(str);
                    GetTableInfo(conOle, TableInfo);
                    mDAL.Close(conOle);

                    tn = "TIMESCALE";
                    foreach (TableInfo ti in TableInfo)
                    {
                        tn = ti.name;
                        csv_file = tn + ".csv";
                        csv_stg = ti.csv + eol;
                        conOle = mDAL.Open();

                        string mySelectQuery = "SELECT * FROM " + tn;

                        OleDbTransaction myTrans = conOle.BeginTransaction();
                        OleDbCommand myCommand = new OleDbCommand(mySelectQuery, conOle, myTrans);
                        myCommand.CommandTimeout = 20;
                        try
                        {
                            OleDbDataReader reader = myCommand.ExecuteReader();
                            str = String.Format("Enumberating table {0}", tn);
                            prt(str);
                            while (reader.Read())
                            {
                                cnt2 = reader.VisibleFieldCount;
                                str = "";
                                //str = reader.GetString(0); // failed with type problem
                                for (i = 0; i < cnt2; i++)
                                {
                                    tmp = reader.GetValue(i).ToString();
                                    //if (tmp.Length > 0)
                                    //{
                                        if (str.Length > 0)
                                            str += ",";
                                        str += tmp;
                                    //}
                                }
                                //if (str.Length > 0)
                                //{
                                    prt(str);
                                    csv_stg += str + eol;
                                //}

                            }
                            reader.Close();
                            writeCSVFile(csv_file, csv_stg);
                            //NO SUCH THING myTrans.Close();
                        }
                        catch (OleDbException ex)
                        {
                            // clsLogging.LOG(new clsMessage(enumMsgType.Fail, ex.Message, "SQL ", mySelectQuery + "\n" + conOle.ConnectionString));
                            //clsLogging.LOG(new clsMessage(enumMsgType.Fail, ex.Message, "SQL ", mySelectQuery));
                            str = String.Format("{0} {1} {2}", ex.Message, "SQL ", mySelectQuery);
                            prt(str);
                        }
                        mDAL.Close(conOle);
                    }
                    conOle = null;
                } 
                catch (Exception ex)
                {
                    str = String.Format("Exception: {0}", ex.Message);
                    prt(str);
                }

            }
            if (file != null)
                file.Close();
        }   // end main()
    }
}
