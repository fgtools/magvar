README.txt - 20140314

from : http://www.ngdc.noaa.gov/geomag/paleo.shtml

DWN: 26/02/2005  17:21         8,773,632 GD2004C.MDB
to : C:\GTools\ConApps\magvar\geomag

XP Access Export to CSV

478,802 GD2004C.csv

"SLAT","SLONG","LOMAGAGE","HIMAGAGE","TESTS","DEMAGCODE","B","N","KD","ED95","PLAT","PLONG","DP","DM"
31.60,-7.30,228,245,"N",2,15,62,27.00,7.50,72.00,254.50,7.50,7.50
19.00,33.30,78,83,"M",2,6,54,,,55.90,277.80,11.30,11.30
18.10,33.70,569,587,"M",2,6,53,14.50,18.00,67.90,314.10,18.80,18.80
...


Exploring more of GD2004C.MDB in Access

Structure of GPMDB (Global PaleoMagnetic DataBase)

[ REFERENCES ] <-->> [ ROCKUNIT ] <-->>  [ PMAGRESULT ] <-- [ALTRESULT/FIELDTESTS/CROSSREF]

Lookup Tables

[TIMESCALE] [CONTINENTS] [TERRANES] [COUNTRIES] [INFORMATION (SYMBOLS)] [VERSION]