# Magetic Variation (Declination)

This wiki http://en.wikipedia.org/wiki/Magnetic_declination give a reasonable 
explanation of magnetic variation, or magnetic declination.

This is the build of three programs that calculate the magnetic variation 
given a specific wsg84 latitude and longitude.

fg_magvar - uses the simgear core library

testmagvar - much the same thing

wmm_magvar - a calculator using the Geomagnetism Library. Given a latitude and longitude in degrees, see http://www.ngdc.noaa.gov/geomag/WMM/soft.shtml for public domain source, to calculate the magnetic deviation of that point on the earth. Last updated 20210109, to use WMM.COF table, dated: 2020.0 WMM-2020 12/10/2019.

### Prerequisites:

 1. git - http://git-scm.com/downloads - to clone the repo
 2. cmake - http://www.cmake.org/download/ - to configurate and generate the native build files
 3. build tools - this can be MSVC in Windows, and gcc/make in unix
 4. simgear - https://sourceforge.net/p/flightgear/simgear/ci/next/tree/
 
Simgear is part of the FlightGear Project - https://sourceforge.net/projects/flightgear/. Building 
simgear from source is not an easy task, since it has a considerable number of dependencies.

The simgear library binaries for various platforms are available from http://build.flightgear.org:8080/.
And the above repo source contains all the simgear headers.

### Building:

This project uses CMake to configure, in this case find the simgearcore library, and generate 
various native build files. This most common is perhaps 'Unix Makefiles'. In Windows, many 
versions of MSVC - https://www.visualstudio.com/en-us/products/free-developer-offers-vs.aspx.

In unix/linux, the build steps are -
 1. cd build
 2. cmake ..
 3. make
 
In Windows, the build steps are -
 1. cd build
 2. cmake ..
 3. cmake --build . --config Release
 
The build directory contains a build-me.bat for windows, and a build-me.sh for unix. These 
scripts can probably be used, with some minor adjustments to suit your particular environment, 
particularly where simgear is installed.

CMake also has a GUI which some prefer.

### Running:

Each app should offer some help running it with, no command, or -h, -?

These apps were mainly for testing and exploring how to calculate the magnetic variation 
of a particular location, on a particular date. Not a lot of effort has gone into making 
them 'smooth' user apps.

Have FUN!  
Geoff.  
20210109 - 20150404

; eof
