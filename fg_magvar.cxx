/* 
   20140313 - Make into a useful app - geoff
   2/14/00 fixed help message- dip angle (down positive), variation (E positive) 
 */
#ifdef _MSC_VER
#pragma warning( disable : 4996 )
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <simgear/constants.h>

#include "coremag.hxx"

#ifndef MV_VERSION
#define MV_VERSION "0.0.9999"
#endif

#define IS_DIGIT(a) (( a >= '0' ) && (a <= '9'))

static int verbose = 0;
static int yy,mm,dd;
static double lat_deg = 200.0;
static double lon_deg = 200.0;
static double h = 0.0;
#ifdef _MSC_VER
#define M_ISDIR _S_IFDIR
#else
#define M_ISDIR __S_IFDIR
#endif

static struct stat stat_buf;
// return: 0 = not file or directory, 1 = file, 2 = directory, 
int is_file_or_directory( char *file )
{
    int iret = 0;
    if (stat(file,&stat_buf) == 0) {
        if (stat_buf.st_mode & M_ISDIR) 
            iret = 2;
        else
            iret = 1;
    }
    return iret;
}


// rought is a decimal service
int is_a_decimal( char *arg )
{
    int i, c, digs = 0, haddec = 0;
    int len = (int)strlen(arg);
    if (len == 0) return 0; // NOT a decimal
    for (i = 0; i < len; i++) {
        c = arg[i];
        if (IS_DIGIT(c)) {
            digs++;
        } else if (c == '.') {
            if (haddec) return 0;
            haddec++;
        } else if (c == '-') {
            if (i) return 0;
        } else {
            return 0;
        }
    }
    if (!digs) return 0;
    return 1;
}

int in_world_range( double lat, double lon ) 
{
    if ((lat < -90.0)||(lat > 90.0)||
        (lon < -180.0)||(lon > 180.0)) {
            return 0;
    }
    return 1;
}

char *get_file_name(char *path)
{
    char *name = path;
    int len = (int)strlen(path);
    int i, c, i2;
    for (i = 0; i < len; i++) {
        c = path[i];
        i2 = i + 1;
        if ((c == '\\')||(c =='/')) {
            if (i2 < len)
                name = &path[i2];
        }
    }
    return name;
}

void give_help( char *vers, char *name )
{
    printf("%s, version %s\n", get_file_name(name), vers);
    printf("Usage: [options] lat_degs lon_degs\n");
    printf("Options:\n");
    printf(" --help  (-h or -?) = This help and exit(2)\n");
    printf(" --verbose     (-v) = Bump verbosity, to show ALL values in output (def=%s)\n",
        (verbose ? "on" : "off") );
    printf(" --alt         (-a) = Altitiude in km AMSL (def=%.1lf)\n", h);
    printf(" --day <num>   (-d) = Day (1-31) (def=%d)\n", dd);
    printf(" --month <num> (-m) = Month (1-12) (def=%d)\n", mm);
    printf(" --year <num>  (-y) = Year - last 2 digits (00-15) (def=%d)\n", yy);
    printf("Note, the Year give is NOT checked to be in the valid range given ablove.\n");
    printf("The default model is IGRF2000, valid 1/1/00 - 12/31/05\n");

    printf("\n");
    printf("Given a latitude and longitude in degrees, use part of the SimGear library,\n");
    printf("see http://flightgear.org for source and details,\n");
    printf("to calculate the magnetic deviation of that point on the earth.\n");
    printf("\nThe SG source code is under a GNU Library General Public License, version 2.\n");
}


int parse_command( int argc, char **argv )
{
    int i, c, i2, j, cnt = 0;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        if ((*arg == '-') && !is_a_decimal(&arg[1])) {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            i2 = i + 1;
            switch (c) {
            case 'h':
            case '?':
                give_help((char *)MV_VERSION, argv[0]);
                return 2;
                break;
            case 'a':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    h = atof(sarg);
                } else {
                    printf("Expected altitude to follow %s\n", arg);
                    return 1;

                }
                break;
            case 'y':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    yy = atoi(sarg);
                } else {
                    printf("Expected year to follow %s\n", arg);
                    return 1;

                }
                break;
            case 'm':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    mm = atoi(sarg);
                } else {
                    printf("Expected month to follow %s\n", arg);
                    return 1;

                }
                break;
            case 'd':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    dd = atoi(sarg);
                } else {
                    printf("Expected day to follow %s\n", arg);
                    return 1;
                }
                break;
            case 'v':
                verbose++;
                break;

            default:
                printf("Unknown argument %s... aborting...\n", arg);
                return 1;
            }
        } else {
            // bare arg - assum lat,lon
            if (cnt == 0) {
               j = sscanf(arg, "%lf", &lat_deg);
               if ((j == 1) && is_a_decimal(arg)) {
                   // this is ok
               } else {
                   printf("First argument not a decimal latitudes! %s\n",arg);
                   return 1;
               }
            } else if (cnt == 1) {
               j = sscanf(arg, "%lf", &lon_deg);
               if ((j == 1) && is_a_decimal(arg)) {
                   // this is ok
               } else {
                   printf("Second argument not a decimal longitude! %s\n",arg);
                   return 1;
               }

            } else {
                printf("Already have lat %lfm, lon %1f. What is this %s!\n",lat_deg,lon_deg,arg);
                printf("Unknown argument %s... aborting...\n", arg);
                return 1;
            }
            cnt++;
        }
    }
    if ((cnt != 2) || !in_world_range(lat_deg, lon_deg)) {
        if (cnt != 2)
            printf("Error: Did NOT find lat-deg lon-deg in command!\n");
        else
            printf("Error: Invalid lat %lf, or lon %lf\n", lat_deg, lon_deg );
        return 1;
    }

    return 0;
}


int main(int argc, char *argv[])
{
    double var;
    int cnt = 0;
    double field[6];
    time_t rawtime;
    struct tm * timeinfo;
    time (&rawtime);
    timeinfo = localtime ( &rawtime );
    if (!timeinfo) {
        fprintf(stderr,"function locatime failed! aborting...\n");
        return 1;

    }
    h  =  timeinfo->tm_hour;
    yy = timeinfo->tm_year + 1900 - 2000;
    mm = timeinfo->tm_mon + 1;
    dd = timeinfo->tm_mday;

    cnt = parse_command(argc,argv);
    if (cnt)
        return cnt;

    var = calc_magvar( SGD_DEGREES_TO_RADIANS * lat_deg, SGD_DEGREES_TO_RADIANS * lon_deg, h,
		       yymmdd_to_julian_days(yy,mm,dd), field );

    if (verbose) {
        fprintf(stdout,"For lat %f, lon %f, date  h %d dd/mm/yy %d/%d/%02d is -\n", lat_deg, lon_deg, (int)h, dd, mm, yy ); 
        fprintf(stdout,"%6.0f %6.0f %6.0f\n", field[0], field[1], field[2] );
        fprintf(stdout,"%6.0f %6.0f %6.0f\n", field[3], field[4], field[5] );
        fprintf(stdout,"%6.0f %6.0f %6.0f %4.2f %4.2f \n",
          field[3],field[4],field[5],
          SGD_RADIANS_TO_DEGREES * (atan(field[5]/pow(field[3]*field[3]+field[4]*field[4],0.5))),
          SGD_RADIANS_TO_DEGREES * var);
        printf("\n");
    }

    printf("Magnetic Variation: %.2lf degrees.\n", SGD_RADIANS_TO_DEGREES * var );
    return 0;

}
  
// eof
