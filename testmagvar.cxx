/* 2/14/00 fixed help message- dip angle (down positive), variation (E positive) */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <simgear/constants.h>
#include "coremag.hxx"

static int verbose = 0;

void out_help()
{
    fprintf(stdout,"Usage: [-v] mag lat_deg lon_deg [alt_km yy mm dd]\n");
    fprintf(stdout,"N,E positive, S,W negative degrees, altitude in km, mm dd yy is date (2 digit year 00 to 05)\n");
    fprintf(stdout,"If alt_km not given, uses 0.0. If no year, month, or day then current date.\n");
    //fprintf(stdout,"model 1,2,3,4,5,6,7 <=> IGRF90,WMM85,WMM90,WMM95,IGRF95,WMM2000,IGRF2000\n");
    fprintf(stdout,"Default model is IGRF2000, valid 00 1 1 - 05 12 31\n");
    fprintf(stdout,"If -v, output Bx (N) By (E) Bz (down) (in nTesla) dip (degrees down positive)\n");
    fprintf(stdout,"and variation (degrees E positive)\n");
}

int main(int argc, char *argv[])
{
    /* args are double lat_deg, double lon_deg, double h, 
        int mm, int dd, int yy,int model */
    /* output N, E, down components of B (nTesla)
         dip angle (down positive), variation (E positive) */
    double lat_deg,lon_deg,h,var;
    int /* model,*/ i,yy,mm,dd;
    double field[6];
    char *arg;
    int  acnt = 0;

    if (argc < 3) {
        out_help();
        return 1;
    }

    // establish DEFAULTS
    time_t t = time(NULL);
	tm* timePtr = localtime(&t);
    h = 0;
    yy = timePtr->tm_year + 1900 - 2000;
    mm = timePtr->tm_mon + 1;
    dd = timePtr->tm_mday;
    // get arguments
    for (i = 1; i < argc; i++) {

        arg = argv[i];
        if ((strcmp(arg,"--help") == 0)||
            (strcmp(arg,"-h") == 0)||
            (strcmp(arg,"-?") == 0)) {
            out_help();
            return 0;
        } else if (strcmp(arg,"-v") == 0) {
            verbose = 1;
            continue;
        }
        acnt++;
        switch (acnt) {
        case 1:
            lat_deg = strtod(arg,NULL);
            break;
        case 2:
            lon_deg = strtod(arg,NULL);
            break;
        case 3:
            h =       strtod(arg,NULL);
            break;
        case 4:
            yy =     (int)strtol(arg,NULL,10);
            break;
        case 5:
            mm =     (int)strtol(arg,NULL,10);
            break;
        case 6:
            dd =     (int)strtol(arg,NULL,10);
            break;
        default:
            fprintf(stderr,"unknown argument [%s]\n",arg);
            fprintf(stderr,"already have lat %lf, lon %lf, alt %lf, year %d, month %d, day %d\n",
                lat_deg, lon_deg, h, yy, mm, dd );
            return 1;
        }
    }

    if (verbose) {
        fprintf(stdout,"for lat %lf, lon %lf, alt %lf, year %d, month %d, day %d\n",
                lat_deg, lon_deg, h, yy, mm, dd );
    }

    var = calc_magvar( SGD_DEGREES_TO_RADIANS * lat_deg, SGD_DEGREES_TO_RADIANS * lon_deg, h,
		   yymmdd_to_julian_days(yy,mm,dd), field );

    if (verbose) {
        fprintf(stdout,"%6.0f %6.0f %6.0f\n", field[0], field[1], field[2] );
        fprintf(stdout,"%6.0f %6.0f %6.0f\n", field[3], field[4], field[5] );
        fprintf(stdout,"%6.0f %6.0f %6.0f %4.2f\n",
            field[3],field[4],field[5],
            SGD_RADIANS_TO_DEGREES * (atan(field[5]/pow(field[3]*field[3]+field[4]*field[4],0.5))));
    }

    fprintf(stdout,"Magnetic Variation %4.2f degrees.\n",
        SGD_RADIANS_TO_DEGREES * var);

    return 0;
}
  
// eof


