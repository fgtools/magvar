@setlocal
@REM Update to DELL03 
@set DOINST=0
@set TMPLOG=bldlog-1.txt
@set TMPSG=D:\FG\next\install\msvc140-64
@set TMP3RD=D:\Projects\3rdParty.x64
@REM set TMPSG=X:\install\msvc100\simgear
@REM set TMP3RD=X:\3rdParty
@if NOT EXIST %TMPSG%\nul goto NOSG
@if NOT EXIST %TMP3RD%\nul goto NO3RD

@set TMPOPTS=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@if "%TMPOPTS%x" == "x" (
@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=C:/MDOS
)
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TMP3RD%

@echo Bgn %DATE% %TIME% > %TMPLOG%

@set SG_ROOT=%TMPSG%
@echo Set SG_ROOT=%SG_ROOT% to find simgear install

@echo Doing 'cmake .. %TMPOPTS%' output to %TMPLOG%
@echo Doing 'cmake .. %TMPOPTS%' >> %TMPLOG%
@cmake .. %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

@echo Doing 'cmake --build . --config Debug'
@echo Doing 'cmake --build . --config Debug' >> %TMPLOG%
@cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

@echo Doing 'cmake --build . --config Release'
@echo Doing 'cmake --build . --config Release' >> %TMPLOG%
@cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@echo.
@echo Appears successful... see %TMPLOG% for details...
@echo.
@if "%DOINST%x" == "1x" goto ADD_INST
@echo Intall NOT configured... set DOINST=1
@goto END

:ADD_INST
@echo Continue with install of release fg_magvar, testmagvar and wmm_magvar?
@echo Only Ctrl+C aborts... all other keys continue
@echo.
@pause

@echo Doing 'cmake --build . --config Release --target INSTALL'
@echo Doing 'cmake --build . --config Release --target INSTALL' >> %TMPLOG%
@cmake --build . --config Release --target INSTALL >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

@echo Done build and install... see %TMPLOG% for details...

@echo.
@echo Some sites with magnetic declination calculators...
@echo http://www.geomag.nrcan.gc.ca/calc/mdcal-eng.php
@echo http://www.ngdc.noaa.gov/geomag-web/
@echo.
@echo Test of fg_magvar...
fg_magvar 37.618674211 -122.375007609
@if ERRORLEVEL 1 goto ERR5
@echo Test of wmm_magvar...
wmm_magvar 37.618674211 -122.375007609
@if ERRORLEVEL 1 goto ERR6
@echo Test of testmagvar...
testmagvar 37.618674211 -122.375007609
@if ERRORLEVEL 1 goto ERR7
@echo All done...

@goto END

:ERR1
@echo cmake error
@goto ISERR

:ERR2
@echo debug error
@goto ISERR

:ERR3
@echo release error
@goto ISERR

:ERR4
@echo install error
@goto ISERR

:ERR5
@echo run of fg_magvar failed
@goto ISERR

:ERR6
@echo run of wmm_magvar failed
@goto ISERR

:ERR7
@echo run of testmagvar failed
@goto ISERR

:NOSG
@echo Can NOT locate directory %TMPSG%! *** FIX ME *** to where simgear is installed
@goto ISERR

:NO3RD
@echo Can NOT locate the directory %TMP3RD%! *** FIX ME *** to where 3rd party libraries are installed
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof

