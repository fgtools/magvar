#!/bin/sh
#< run-tests.sh - run some tests on installed binaries
TMPLAT=37.618674211
TMPLON=-122.375007609

TMPFAIL=0

echo ""
echo "Run three tests for $TMPLAT $TMPLON..."
echo "Test of fg_magvar..."
fg_magvar $TMPLAT $TMPLON
if [ ! "$?" = "0" ]; then
    echo "fg_magvar exe failed!"
    TMPFAIL=`expr $TMPFAIL + 1`
fi
echo "Test of wmm_magvar..."
wmm_magvar $TMPLAT $TMPLON
if [ ! "$?" = "0" ]; then
    echo "wmm_magvar exe failed!"
    TMPFAIL=`expr $TMPFAIL + 1`
fi
echo "Test of testmagvar..."
testmagvar $TMPLAT $TMPLON
if [ ! "$?" = "0" ]; then
    echo "testmagvar exe failed!"
    TMPFAIL=`expr $TMPFAIL + 1`
fi
if [ ! "$TMPFAIL" = "0" ]; then
    echo "Had $TMPFAIL FAILURES!"
    exit 1
fi
TMPLAT=48.7269692925
TMPLON=2.3699923175

echo ""
echo "Run three tests for $TMPLAT $TMPLON..."
echo "Test of fg_magvar..."
fg_magvar $TMPLAT $TMPLON
if [ ! "$?" = "0" ]; then
    echo "fg_magvar exe failed!"
    TMPFAIL=`expr $TMPFAIL + 1`
fi
echo "Test of wmm_magvar..."
wmm_magvar $TMPLAT $TMPLON
if [ ! "$?" = "0" ]; then
    echo "wmm_magvar exe failed!"
    TMPFAIL=`expr $TMPFAIL + 1`
fi
echo "Test of testmagvar..."
testmagvar $TMPLAT $TMPLON
if [ ! "$?" = "0" ]; then
    echo "testmagvar exe failed!"
    TMPFAIL=`expr $TMPFAIL + 1`
fi
if [ ! "$TMPFAIL" = "0" ]; then
    echo "Had $TMPFAIL FAILURES!"
    exit 1
fi
echo ""
echo "Should only be a +/- 0.1 maximum difference.."
echo "Appears all test succeeded..."

#eof

