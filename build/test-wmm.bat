@setlocal
@set TMPEXE=Release\wmm_magvar.exe
@if NOT EXIST %TMPEXE% goto ERR1

%TMPEXE% -?
@echo For KSFO, San Francisco
%TMPEXE% -v 37.618674211 -122.375007609
@echo For LFPO, Paris
@%TMPEXE% 48.726969293 2.369992317

@goto END

:ERR1
@echo Error: Can NOT locate %TMPEXE%! Has it been built?
:END
