README.geomaglib.txt - 20140313

Notes while building wmm_magvar.c

1: Latitude is placed in CoordGeodetic->phi
            j = sscanf(buffer, "%lf", &CoordGeodetic->phi);

2: Longitude is placed in CoordGeodetic->lambda
            j = sscanf(buffer, "%lf", &CoordGeodetic->lambda);

3: Altitude is placed in CoordGeodetic->HeightAboveGeoid
            Geoid->UseGeoid = 1;
            j = sscanf(buffer, "%lf", &CoordGeodetic->HeightAboveGeoid);
            MAG_ConvertGeoidToEllipsoidHeight(CoordGeodetic, Geoid);

4: Month, Day, Year into MagneticDate->Month, MagneticDate->Day, MagneticDate->Year, and converted
            sscanf(buffer, "%d/%d/%d", &MagneticDate->Month, &MagneticDate->Day, &MagneticDate->Year);
            if(!MAG_DateToYear(MagneticDate, Error_Message)) {
                printf(Error_Message);
If date out of date
            if(MagneticDate->DecimalYear > MagneticModel->CoefficientFileEndDate || MagneticDate->DecimalYear < MagneticModel->epoch) {
                switch(MAG_Warnings(4, MagneticDate->DecimalYear, MagneticModel)) {

Then library does its magic...

            MAG_GeodeticToSpherical(Ellip, CoordGeodetic, &CoordSpherical); /*Convert from geodetic to Spherical Equations: 17-18, WMM Technical report*/

            MAG_TimelyModifyMagneticModel(UserDate, MagneticModels[0], TimedMagneticModel); /* Time adjust the coefficients, Equation 19, WMM Technical report */

            MAG_Geomag(Ellip, CoordSpherical, CoordGeodetic, TimedMagneticModel, &GeoMagneticElements); /* Computes the geoMagnetic field elements and their time change*/

            MAG_CalculateGridVariation(CoordGeodetic, &GeoMagneticElements);

            MAG_PrintUserData(GeoMagneticElements, CoordGeodetic, UserDate, TimedMagneticModel, &Geoid); /* Print the results */


Eh voila... for 37.618, -122.375, 0, 03/13/2014 -

 Results For

Latitude        37.62N
Longitude       122.38W
Altitude:       0.00 Kilometers above mean sea level
Date:           2014.2

                Main Field                      Secular Change
F       =       48612.0   nT              Fdot = -80.0  nT/yr
H       =       23503.7   nT              Hdot = -28.9  nT/yr
X       =       22821.7   nT              Xdot = -17.5  nT/yr
Y       =       5621.0    nT              Ydot = -49.8  nT/yr
Z       =       42552.4   nT              Zdot = -75.4  nT/yr
Decl    =      13 Deg  50 Min  (EAST)     Ddot = -6.5   Min/yr
Incl    =      61 Deg   5 Min  (DOWN)     Idot = -0.8   Min/yr

The SG magvar shows 
For lat 37.618674, lon -122.375008, date  h 17 dd/mm/yy 13/3/14 is -
-42348 -22512   5570
 22649   5570  42275
 22649   5570  42275 61.11 13.82

Looks somewhat SIMILAR!!! 


# eof


