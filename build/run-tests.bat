@setlocal

@set TMPLAT=37.618674211
@set TMPLON=-122.375007609

@echo Run three tests for %TMPLAT% %TMPLON% ...

@echo Test of fg_magvar...
@fg_magvar %TMPLAT% %TMPLON%
@if ERRORLEVEL 1 goto ERR5
@echo Test of wmm_magvar...
@wmm_magvar %TMPLAT% %TMPLON%
@if ERRORLEVEL 1 goto ERR6
@echo Test of testmagvar...
@testmagvar %TMPLAT% %TMPLON%
@if ERRORLEVEL 1 goto ERR7

@set TMPLAT=48.7269692925
@set TMPLON=2.3699923175

@echo Run three tests for %TMPLAT% %TMPLON% ...

@echo Test of fg_magvar...
@fg_magvar %TMPLAT% %TMPLON%
@if ERRORLEVEL 1 goto ERR5
@echo Test of wmm_magvar...
@wmm_magvar %TMPLAT% %TMPLON%
@if ERRORLEVEL 1 goto ERR6
@echo Test of testmagvar...
@testmagvar %TMPLAT% %TMPLON%
@if ERRORLEVEL 1 goto ERR7

@goto END

:ERR5
@echo fg_magvar exe failed!
@goto ISERR

:ERR6
@echo wmm_magvar exe failed!
@goto ISERR

:ERR7
@echo testmagvar exe failed!
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
