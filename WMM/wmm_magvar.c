//---------------------------------------------------------------------------

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "GeomagnetismHeader.h"
#include "EGM9615.h"

#ifndef DEF_COF_FILE
#define DEF_COF_FILE "WMM.COF"
#endif

//---------------------------------------------------------------------------

/* 

WMM Magnetic Variation for a point

The Geomagnetism Library is used to perform calculation on a user input location,
and prints the results to the standard output. The program is link to the WMM.lib,
built from GeomagnetismLibrary.c, GeomagnetismHeader.h.
The loacation of the WMM.COF file is hard coded by default, but a new loaction can be 
given from the command line. 

Geoff R. McLane - reports _at_ geoffair _dot_ info
March 13, 2014

 *  Revision Number: $Revision: 001 $
 *  Last changed by: $Author: geoff $
 *  Last changed on: $Date: 2014/03/13 20:39:24 +0100 (Thu, 13 Mar 2014) $
 *  2021/01/09 - Update WMM.COF to 2020.0 12/10/2019
 */

#define IS_DIGIT(a) (( a >= '0' ) && (a <= '9'))

static int verbose = 0;
static double def_height = 0.0;
static MAGtype_Date UserDate;
static const char *filename = DEF_COF_FILE; //"WMM.COF";
/* 
 *  Update WMM.COFF -     2020.0            WMM-2020        12/10/2019
 */
static char VersionDate_Large[]      = "$Date: 2019/10/12 00:00:00 +0100 (Sat, 12 Oct 2019) $";
static char VersionDate_Large_prev[] = "$Date: 2014/03/13 20:39:24 +0100 (Thu, 13 Mar 2014) $";
static MAGtype_CoordGeodetic CoordGeodetic;
static char Error_Message[255];
static char VersionDate[12];

#ifdef _MSC_VER
#define M_ISDIR _S_IFDIR
#else
#define M_ISDIR __S_IFDIR
#endif

static struct stat stat_buf;
// return: 0 = not file or directory, 1 = file, 2 = directory, 
int is_file_or_directory( char *file )
{
    int iret = 0;
    if (stat(file,&stat_buf) == 0) {
        if (stat_buf.st_mode & M_ISDIR) 
            iret = 2;
        else
            iret = 1;
    }
    return iret;
}


// rought is a decimal service
int is_a_decimal( char *arg )
{
    int i, c, digs = 0, haddec = 0;
    int len = (int)strlen(arg);
    if (len == 0) return 0; // NOT a decimal
    for (i = 0; i < len; i++) {
        c = arg[i];
        if (IS_DIGIT(c)) {
            digs++;
        } else if (c == '.') {
            if (haddec) return 0;
            haddec++;
        } else if (c == '-') {
            if (i) return 0;
        } else {
            return 0;
        }
    }
    if (!digs) return 0;
    return 1;
}

int in_world_range( double lat, double lon ) 
{
    if ((lat < -90.0)||(lat > 90.0)||
        (lon < -180.0)||(lon > 180.0)) {
            return 0;
    }
    return 1;
}

char *get_file_name(char *path)
{
    char *name = path;
    int len = (int)strlen(path);
    int i, c, i2;
    for (i = 0; i < len; i++) {
        c = path[i];
        i2 = i + 1;
        if ((c == '\\')||(c =='/')) {
            if (i2 < len)
                name = &path[i2];
        }
    }
    return name;
}

void give_help( char *vers, char *name )
{
    printf("%s, version %s\n", get_file_name(name), vers);
    printf("Usage: [options] lat_degs lon_degs\n");
    printf("Options:\n");
    printf(" --help  (-h or -?) = This help and exit(2)\n");
    printf(" --verbose     (-v) = Bump verbosity, to show ALL values in output (def=%s)\n",
        (verbose ? "on" : "off") );
    printf(" --cof <file>  (-c) = Location of COF file (def=%s)\n", filename);
    printf(" --alt         (-a) = Altitiude in km AMSL (def=%.1lf)\n", def_height);
    printf(" --day <num>   (-d) = Day (1-31) (def=%d)\n", UserDate.Day);
    printf(" --month <num> (-m) = Month (1-12) (def=%d)\n", UserDate.Month);
    printf(" --year <num>  (-y) = Year - 4 digits (2010-2025) (def=%d)\n", UserDate.Year);
    printf("Note, the Year give is NOT checked to be in the valid range given ablove.\n");

    printf("\n");
    printf("Given a latitude and longitude in degrees, use the 'Geomagnetism' Library,\n");
    printf("see http://www.ngdc.noaa.gov/geomag/WMM/soft.shtml, for source and details,\n");
    printf("to calculate the current magnetic deviation of that point on the earth.\n");

    printf("\n");
    printf("Presently using file '%s' %s\n", filename,
        ((is_file_or_directory((char*)filename) == 1) ? "ok" : "Not Found"));
    printf("Last updated 2021/01/09 - to vers 2020.0 12/10/2019, valid 2020.0 to 2025.0\n");

    printf("\nThe WMM source code is in the public domain and not licensed or under copyright.\n");

}

int parse_command( int argc, char **argv )
{
    int i, c, i2, j, cnt = 0;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        if ((*arg == '-') && !is_a_decimal(&arg[1])) {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            i2 = i + 1;
            switch (c) {
            case 'h':
            case '?':
                give_help(VersionDate, argv[0]);
                return 2;
                break;
            case 'a':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    CoordGeodetic.HeightAboveGeoid = atof(sarg);
                } else {
                    printf("Expected altitude to follow %s\n", arg);
                    return 1;

                }
                break;
            case 'c':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    filename = strdup(sarg);
                } else {
                    printf("Expected cof file to follow %s\n", arg);
                    return 1;

                }
                break;
            case 'y':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    UserDate.Year = atoi(sarg);
                } else {
                    printf("Expected year to follow %s\n", arg);
                    return 1;

                }
                break;
            case 'm':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    UserDate.Month = atoi(sarg);
                } else {
                    printf("Expected month to follow %s\n", arg);
                    return 1;

                }
                break;
            case 'd':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    UserDate.Day = atoi(sarg);
                } else {
                    printf("Expected day to follow %s\n", arg);
                    return 1;
                }
                break;
            case 'v':
                verbose++;
                break;

            default:
                printf("Unknown argument %s... aborting...\n", arg);
                return 1;
            }
        } else {
            // bare arg - assume lat lon
            if (cnt == 0) {
               j = sscanf(arg, "%lf", &CoordGeodetic.phi);
               if ((j == 1) && is_a_decimal(arg)) {
                   // this is ok
               } else {
                   printf("First argument not a decimal latitudes! %s\n",arg);
                   return 1;
               }
            } else if (cnt == 1) {
               j = sscanf(arg, "%lf", &CoordGeodetic.lambda);
               if ((j == 1) && is_a_decimal(arg)) {
                   // this is ok
               } else {
                   printf("Second argument not a decimal longitude! %s\n",arg);
                   return 1;
               }

            } else {
                printf("Already have lat %lfm, lon %1f. What is this %s\n", 
                    CoordGeodetic.phi, CoordGeodetic.lambda, arg);
                printf("Unknown argument %s... aborting...\n", arg);
                return 1;
            }
            cnt++;
        }

    }
    if ((cnt != 2) || !in_world_range(CoordGeodetic.phi, CoordGeodetic.lambda)) {
        if (cnt != 2)
            printf("Error: Did NOT find lat lon in command!\n");
        else
            printf("Error: Invalid lat %lf, or lon %lf\n", CoordGeodetic.phi, CoordGeodetic.lambda );
        return 1;
    }
    if(!MAG_DateToYear(&UserDate, Error_Message)) {
        printf("%s",Error_Message);
        return 1;
    }
    if (is_file_or_directory((char *)filename) != 1) {
        printf("Erro: Unable to locate file [%s]\n",filename);
        printf("Check name and location of file. Aborting...\n");
        return 1;
    }

    return 0;
}

int main(int argc, char **argv)
{
    //static char DeclString[100];

    int i;
    //int cnt = 0;
    time_t rawtime;
    struct tm * timeinfo;

    MAGtype_MagneticModel * MagneticModels[1], *TimedMagneticModel;
    MAGtype_Ellipsoid Ellip;
    MAGtype_CoordSpherical CoordSpherical;
    MAGtype_GeoMagneticElements GeoMagneticElements;
    MAGtype_Geoid Geoid;
    int NumTerms;
    //int Flag = 1;
    int nMax = 0;
    int epochs = 1;

    strncpy(VersionDate, VersionDate_Large + 39, 11);
    VersionDate[11] = '\0';

    MAG_SetDefaults(&Ellip, &Geoid); /* Set default values and constants */
    /* Check for Geographic Poles */
    //MAG_InitializeGeoid(&Geoid);    /* Read the Geoid file */
    /* Set EGM96 Geoid parameters */
    Geoid.GeoidHeightBuffer = GeoidHeightBuffer;
    Geoid.Geoid_Initialized = 1;
    /* Set EGM96 Geoid parameters END */
    //b = MAG_GeomagIntroduction_WMM(MagneticModels[0], VersionDate);
    CoordGeodetic.phi = 200.0;
    CoordGeodetic.lambda = 200.0;
    Geoid.UseGeoid = 1;
    CoordGeodetic.HeightAboveGeoid = def_height;

    time (&rawtime);
    timeinfo = localtime ( &rawtime );
    if (!timeinfo) {
        printf("function locatime failed! aborting...\n");
        return 1;

    }
    //h  =  timeinfo->tm_hour;
    // MagneticDate->Month, MagneticDate->Day, MagneticDate->Year
    UserDate.Year  = timeinfo->tm_year + 1900;
    UserDate.Month = timeinfo->tm_mon + 1;
    UserDate.Day   = timeinfo->tm_mday;
    i = parse_command(argc,argv);
    if (i)
        return i;

    /* Memory allocation */
    MAG_robustReadMagModels((char *)filename, &MagneticModels, epochs);
    if(nMax < MagneticModels[0]->nMax) nMax = MagneticModels[0]->nMax;
    NumTerms = ((nMax + 1) * (nMax + 2) / 2);
    TimedMagneticModel = MAG_AllocateModelMemory(NumTerms); /* For storing the time modified WMM Model parameters */
    if((MagneticModels[0] == NULL) || (TimedMagneticModel == NULL)) {
       printf("Error allocating in MAG_AllocateModelMemory. aborting...\n");
       return 1;
    }

    MAG_ConvertGeoidToEllipsoidHeight(&CoordGeodetic, &Geoid);

    MAG_GeodeticToSpherical(Ellip, CoordGeodetic, &CoordSpherical); /*Convert from geodetic to Spherical Equations: 17-18, WMM Technical report*/
    MAG_TimelyModifyMagneticModel(UserDate, MagneticModels[0], TimedMagneticModel); /* Time adjust the coefficients, Equation 19, WMM Technical report */
    MAG_Geomag(Ellip, CoordSpherical, CoordGeodetic, TimedMagneticModel, &GeoMagneticElements); /* Computes the geoMagnetic field elements and their time change*/
    MAG_CalculateGridVariation(CoordGeodetic, &GeoMagneticElements);
    if (verbose) {
        MAG_PrintUserData(GeoMagneticElements, CoordGeodetic, UserDate, TimedMagneticModel, &Geoid); /* Print the results */
        printf("From input '%s' file.\n", filename);
        printf("\n");
    } 

    printf("Magnetic Variation: %.2lf degrees.\n", GeoMagneticElements.Decl );

    MAG_FreeMagneticModelMemory(TimedMagneticModel);
    MAG_FreeMagneticModelMemory(MagneticModels[0]);

    return 0;
}

// eof
