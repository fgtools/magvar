object AboutForm: TAboutForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'World Magnetic Model 2010 About'
  ClientHeight = 254
  ClientWidth = 439
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 7
    Top = 8
    Width = 424
    Height = 201
    TabStop = False
    BevelInner = bvNone
    BevelOuter = bvNone
    Color = clBtnFace
    Lines.Strings = (
      
        'The World Magnetic Model (WMM) is a joint product of the United ' +
        'State`s National '
      
        'Geospatioal-Intelligence Agency (NGA) and the United Kingdom`s D' +
        'efence '
      
        'Geographic Centre (DGC). The WMM was developed jointly by the Na' +
        'tional '
      
        'Geophysical Data Center (NGDC, Boulder CO, USA) and the British ' +
        'Geological Survey '
      '(BGS, Edinburgh, Scotland, UK).'
      ''
      
        'When installed with the correct coefficients file, WMM.COF (WMM2' +
        '010, released on '
      
        'December 15, 2009), this program computes the elements of the Wo' +
        'rld Magnetic '
      'Model 2010.'
      '')
    ReadOnly = True
    TabOrder = 0
  end
  object AboutCloseButton: TButton
    Left = 169
    Top = 221
    Width = 75
    Height = 25
    Caption = 'Close'
    Default = True
    TabOrder = 1
    OnClick = AboutCloseButtonClick
  end
end
