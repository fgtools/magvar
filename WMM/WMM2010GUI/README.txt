The World Magnetic Model
------------------------

GUI:
To run the Graphical User Interface for the WMM-2010, open the subfolder 
'exe' and click on WMM.exe

Source code:
The subfolders 'StringGrid' and 'WMM2005GUI' contain the source code. 
It can be re-compiled by the follwing steps:
1. Open StringGrid/CustomStringGrid.bdsproj with Borland C++ 2009.
2. Go to Project>Build Configurations.
3. Select the available project and select Edit.
4. Highlight Release Build and click Activate.
5. Exit out of those windows back to the main window.
6. Right click on CustomStringGrid.bpl in the project manager.
7. Select install from the right click menu.
8. Expect message "C:\Documents and Settings\...\bpl\CustomGrid.bpl has been installed.  The following new components have been registered: TCustomStringGrid."
9. Select File>Close All.
10. Open WMM2010GUI/WMM.bdsproj with Borland C++ 2009.
11. Repeat steps 2-5.
12. Open the WMMForm1.cpp file for editing.  (If the GUI form is in view 
	instead of the GUI souce code, look for options "WMMForm1.cpp/WMMForm1.h/Design/History" at the bottom of the WMMForm1 window, and choose "WMMForm1.cpp".)
13. Change lines 13-15 to point to the indicated files on your machine.
14. Open the WMMForm1.h file for editing.  (If the GUI form is in view 
	instead of the GUI souce code, look for options "WMMForm1.cpp/WMMForm1.h/Design/History" at the bottom of the WMMForm1 window, and choose "WMMForm1.h".)
15. Change line 14 to point to the correct path for file "CStringGrid.h"
16. Select Project > Build
17. Select Run > Run.
	-NOTE- WMM.cof, EGM9615.bin and WMM2010_HELP must be in the build directories for the executable to run properly. These can be found in the exe directory.



Background:
============

The World Magnetic Model is a product of the United States National 
Geospatial-Intelligence Agency (NGA). The U.S. National Geophysical 
Data Center (NGDC) and the British Geological Survey (BGS) produced 
the WMM with funding provided by NGA in the USA and by the Defence 
Geographic Imagery and Intelligence Agency (DGIA) in the UK.

The World Magnetic Model is the standard model of the US Department 
of Defense, the UK Ministry of Defence, the North Atlantic Treaty 
Organization (NATO), and the World Hydrographic Office (WHO) navigation 
and attitude/heading referencing systems. It is also used widely in 
civilian navigation systems. The model, associated software, and 
documentation are distributed by NGDC on behalf of NGA. The model 
is produced at 5-year intervals, with the current model (WMM2010) expiring 
on December 31, 2014.

Model Software Support
======================

*  National Geophysical Data Center
*  NOAA EGC/2
*  325 Broadway"
*  Boulder, CO 80303 USA
*  Attn: Manoj Nair or Stefan Maus
*  Phone:  (303) 497-4642 or -6522
*  Email:  Manoj.C.Nair@noaa.gov or Stefan.Maus@noaa.gov
For more details about the World Magnetic Model visit 
http://www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml


Updates to WMM GUI
==================

12/2009: 
Updated the date TDateTimePicker to allow dates to be selectable for the next WMM model period (2010-2015).
Updated the About and Help dialogs to reflect new WMM model.
Updating to C++ Builder 2009 required all Anistring variables to be altered to confirm to complete Unicode compatibility as mandated by the 2009 release.
1/2010:
Altered CalculateButtonClick function to call new WMM software for magnetic calculations instead of using local functions to make these calculations.
New code is lines 185 � 322, inclusive. The important WMM function call is WMM_Geomag on line 213
Old code removed from WMM GUI included the functions geomag and geomag1, on lines 594 and 601. These functions called function E0000, line 315, in different manners. This function was responsible for calculating the Magnetic properties but has since been removed.
2/1/2010:
Integrated the new WMM subroutines. WMM_Sublibrary.c provides the function WMM_Geomag(), which provides greater accuracy in all geomagnetic calculations.
2/19/2010
Added a switch for height above MSL / ELLIPSOID
Print X,Y and Declinamtion values at Geographic pole

